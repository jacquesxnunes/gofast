<?php

/**
 * Classe para criar o objeto combobox 
 * 
 * @file                cmbBoxClass.php
 * @license		F71
 * @link		
 * @copyright           2016 F71
 * @author		Jacques <jacques@f71.com.br>
 * @package             cmbBoxClass
 * @access              public  
 * 
 * @version: 3.0.0000 - 16/01/2017 - Jacques - Versão Inicial 
 * 
 * @todo 
 * 
 */

namespace GoFast\Lib;

use GoFast\Kernel\Core;

/**
 * Classe para criação e manipulação do combobox
 */
class CmbBox extends Core
{

    private $dbBox;
    private $selected;
    private $html;

    use \GoFast\Lib\Bridge;

    public function __construct($value = null)
    {

        if (is_array($value)) {
            parent::__construct($value);
            if (isset($value['db'])) {
                $this->dbBox = $value['db'];
            } else {
                $this->dbBox = $value;
            }
        } else {
            $this->dbBox = $value;
        }

        $this->createCoreClass($value);


        return $this;
    }

    /**
     * Método que define Define valores default da classe
     * 
     * @access public
     * @method setDefault
     * @param  
     * 
     * @return $this
     */
    public function setDefault()
    {

        $this->value = 0;
        $this->html = '';

        return $this;
    }


    /**
     * Método que link um banco de dados ao objeto
     * 
     * @access public
     * @method setDefault
     * @param  
     * 
     * @return $this
     */
    public function link($value = null)
    {

        $this->dbBox = $value;

        return $this;
    }

    /**
     * O procedimento retorna uma string em html formatando um <select> quando referênciado diretamente o objeto
     * 
     * 09/01/2020 - fix: correção de setSelect e uso via array que permitia inconsistência no uso do operador OR
     *                   na seleção do registro selecionado
     * 
     * @access public
     * @method getHtml
     * @param  $v (string)            - Nome do Campo que possui o valor ou um array
     * @param  $text (string)         - Nome ou (Nomes concatenados com +) de Campos que deverão ser exibidos como option
     * @param  $id (string)           - Nome do id da classe
     * @param  $class (string)        - Nome da Classe
     * @param  $name (string)         - Nome do campo do formulário
     * @param  $charset (string)      - Define o charset de saída
     * 
     * @return string
     */
    public function getHtml($v = null, $text = null, $id = null, $class = null, $name = null, $charset = '')
    {

        try {

            $this->setValue(0);

            $attrib_select = '';
            $onlyOptions = false;
            $default = true;

            if (is_array($v)) {

                $class = $name = '';

                foreach ($v as $key => $value) {

                    switch ($key) {
                        case 'selected':
                            $this->setSelected($value);
                            break;
                        case 'id':
                        case 'class':
                        case 'name':
                        case 'disabled':
                        case 'multiple':
                            $attrib_select .= "{$key}=\"{$value}\" ";
                            break;
                        case 'data':
                            if (is_array($value)) {
                                foreach ($value as $data_k => $data_v) {
                                    foreach (explode(',', $data_v) as $data_kk => $data_vv) {
                                        $data_key[$data_k] .= "data-{$data_vv} ";
                                    }
                                }
                            } else {
                                foreach (explode(',', $value) as $data_kk => $data_vv) {
                                    $attrib_select .= "data-{$data_vv} ";
                                }
                            }
                            break;
                        case 'readonly':
                            $attrib_select .= $key;
                            break;
                        case 'text':
                            $text = $value;
                            break;
                        case 'value':
                            $_v = $value;
                            break;
                        case 'charset':
                            $charset = $value;
                            break;
                        case 'default':
                            $default = $value;

                            if ($default) {
                                $default = $value;

                                if (!isset($default['value']))
                                    $default['value'] = '-1';

                                if (!isset($default['text']))
                                    $default['text'] = '<< Selecione >>';
                            }

                            break;
                        case 'onlyOptions':
                            $onlyOptions = $value;
                        default:
                            break;
                    }
                }
                if (!$onlyOptions)
                    $this->html = "<select {$attrib_select}>";
            } else {

                $id_v = !empty($id) ? "id='{$id}'" : "";

                $class_v = !empty($class) ? "class='{$class}'" : "";

                $name_v = !empty($name) ? "name='{$name}'" : "";

                $this->html = "<select {$id_v} {$class_v} {$name_v}>";

                $_v = $v;
            }

            if (is_array($default)) {
                if ($default['text'] !== false)
                    $this->html .= "<option value='{$default['value']}' class='{$default['class']}'>{$default['text']}</option>";
            } else {
                if ($default) $this->html .= "<option value='-1'>« Selecione »</option>";
            }

            $fields = explode('+', $text);

            if (is_object($this->dbBox) || is_array($this->dbBox)) {

                $array = is_array($this->dbBox) ? $this->dbBox : $this->dbBox->getArray();

                foreach ($array as $key_val => $val) {

                    if ($val) {

                        $option = '';

                        if (empty($text)) {

                            $option = $val;

                            $selected = $key_val;
                        } else {

                            $selected = $val[$_v];

                            foreach ($fields as $key_field => $field) {

                                $option .= ($key_field > 0 ? " - " : "") . $val[$field];
                            }
                        }

                        $tag_selected = ($selected == $this->getSelected() || in_array($selected, $this->getSelected())) ? "selected='selected'" : "";

                        $this->html .= "<option value='{$selected}' {$tag_selected} {$data_key[$selected]}>{$option}</option>";
                    }
                }
            }

            if (!$onlyOptions)
                $this->html .= "</select>";

            $this->setValue(1);
        } catch (\Exception $ex) {
        }

        return empty($charset) ? $this->html : iconv(mb_detect_encoding($this->html), $charset, $this->html);

        return $this->html;
    }

    /**
     * O procedimento constroi um select baseado em um array 
     * 
     * @access public
     * @method make
     * @param  $options (string)     - Array de opções para montagem do select
     * @param  $value (string)       - Valor do select selecionado
     * @param  $atributos (string)   - Atributos do select
     * 
     * @return string
     */
    public function make($options = null, $value = null, $atributos = null)
    {

        $this->html = "<select ";

        if (is_array($atributos)) {

            foreach ($atributos as $key => $val) {

                $this->html .= $key . "=\"" . $val . "\" ";
            }
        } else {

            $this->html .= $atributos;
        }

        $this->html .= ">";


        if (is_array($options)) {

            foreach ($options as $key => $val) {

                if ((!empty($value) && $value == $key) || $key == $this->getSelected()) {

                    $selected = " selected=\"selected\"";
                } else {

                    $selected = "";
                }

                $this->html .= "<option value=\"" . $key . "\"$selected>" . $val . "</option>";
            }
        }

        $this->html .= "</select>";

        return $this;
    }

    public function setSelected($value = null)
    {

        $this->selected = $value;

        return $this;
    }

    public function getSelected()
    {

        return $this->selected;
    }
}
