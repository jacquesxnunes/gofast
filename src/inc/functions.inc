<?php

// pra imprimir arrays com tag pre
function print_array($array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
    return TRUE;
}

function print_backtrace() {
    foreach(debug_backtrace() as $key => $value) {
        echo "{$value['file']} [{$value['line']}] {$value['class']}->{$value['function']}<br/>";
    }
}

function dump_file($value){
    
    ob_start();
    print_array($value);
    $buffer = ob_get_contents();
    ob_end_clean();
    //file_put_contents(sys_get_temp_dir() . DS  . 'dump.txt', $buffer);  
    file_put_contents(ROOT_DIR . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'dump.txt', $buffer);  

}

/**
 * Está função deverá entrar na classe de bibliotecas
 */
function array_find($string, $array)
{

    foreach ($array as $key => $value) {

        if (strpos($value, $string) !== FALSE) {

            return $key;

            break;
        }
    }
}

// if (!is_object($Language)) {

//     $Language = new \GoFast\Lib\Language(array('locale' => $_REQUEST['locale']));
// }

/**
 * Tradutor de linguagem via array com dicionário de dados
 *
 * @param array $value
 *
 * @return string
 */
function _lang($value)
{

    $string = array_key_exists($value['key'], $messages_dictionary) ? $messages_dictionary[$value['key']] : $value['key'];

    return (count($value['args']) > 1) ? vsprintf($string, $value['args']) : $string;
}


/**
 * Ativa o modo emulado de tradução de texto com o alias _() e carrega a classe apenas uma vez
 *
 * @param  string
 * @return string translator
 */
if (false === function_exists('gettext')) {

    function _($value)
    {


        return $value;

        // global $Language;

        // return $Language->isOk() ? $Language->translator->gettext($value) : $value;
    }
}
