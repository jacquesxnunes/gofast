<?php
/**
 * Classe para manipulação das classes
 * 
 * @file                git.class.php
 * @license		F71
 * @link		
 * @copyright           2016 F71
 * @author		Jacques <jacques@f71.com.br>
 * @package             Version
 * @access              public  
 * 
 * @version: 3.0.0000 - 27/12/2018 - Jacques - Versão Inicial 
 * 
 * @todo 
 * 
 */
namespace GoFast\Lib;

trait Bridge {


   /**
    * Método para criar e instânciar as classes mães do core
    * 
    * @access public
    * @method createCoreClass
    * @param
    * 
    * @return $this
    */       
    public function createCoreClass(&$value = null) {


        $trace = debug_backtrace(2);



        // echo $trace[1]['file'] . '<br/>';
        // echo $trace[0]['file'] . '<br/>';    
        // echo "<div style='margin-left: 50px;border-width: 5px;border-color: red;border-style: dashed;'>{$value['class']}->createCoreClass({$value['core']}<br/>";


        if(!include($file = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'inc' . DIRECTORY_SEPARATOR . 'create-core.inc')) die ("Não foi possível incluir {$file} a partir de ".__FILE__); 

        // echo ")</div>";


    } 
            
  
    
}

