<?php 

/**
 * Classe data
 * 
 * @version: 3.00.0000 - 00/00/0000 - Jacques - Classe para manipula��o de datas
 * @version: 3.01.0000 - 11/08/2015 - Jacques - Adicionada fun��o para calculo de datas com dois formatos
 * @version: 3.02.0000 - 14/09/2015 - Sin�sio - Cria��o do método explodeDate().
 * @version: 3.03.0000 - 23/09/2015 - Jacques - Adicionado método para converter formata��o de data na codifica��o PHP para sintaxe SQL
 * @version: 3.04.0000 - 06/10/2015 - Jacques - Alterado a f�rmula de c�lculo do método diffInDays
 * @version: 3.05.0000 - 13/10/2015 - Jacques - Implementado a possibilidade de uso de métodos encadeados e uso simplificado do set e get
 * @version: 3.06.0000 - 27/10/2015 - Jacques - Implementado o método getNow que obtem a hora atual
 * @version: 3.07.0000 - 11/11/2015 - Jacques - Implementado o método para retornar o m�s por extenso
 * @version: 3.08.4340 - 11/11/2015 - Jacques - Altera��o do método daysInMonth para usar fun��o nativa do PHP
 * @version: 3.08.4340 - 11/01/2016 - Jacques - Estou tendo um bug em rela��o ao método get dessa classe. Quando seto o método para n�o retornar 
 *                                            retornar a data de 1970-01-01 para datas em branco, afeto os calculos de per�odo aquisitivo e f�rias dobradas
 *                                            $this->value_return = isset($format) ?  empty($this->value) ? '' : gmdate($format,strtotime($this->value)) : $this->value;
 * @version: 3.08.7144 - 29/02/2016 - Jacques - Adicionado métodos sumYear() e minusYear()
 * @version: 3.08.0000 - 21/03/2016 - Jacques - Erro no método minusMonth que estava executando a opera��o em dias ao inv�z de m�s
 * 
 * @see: $date_obj = DateTime::createFromFormat('d-m-Y', $data_string)->format('Y-m-d'); 
 * 
 * http://il.php.net/manual/en/book.datetime.php
 * 
 * @Jacques
 */
namespace GoFast\Lib;

use GoFast\Kernel\Core;
class Date extends Core {
    
    use \GoFast\Lib\Bridge; 
    
    private $date_time;
    private $date_time_on = 0;
    private $time_zone = 'America/Sao_Paulo';
    private $month = array();
    private $value_return;
    
    public function __construct($value = null) {
        
        $this->createCoreClass($value);
        
        $this->month[1] = _("Janeiro");
        $this->month[2] = _("Fevereiro");
        $this->month[3] = _("Março");
        $this->month[4] = _("Abril");
        $this->month[5] = _("Maio");
        $this->month[6] = _("Junho");
        $this->month[7] = _("Julho");
        $this->month[8] = _("Agosto");
        $this->month[9] = _("Setembro");
        $this->month[10] = _("Outubro");
        $this->month[11] = _("Novembro");
        $this->month[12] = _("Dezembro");
        
        $this->set(empty($value) ? 'now' : $value);
        
    }

    
    /**
     * Define valores default para a classe data
     * 
     * @access public
     * @method setDefault
     * @param 
     * 
     * @see 
     * 
     * @return $this;
     */     
    public function setDefault(){
        
        $this->value = $this->value_return = $this->date_time = '';
        
        return $this;        
        
    }
    
    /**
     * Define o valor de uma data, caso o parâmetro seja vazio define o valor com a data corrente
     * Necessário identificar tipo de data no Banco de Dados para formata-la (a implementar)
     * 
     * @access public
     * @method set
     * @param 
     * 
     * @see 
     * 
     * @return $this;
     */     
    public function set($value = null,$format = 'Y-m-d'){
        
        if(class_exists('DateTime') && $format!=='Y-m-d') {
            
            $d = \DateTime::createFromFormat($format, $value);

            $this->value = ($d && $d->format($format) === $value ? $d->format('Y-m-d') : $value);             
            
        }
        else {
                
            $value = is_null($value) ? "" : ($value=='now' ? $this->now() : $value);

            $this->value = $value; 
            
        }
        
        return $this;        
        
    } 

    /**
     * Define o valor de uma data no padrão antigo
     * 
     * @access public
     * @method setDate
     * @param 
     * 
     * @see 
     * 
     * @return $this;
     */    
    public function setDate($value = null,$format = null){
        
        $this->set($value,$format);
        
        return $this;        
        
    } 
    
    /**
     * Obtem o valor de uma data
     * 
     * @access public
     * @method get
     * @param 
     * 
     * @see 
     * 
     * @return $this;
     */    
    public function get($format = null, $value = null){
        
        if(!empty($value)){

            $this->value = $value;

        }
        
        if(class_exists('DateTime') && $this->date_time_on) {
            
            $this->value_return = empty($format) ?  $this->value : $this->date_time->format($format);            
            
        }
        else {
            
            if(!empty($this->value) && $this->value!=='0000-00-00') {

                $this->value_return = empty($format) ?  $this->value : gmdate($format,strtotime($this->value));    

            }
            else {

                $this->value_return = '';

            }
            
        }
        
        return $this;        
        
    }
    
    /**
     * Obtem o valor de uma data no formato antigo - Deprecated
     * 
     * @access public
     * @method get
     * @param 
     * 
     * @see 
     * 
     * @return $this;
     */     
    public function getDate($format = null, $value = null){
        
        $this->get($format,$value);
        
        return $this;        
        
    }
    
    /**
     * Método para formatar uma data no padrão mysql
     * 
     * @access public
     * @method getFmtDateConvSql
     * @param 
     * 
     * @see 
     * 
     * @return $this;
     */     
    public function getFmtDateConvSql($value = null){
        
        $patterns = array();
        $patterns[0] = '/d/';
        $patterns[1] = '/m/';
        $patterns[2] = '/y/';
        $patterns[3] = '/D/';
        $patterns[4] = '/M/';
        $patterns[5] = '/Y/';
        
        $replacements = array();
        $replacements[0] = '%d';
        $replacements[1] = '%m';
        $replacements[2] = '%y';
        $replacements[3] = '%D';
        $replacements[4] = '%M';
        $replacements[5] = '%Y';
        
        /**
         * Define o formato de data de Query Sql baseado no formato de data
         */
        $this->value_return = preg_replace($patterns, $replacements, $value);    
        
        return $this;        
        
    }
    
    /**
     * Método que retorna o timestamp de uma data no formato DD/MM/AAAA passada como valor
     * 
     * @access public
     * @method geraTimeStamp
     * @param 
     * 
     * @see 
     * 
     * @return $this;
     */      
    public function geraTimeStamp($value = null){
        
        $partes = explode('/', $value);
        
        $this->value_return = mktime(0, 0, 0, $partes[1], $partes[0], $partes[2]);
        
        return $this;        
        
        
    }    
    
    public function getMonthString($value = null){
        
        $this->value_return = !isset($value) ? str_replace('01/01/1970','erro',gmdate($format,strtotime($value))) : $date;        
        
        return $this;        
        
    }
    
    /**
     * Obtem a data e hora atual
     * 
     * @access public
     * @method getNow
     * @param
     * 
     * @return string;
     */      
    public function now($value = null){
        
        $this->value_return = $this->value = date(empty($value) ? "Y-m-d H:i:s" : $value, mktime()); 
        
        return $this;
        
    }
    
    /**
     * Obtem a data e hora atual
     * 
     * @access public
     * @method getNow
     * @param
     * 
     * @return string;
     */     
    public function getNow($value = null){
        
        return $this->now()->get($value);
        
    }
    
    /**
     * Calcula a diferença em dias entre duas datas passadas por valor ou entre a data do objeto e o valor
     * 
     * @access public
     * @method diffInDays
     * @param
     * 
     * @return $this;
     */     
    public function diffInDays($value1 = null,$value2 = null){

        if(empty($value2)) {
            
            $value2 = $this->get();
            
        }
        
        /**
         * Calcula a diferença em segundos entre as datas
         */
        $diferenca = strtotime($value1) - strtotime($value2);

        /**
         * Calcula e retorna a diferença em dias
         */
        $this->value_return = floor($diferenca / (60 * 60 * 24));     
        
//        echo '<pre>';
//        echo "value1 = {$value1}";
//        echo "value2 = {$value2}";
//        echo '</pre>';
        
        return $this;  
        
    }    
    
    /**
     * Calcula o número de dias no mês a partir do dia final do mês ou inicial
     * 
     * @access public
     * @method daysMonth
     * @param
     * 
     * @return $this;
     */     
    public function daysMonth($value = 'start'){
        
        
        if($value=='start') {

           $diff = (int)$this->get("d")->val();
                   
        }
        else {
            
            if(!empty($value)) $this->set($value);

            /**
             * Calcula a diferença em segundos entre as datas
             */
            $dia = $this->get()->daysInMonth()->val();

            $value2 = $this->get("Y-m-{$dia}")->val();
            
            $diff = (int)$this->diffInDays($value2)->val() + 1;
            
        }

        /**
         * Calcula e retorna a diferença em dias
         */
        $this->value_return = $diff;
        
//        echo '<pre>';
//        echo "value1    = {$this->get()}</br>";
//        echo "value2    = {$value2}</br>";
//        echo "diferenca = {$diff}</br>";
//        echo '</pre>';
        
        return $this;  
        
    }
    
    /**
     * Funcao que retorna qual é o dia da semana em numero. 0 a 6 - Dom a Sab
     * 
     * @return $this
     */
    public function getWeekdayInMonth(){
        $this->value;
        $_mes = $this->get('m')->val();
        $_ano = $this->get('Y')->val();
        $_dia = $this->get('d')->val();
        $this->value_return = date('w', mktime(0, 0, 0, $_mes, $_dia, $_ano));
        return $this;
    }
    /**
     * Função que monta um array com 2 opções.( Dia e Semana ) 
     * 1 - Conta quantos dias da Semana tem no mes e ano solicitado
     * 2 - Pega os dias referentes referentes ao mes e ano solicitado. 
     * Obs: O parametro type decide qual a opção.
     * @param type $type
     * @return $this
    */ 
    
    public function countWeekdayInMonth($type = null){

        $this->value;
        $_mes = $this->get('m')->val();
        $_ano = $this->get('Y')->val();
        $tmp = mktime(0, 0, 0, $_mes, 1, $_ano);
        $ultimo_dia = date("t",$tmp);

        $dias = "";
        for($a=1;$a <= $ultimo_dia;$a++) {

            switch($this->set("{$_ano}-{$_mes}-{$a}")->getWeekdayInMonth()->val())
            {
                       case '0':
                                                $dias['days'][1][]=$a;
						$dias['count'][1]++;
                                                break;
                        case '1':
                                                $dias['days'][2][]=$a;
						$dias['count'][2]++;
                                                break;
                        case '2':
                                                $dias['days'][3][]=$a;
						$dias['count'][3]++;
                                                break;
                        case '3':
                                                $dias['days'][4][]=$a;
						$dias['count'][4]++;
                                                break;
                        case '4':
                                                $dias['days'][5][]=$a;
						$dias['count'][5]++;
                                                break;
                        case '5':
                                                $dias['days'][6][]=$a;
						$dias['count'][6]++;
                                                break;
                        case '6':
                                                $dias['days'][7][]=$a;
						$dias['count'][7]++;
                                                break;
            }

        }
        empty($type) ? $this->value_return = $dias : $this->value_return = $dias[$type] ;
        
                       
    return $this;  
        
    }
    
        
    /**
     * Método apra separar dia, mês e ano em um vetor
     * 
     * @access public
     * @method daysInMonth
     * @param
     * 
     * @return $this;
     */      
    public function explodeDate($value = null,&$array = null){
        
        if(empty($value)){
            
            $value = $this->value;
                    
        }
        
        $separador = preg_replace("/[0-9]/","", $value); 
        
        if(empty($separador[0])){
            
            $array = array(
                          substr($value, 1, 4),
                          substr($value, 5, 2),
                          substr($value, 7, 2),
                          );
        }
        else {

            $array = explode($separador[0],$value);
         
        }    
        
        return $this;  
        
    }
    
    /**
     * Calcula o número de dias no mês a partir de uma data 
     * 
     * @access public
     * @method daysInMonth
     * @param
     * 
     * @return $this;
     */      
    public function daysInMonth($value = null) 
    { 
       
        if(!empty($value)){
            
            $this->set($value);
            
        }
       
        $this->value_return = cal_days_in_month(CAL_GREGORIAN,$this->get('m')->val(), $this->get('Y')->val());
        
        return $this;
        
    }     

    /**
     * Executa a soma de dias a data corrente da classe
     * 
     * @access protected
     * @method sumDays
     * @param
     * 
     * @return $this;
     */     
    public function sumDays($value = 1){
        
        $this->value_return = $this->value = date('Y-m-d', strtotime("+{$value} days",strtotime($this->value)));        
        
        return $this;
        
    }

    /**
     * Executa a subtração de dias a data corrente da classe
     * 
     * @access public
     * @method minusDays
     * @param
     * 
     * @return $this;
     */     
    public function minusDays($value = 1){

        $this->value_return = $this->value = date('Y-m-d', strtotime("-{$value} days",strtotime($this->value)));        
        
        return $this;
        
    }
    
    
    /**
     * Executa a soma de meses
     * 
     * @access public
     * @method sumMonth
     * @param
     * 
     * @return $this;
     */     
    public function sumMonth($value = 1){

        $this->value_return = $this->value = date('Y-m-d', strtotime("+{$value} month",strtotime($this->value)));        
        
        return $this;
        
    }
    
    /**
     * Executa a subtração de meses
     * 
     * @access public
     * @method minusMonth
     * @param
     * 
     * @return $this;
     */      
    public function minusMonth($value = 1){

        $this->value_return = $this->value = date('Y-m-d', strtotime("-{$value} month",strtotime($this->value)));        
        
        return $this;
        
    }
    
    /**
     * Executa a soma de anos a data corrente da classe
     * 
     * For PHP < 5.3
     * 
     * @access public
     * @method sumYear
     * @param
     * 
     * @return $this;
     */      
    public function sumYear($value = 1){
        
        $this->value_return = $this->value = date('Y-m-d', strtotime("+{$value} year",strtotime($this->value)));        
        
        return $this;
        
    }
    
    /**
     * Executa a subtração de anos a data corrente da classe
     * 
     * @access public
     * @method minusYear
     * @param
     * 
     * @return $this;
     */      
    public function minusYear($value = 1){

        $this->value_return = $this->value = date('Y-m-d', strtotime("-{$value} year",strtotime($this->value)));        
        
        return $this;
        
    }
    
    /**
     * Encontra o intervalo entre duas datas medidas de acordo com o parámetros (intervalo, período)
     * 
     * (PHP 5 >= 5.3.0)
     * 
     * @access public
     * @method between
     * @param
     * 
     * @return $this;
     */      
    public function between($value = null,$type = null){
        
        $date_begin = new \DateTime($this->val());
        
        $date_end = new \DateTime($value->val());
        
        if(empty($type)){
            
            $this->value_return = $date_begin->diff($date_end);       
            
        }
        else {
            
            $this->value_return = new \DatePeriod($date_begin, new \DateInterval($type) ,$date_end);    
            
        }
        
        return $this;
        
    }
    
    /**
     * Retorna o por extenso o valor do mes data da classe
     *
     * 
     * @access public
     * @method stringMonth
     * @param
     * 
     * @return $this;
     */      
    public function stringMonth($value = null){
        
        
        if(empty($value)) {
            
            $this->value_return = $this->month[$this->get('m')];
            
        }
        else {
            
            $this->value_return = $this->month[$value];
            
        }
        
        return $this;
        
    }
    
    /**
     * Retorna a data por string
     * 
     * @access public
     * @method stringMonth
     * @param
     *      
     *  Known format   : 'A' = 'Friday'            ( A full textual representation of the day )
     *  Known format   : 'B' = 'December'          ( Full month name, based on the locale )
     *  Known format   : 'H' = '11'                ( Two digit representation of the hour in 24-hour format )
     *  Known format   : 'I' = '11'                ( Two digit representation of the hour in 12-hour format )
     *  Known format   : 'M' = '24'                ( Two digit representation of the minute )
     *  Known format   : 'S' = '44'                ( Two digit representation of the second )
     *  Known format   : 'U' = '48'                ( Week number of the given year, starting with the first Sunday as the first week )
     *  Known format   : 'W' = '48'                ( A numeric representation of the week of the year, starting with the first Monday as the first week )
     *  Known format   : 'X' = '11:24:44'          ( Preferred time representation based on locale, without the date )
     *  Known format   : 'Y' = '2010'              ( Four digit representation for the year )
     *  Known format   : 'Z' = 'GMT Standard Time' ( The time zone offset/abbreviation option NOT given by %z (depends on operating system) )
     *  Known format   : 'a' = 'Fri'               ( An abbreviated textual representation of the day )
     *  Known format   : 'b' = 'Dec'               ( Abbreviated month name, based on the locale )
     *  Known format   : 'c' = '12/03/10 11:24:44' ( Preferred date and time stamp based on local )
     *  Known format   : 'd' = '03'                ( Two-digit day of the month (with leading zeros) )
     *  Known format   : 'j' = '337'               ( Day of the year, 3 digits with leading zeros )
     *  Known format   : 'm' = '12'                ( Two digit representation of the month )
     *  Known format   : 'p' = 'AM'                ( UPPER-CASE "AM" or "PM" based on the given time )
     *  Known format   : 'w' = '5'                 ( Numeric representation of the day of the week )
     *  Known format   : 'x' = '12/03/10'          ( Preferred date representation based on locale, without the time )
     *  Known format   : 'y' = '10'                ( Two digit representation of the year )
     *  Known format   : 'z' = 'GMT Standard Time' ( Either the time zone offset from UTC or the abbreviation (depends on operating system) )
     *  Known format   : '%' = '%'                 ( A literal percentage character ("%") )
     *  Unknown format : 'C'                       ( Two digit representation of the century (year divided by 100, truncated to an integer) )
     *  Unknown format : 'D'                       ( Same as "%m/%d/%y" )
     *  Unknown format : 'E'
     *  Unknown format : 'F'                       ( Same as "%Y-%m-%d" )
     *  Unknown format : 'G'                       ( The full four-digit version of %g )
     *  Unknown format : 'J'
     *  Unknown format : 'K'
     *  Unknown format : 'L'
     *  Unknown format : 'N'
     *  Unknown format : 'O'
     *  Unknown format : 'P'                       ( lower-case "am" or "pm" based on the given time )
     *  Unknown format : 'Q'
     *  Unknown format : 'R'                       ( Same as "%H:%M" )
     *  Unknown format : 'T'                       ( Same as "%H:%M:%S" )
     *  Unknown format : 'V'                       ( ISO-8601:1988 week number of the given year, starting with the first week of the year with at least 4 weekdays, with Monday being the start of the week )
     *  Unknown format : 'e'                       ( Day of the month, with a space preceding single digits )
     *  Unknown format : 'f'
     *  Unknown format : 'g'                       ( Two digit representation of the year going by ISO-8601:1988 standards (see %V) )
     *  Unknown format : 'h'                       ( Abbreviated month name, based on the locale (an alias of %b) )
     *  Unknown format : 'i'
     *  Unknown format : 'k'                       ( Hour in 24-hour format, with a space preceding single digits )
     *  Unknown format : 'l'                       ( Hour in 12-hour format, with a space preceding single digits )
     *  Unknown format : 'n'                       ( A newline character ("\n") )
     *  Unknown format : 'o'
     *  Unknown format : 'q'
     *  Unknown format : 'r'                       ( Same as "%I:%M:%S %p" )
     *  Unknown format : 's'                       ( Unix Epoch Time timestamp )
     *  Unknown format : 't'                       ( A Tab character ("\t") )
     *  Unknown format : 'u'                       ( ISO-8601 numeric representation of the day of the week )
     *  Unknown format : 'v'
     *
     * 
     * @return $this;
     */      
    public function extensive($value = "%A, %d de %B de %Y"){
        
        return utf8_encode(strftime($value, strtotime($this->get())));;
        
    }    
    
    /**
     * Esse método não foi testado ainda! Método para verificar se o valor da data está vazio ou nulo.   
     * 
     * @access public
     * @method val
     * @param  string
     * 
     * @return string;
     */      
    public function isEmpty(){
        
        if($this->get()==NULL || empty($this->get()) || $this->get()=='19700101'){
            
            $this->value_return = '1';
        
        }
        else {
            
            $this->value_return = '0';
            
        }
        
        return $this;
        
    }
    
    /**
     * Método que retorna o valor da classe ao invéz de retornar a própria classe
     *
     * 
     * @access public
     * @method val
     * @param  string
     * 
     * @return string;
     */    
    public function val($value = null){
        
        return isset($value) ?  gmdate($value,strtotime($this->value_return)) : $this->value_return; 
        
    }
    
    /**
     * Método que retorna o número de dias úteis entre duas datas que poderão ser passadas por parâmetro do array
     * ou a primeira já estar registrada na própria classe
     *
     * 
     * @access public
     * @method getWorkingDays
     * @param  array
     * 
     * @return string;
     */      
    
    function getWorkingDays($value = null) {
        
        $startDate = $this->get("Y-m-d")->val();        

        foreach ($value as $k => $v) {

            switch ($k) {
                case 'start_date':
                    $startDate = new \DateTime($v);
                    break;
                case 'end_date':
                    $endDate = new \DateTime($v);
                    break;
                case 'holidays':
                    $arr_holidays = $v;
                    break;
                default:
                    break;
            }

        }    
        
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($startDate, $interval, $endDate);
        
        $working_days = $weeks_days = 0;

        foreach ($period as $data) {
            
            $d = gregoriantojd($data->format("m"),$data->format("d"),$data->format("Y"));
            
            if(in_array($data->format("Y-m-d"),$arr_holidays)) $holidays++;
                    
            switch (jddayofweek($d,0)) {
                case 0:
                case 6:    
                    $weeks_days++;                    
                    break;
                default:
                    $working_days++;
                    break;
            }
            
            $working_days-=$holidays;

            //echo $data->format("Y-m-d").' - '.jddayofweek($d,1).' - '.jddayofweek($d,0).'<br/>';
            
        }        

        return $working_days;
    }    

}
