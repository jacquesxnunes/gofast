<?php 
/**
 * Classe para uso do curl
 * 
 * @file      curl.class.php
 * @license   
 * @link      
 * @copyright 2017 F71
 * @author    Jacques <jacques@f71.com.br>
 * @package   Curl
 * @access    public  
 * @version:  3.0.0000 - 25/05/2016 - Jacques - Versão Inicial 
 * @todo 
 * @example:  
 * 
 * 
 */
namespace GoFast\Lib;

use GoFast\Kernel\Core;

class Curl extends Core
{

    public static $instance;         

    public  $error_curl;
    public  $error_code_http;
    public  $error_code_curl;
    public  $error_msg;
    
    public  $error;
    public  $config;    
    
    private $_curl;
    private $_protocols;
    private $_user;
    private $_pass;
    private $_domain;
    private $_port;
    private $_path;
    private $_query;
    private $_url;
    private $_response;
    private $_cookie;

    private $cert_file;
    private $curlopt;
    private $curl_error_code = array(
                1 => 'CURLE_UNSUPPORTED_PROTOCOL', 
                2 => 'CURLE_FAILED_INIT', 
                3 => 'CURLE_URL_MALFORMAT', 
                4 => 'CURLE_URL_MALFORMAT_USER', 
                5 => 'CURLE_COULDNT_RESOLVE_PROXY', 
                6 => 'CURLE_COULDNT_RESOLVE_HOST', 
                7 => 'CURLE_COULDNT_CONNECT', 
                8 => 'CURLE_FTP_WEIRD_SERVER_REPLY',
                9 => 'CURLE_REMOTE_ACCESS_DENIED',
                11 => 'CURLE_FTP_WEIRD_PASS_REPLY',
                13 => 'CURLE_FTP_WEIRD_PASV_REPLY',
                14=>'CURLE_FTP_WEIRD_227_FORMAT',
                15 => 'CURLE_FTP_CANT_GET_HOST',
                17 => 'CURLE_FTP_COULDNT_SET_TYPE',
                18 => 'CURLE_PARTIAL_FILE',
                19 => 'CURLE_FTP_COULDNT_RETR_FILE',
                21 => 'CURLE_QUOTE_ERROR',
                22 => 'CURLE_HTTP_RETURNED_ERROR',
                23 => 'CURLE_WRITE_ERROR',
                25 => 'CURLE_UPLOAD_FAILED',
                26 => 'CURLE_READ_ERROR',
                27 => 'CURLE_OUT_OF_MEMORY',
                28 => 'CURLE_OPERATION_TIMEDOUT',
                30 => 'CURLE_FTP_PORT_FAILED',
                31 => 'CURLE_FTP_COULDNT_USE_REST',
                33 => 'CURLE_RANGE_ERROR',
                34 => 'CURLE_HTTP_POST_ERROR',
                35 => 'CURLE_SSL_CONNECT_ERROR',
                36 => 'CURLE_BAD_DOWNLOAD_RESUME',
                37 => 'CURLE_FILE_COULDNT_READ_FILE',
                38 => 'CURLE_LDAP_CANNOT_BIND',
                39 => 'CURLE_LDAP_SEARCH_FAILED',
                41 => 'CURLE_FUNCTION_NOT_FOUND',
                42 => 'CURLE_ABORTED_BY_CALLBACK',
                43 => 'CURLE_BAD_FUNCTION_ARGUMENT',
                45 => 'CURLE_INTERFACE_FAILED',
                47 => 'CURLE_TOO_MANY_REDIRECTS',
                48 => 'CURLE_UNKNOWN_TELNET_OPTION',
                49 => 'CURLE_TELNET_OPTION_SYNTAX',
                51 => 'CURLE_PEER_FAILED_VERIFICATION',
                52 => 'CURLE_GOT_NOTHING',
                53 => 'CURLE_SSL_ENGINE_NOTFOUND',
                54 => 'CURLE_SSL_ENGINE_SETFAILED',
                55 => 'CURLE_SEND_ERROR',
                56 => 'CURLE_RECV_ERROR',
                58 => 'CURLE_SSL_CERTPROBLEM',
                59 => 'CURLE_SSL_CIPHER',
                60 => 'CURLE_SSL_CACERT',
                61 => 'CURLE_BAD_CONTENT_ENCODING',
                62 => 'CURLE_LDAP_INVALID_URL',
                63 => 'CURLE_FILESIZE_EXCEEDED',
                64 => 'CURLE_USE_SSL_FAILED',
                65 => 'CURLE_SEND_FAIL_REWIND',
                66 => 'CURLE_SSL_ENGINE_INITFAILED',
                67 => 'CURLE_LOGIN_DENIED',
                68 => 'CURLE_TFTP_NOTFOUND',
                69 => 'CURLE_TFTP_PERM',
                70 => 'CURLE_REMOTE_DISK_FULL',
                71 => 'CURLE_TFTP_ILLEGAL',
                72 => 'CURLE_TFTP_UNKNOWNID',
                73 => 'CURLE_REMOTE_FILE_EXISTS',
                74 => 'CURLE_TFTP_NOSUCHUSER',
                75 => 'CURLE_CONV_FAILED',
                76 => 'CURLE_CONV_REQD',
                77 => 'CURLE_SSL_CACERT_BADFILE',
                78 => 'CURLE_REMOTE_FILE_NOT_FOUND',
                79 => 'CURLE_SSH',
                80 => 'CURLE_SSL_SHUTDOWN_FAILED',
                81 => 'CURLE_AGAIN',
                82 => 'CURLE_SSL_CRL_BADFILE',
                83 => 'CURLE_SSL_ISSUER_ERROR',
                84 => 'CURLE_FTP_PRET_FAILED',
                84 => 'CURLE_FTP_PRET_FAILED',
                85 => 'CURLE_RTSP_CSEQ_ERROR',
                86 => 'CURLE_RTSP_SESSION_ERROR',
                87 => 'CURLE_FTP_BAD_FILE_LIST',
                88 => 'CURLE_CHUNK_FAILED');     
    

    
    use \GoFast\Lib\Bridge;
    
   /**
    * Método construtor de classe que pode iniciar via array de parâmetros
    * 
    * @access private
    * @method __construct
    * @param
    * 
    * @return $this
    */      
    public function __construct($value = null) {
        
        parent::__construct($value);

        $this->createCoreClass($value);
        
        $this->defaultCookie();

        $this->init($value);
        
        
        
    }    
   
    /**
     * Método que define Define valores default da classe
     * 
     * @access public
     * @method setDefault
     * @param  
     * 
     * @return $this
     */
    public function setDefault() {

        $this->_url = '';
        
        return $this;
    }    
           
    
   /**
    * Método executado no instânciamento da classe para execuções de procedimentos iniciais
    * 
    * @access public
    * @method init
    * @param
    * 
    * @return $this
    */      
   public function init($value = null) {
        
        if(!empty($value)) {
       
            if (is_array($value)) {


                foreach ($value as $k => $v) { 

                    switch ($k) {
                        case 'url':
                            $this->url($v);
                            break;
                        case 'cert_file':
                            $this->cert_file = $v;
                            break;
                        case 'cookie':
                            $this->cookie($v);
                            break;                    
                        default:
                            break;
                    }

                }

            }   
            else {

                $this->url($value);

            }        

            if(!$this->_curl = curl_init()) $this->error->set(_("# [curl] Não foi possível iniciar o curl para a chamada da url {$this->url()}"),E_FRAMEWORK_ERROR); 
            
            $_COOKIE[session_name()]=session_id();

            $this->setOpt($value);
        
        }
        
        return $this;
        
    }
    
   /**
    * Método de definição da url de acesso
    * 
    * @access private
    * @method setUrl
    * @param
    * 
    * @return $this
    */      
    public function url($value = null) {
        
        $flag = array(
                        'query' => '?',
                        'fragment' => '#'
                      ) ;
        
        if(isset($value)) {
            
            $v = parse_url($value);
            
            $this->protocols(isset($v['scheme']) ? $v['scheme'] : '');
            $this->domain(isset($v['host']) ? $v['host'] : '');
            $this->port(isset($v['port']) ? $v['port'] : '');
            $this->user(isset($v['user']) ? $v['user'] : '');
            $this->pass(isset($v['pass']) ? $v['pass'] : '');
            $this->path(isset($v['path']) ? $v['path'] : '');
            $this->query(isset($v['query']) ? $v['query'] : '');
            
            $this->_url = $value; 

            
            return $this;
        
        }
        else {
            
            return $this->_url;
            
        }
        
    }
    
   /**
    * Método de definição de cookies
    * 
    * @access private
    * @method cookie
    * @param
    * 
    * cookie format to the curl
    * string example.com - the domain name
    * boolean FALSE - include subdomains
    * string /foobar/ - path
    * boolean TRUE - send/receive over HTTPS only
    * number 1462299217 - expires at - seconds since Jan 1st 1970, or 0
    * string person - name of the cookie
    * string daniel - value of the cookie
    *     * 
    * @return $this
    */      
   private function cookie($value = null) {
        
        if(isset($value)){
            
            $this->curlopt['curlopt_cookiesession'] = 1;
                    
            if (is_array($value)) {
                
                $this->_cookie[$value['name']] = $value;

            }   
            
            return $this;
        
        }
        else {
            
            return $this->_cookie;
            
        }
        
    }     
    
   /**
    * Método de definição de cookies
    * 
    * @access private
    * @method cookie
    * @param
    * 
    *    that start with `#` are treated as comments.
    *
    *    Each line that each specifies a single cookie consists of seven text fields
    *    separated with TAB characters.
    *
    *    |Field| Type  | Example     | Meaning                                       |
    *    |---|---------|-------------|-----------------------------------------------|
    *    | 0 | string  | example.com | Domain name                                   |
    *    | 1 | boolean | FALSE       | Include subdomains                            |
    *    | 2 | string  | /foobar/    | Path                                          |
    *    | 3 | boolean | TRUE        | Send/receive over HTTPS only                  |
    *    | 4 | number  | 1462299217  | Expires at – seconds since Jan 1st 1970, or 0 |
    *    | 5 | string  | person      | Name of the cookie                            |
    *    | 6 | string  | daniel      | Value of the cookie                           |
    *     * 
    * @return $this
    */      
    public function defaultCookie($value = null) {
        
        $this->_cookie['default']['domain'] = '';
        $this->_cookie['default']['subdomains'] = 'FALSE';
        $this->_cookie['default']['path'] = '/';
        $this->_cookie['default']['over_https'] = 'TRUE';
        $this->_cookie['default']['expires'] = 1462299217;
        $this->_cookie['default']['name'] = 'default';
        $this->_cookie['default']['value'] = 'cookie';
        
        return $this;
        
    }  
    
   /**
    * Método de definição de cookies
    * 
    * @access private
    * @method cookie
    * @param  string Y-m-d H:i:s (UTC)
    * 
    * @return $this
    */      
    private function cookieDateConvert($value = null) {
        
        try {
            
            $d = \DateTime::createFromFormat(
                                                'Y-m-d H:i:s',
                                                '22-09-2008 00:00:00',
                                                new \DateTimeZone('UTC')
                                            );

            if ($d === false) $this->error->set(sprintf(_('Formato de data inválido para definição de cookie'),$file),E_FRAMEWORK_ERROR); 
            
            $this->setValue(1);
        
        }     
        catch (\Exception $ex) {
           
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        } 
        
        return $d === false? 0 : $d->getTimestamp();
        
    }    
    
    /**
     * Serializa todas as classes do framework em uma sessão de usuário para salvar seu estado atual
     * 
     * 
     * @access public
     * @method cookieSave
     * @param
     * 
     * @return $this    
     * 
     */     
    private function cookieSave(){
        
      try {
          
            foreach( $_COOKIE as $key => $value ) {
                $this->cookie_string .= "$key=$value;";
            };
            
            foreach ($this->_cookie as $k_1 => $v_1) {
                
                if($k_1!=='default'){
                    
                    $this->cookie_string .= "{$this->_cookie[$k_1]['name']}={$this->_cookie[$k_1]['value']};";

                    foreach ($this->_cookie['default'] as $k_2 => $v_2) {
                        
                        if($k_2=='expires' && isset($this->_cookie[$k_1][$k_2])){
                            $this->_cookie[$k_1][$k_2] = $this->convertDateCookie($this->_cookie[$k_1][$k_2]);
                        }
                        elseif($k_2=='domain' && !isset($this->_cookie[$k_1][$k_2])) {
                            $this->_cookie[$k_1][$k_2] = $this->domain();
                        }
                        elseif(!isset($this->_cookie[$k_1][$k_2])) {
                            $this->_cookie[$k_1][$k_2] = $this->_cookie['default'][$k_2];
                        }

                    }

                    $buffer .=    "# Netscape HTTP Cookie File\n"
                                . "# http://curl.haxx.se/docs/http-cookies.html\n"
                                . "# This file was generated by libcurl! Edit at your own risk.\n"
                                . "{$this->_cookie[$k_1]['domain']}\t"
                                . "{$this->_cookie[$k_1]['subdomains']}\t"
                                . "{$this->_cookie[$k_1]['path']}\t"
                                . "{$this->_cookie[$k_1]['over_https']}\t"
                                . "{$this->_cookie[$k_1]['expires']}\t"
                                . "{$this->_cookie[$k_1]['name']}\t"
                                . "{$this->_cookie[$k_1]['value']}\t\n";
                    
                }
                

            }

            $this->file['name'] = sha1("cookie-{$session}");
            $this->file['dir'] = sys_get_temp_dir().DS;
            $this->file['path'] = $this->file['dir'] . $this->file['name'];
            
 //           if(!file_put_contents($this->file['path'],$buffer)) $this->error->set(sprintf(_('Não foi possível salvar o cookie da classe curl em %s'),$this->file['path']),E_FRAMEWORK_ERROR);    
            
            $this->setValue(1);
            
       } 
       catch (\Exception $ex) {
           
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
       }         
        
        return $this;
        
    }     
    
   /**
    * Método executado no instânciamento da classe para execuções de procedimentos iniciais
    * 
    * @access private
    * @method setOpt
    * @param
    * 
    * @return $this
    */      
    private function setCurlDefault() {
        
        //Define o tempo máximo para uma resposta do sistema de cache de dns
        $this->curlopt['post'] = 0;
        //Define o tempo máximo para uma resposta do sistema de cache de dns
        $this->curlopt['dns_cache_timeout'] = $this->config->title('curl')->key('dns_cache_timeout')->val() ? $this->config->title('curl')->key('dns_cache_timeout')->val() : 2000;
        //Define o tempo máximo para se estabelecer uma conexão com o servidor
        $this->curlopt['connecttimeout_ms'] = $this->config->title('curl')->key('connecttimeout_ms')->val() ? $this->config->title('curl')->key('connecttimeout_ms')->val() : 10000;
        //Define o tempo máximo para se estabelecer uma conexão com o servidor
        $this->curlopt['connecttimeout'] = $this->config->title('curl')->key('connecttimeout')->val() ? $this->config->title('curl')->key('connecttimeout')->val() : 2000;
        //Define o tempo máximo de execução do script
        $this->curlopt['timeout'] = $this->config->title('curl')->key('timeout')->val() ? $this->config->title('curl')->key('timeout')->val() : 10;
        //Define o Total de bytes em um dado intervalo de tempo
        $this->curlopt['low_speed_limit'] = $this->config->title('curl')->key('low_speed_limit')->val() ? $this->config->title('curl')->key('low_speed_limit')->val() : -1;
        //Define o range de tempo para transferência de um determinado número de byte definidos em CURLOPT_LOW_SPEED_LIMIT
        $this->curlopt['low_speed_time'] = $this->config->title('curl')->key('low_speed_time')->val() ? $this->config->title('curl')->key('low_speed_time')->val() : -1;
        //Define para replicar os cookies da sessão em execução.
        $this->curlopt['curlopt_cookiesession'] = 1;
  
    }    
    
   /**
    * Método executado no instânciamento da classe para execuções de procedimentos iniciais
    * 
    * @access private
    * @method setOpt
    * @param
    * 
    * 
    * cookie format to the curl
    * string example.com - the domain name
    * boolean FALSE - include subdomains
    * string /foobar/ - path
    * boolean TRUE - send/receive over HTTPS only
    * number 1462299217 - expires at - seconds since Jan 1st 1970, or 0
    * string person - name of the cookie
    * string daniel - value of the cookie
    * 
    * @return $this
    */      
    private function setOpt($value = null) {
        
        $this->setCurlDefault();
        
        if (is_array($value)) {
            
            foreach ($value as $k => $v) { 
                
                switch ($k) {
                    case 'curlopt_postfields':
                        $this->curlopt['postfields'] = $v;
                        break;
                    case 'curlopt_post':
                        $this->curlopt['post'] = $v;
                        break;
                    case 'curlopt_cookiesession':
                        $this->curlopt['curlopt_cookiesession'] = $v;
                        break;                        
                    default:
                        break;
                }

            }

        }         
        
        if($_SERVER['HTTPS'] && $this->_protocols == 'https' && $this->cert_file) {
            
            if(!file_exists(ROOT_CERT.$file)) $this->error->set("# [curl] apesar do nome de arquivo de certificado definido não foi possível localiza-lo em ".ROOT_CERT.$file,E_FRAMEWORK_ERROR); 

            curl_setopt($_curl, CURLOPT_CAINFO, ROOT_CERT.$file);                
            curl_setopt($_curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($_curl, CURLOPT_SSL_VERIFYHOST,  2);            

            $this->_protocols = 'https';

        }
        
        curl_setopt($this->_curl, CURLOPT_HEADER, 0);
        curl_setopt($this->_curl, CURLOPT_VERBOSE, 1);
        curl_setopt($this->_curl, CURLOPT_RETURNTRANSFER, 1);                                           // Define o tipo de transferência (Padrão: 1)
        curl_setopt($this->_curl, CURLOPT_TIMEOUT, 15);
        curl_setopt($this->_curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->_curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->_curl, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($this->_curl, CURLOPT_POST, $this->curlopt['post']);                              // Habilita o protocolo POST
        curl_setopt($this->_curl, CURLOPT_REFERER,"{$this->_protocols}://{$this->_domain}/");
        curl_setopt($this->_curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($this->_curl, CURLOPT_ENCODING, 'gzip');
        
        curl_setopt($this->_curl, CURLOPT_CONNECTTIMEOUT_MS, $this->curlopt['connecttimeout_ms']);                   
        curl_setopt($this->_curl, CURLOPT_DNS_CACHE_TIMEOUT, $this->curlopt['dns_cache_timeout']);  
        curl_setopt($this->_curl, CURLOPT_CONNECTTIMEOUT, $this->curlopt['connecttimeout']);            // Tempo aguardado para considerar falha na conexão
        curl_setopt($this->_curl, CURLOPT_TIMEOUT, $this->curlopt['timeout']);                          // Tempo máximo de execução do script
        curl_setopt($this->_curl, CURLOPT_LOW_SPEED_TIME, $this->curlopt['low_speed_time']);            // Range de tempo para transferência de um determinado número de byte definidos em CURLOPT_LOW_SPEED_LIMIT
        curl_setopt($this->_curl, CURLOPT_LOW_SPEED_LIMIT, $this->curlopt['low_speed_limit']);          // Total de bytes em um dado intervalo de tempo
        
        //curl_setopt ($ch, CURLOPT_POSTFIELDS, "class=construct&table={$table}&alias={$alias}");       // Define os parâmetros que serão enviados (usuário e senha por exemplo)
        //curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');                                            // Imita o comportamento patrão dos navegadores: manipular cookies
        //curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookies.txt');
            
        if($this->curlopt['curlopt_cookiesession'] = 1) {
           
            $this->cookieSave();

//            curl_setopt($this->_curl, CURLOPT_COOKIESESSION, TRUE );
//            curl_setopt($this->_curl, CURLOPT_COOKIEJAR, $this->_file['path'] );                        // Define o arquivo de cookies de envio
//            curl_setopt($this->_curl, CURLOPT_COOKIEFILE, $this->_file['path'] );                       // Define o arquivo de cookies de retorno
//            curl_setopt($this->_curl, CURLOPT_HTTPHEADER, array("Cookie: logado=275"));                 // Seta os cookiee com um array
            curl_setopt($this->_curl, CURLOPT_COOKIE, $this->cookie_string);                              // Seta os cookies com uma string  
            
        }
        
        curl_setopt($this->_curl, CURLOPT_VERBOSE, TRUE);
        curl_setopt($this->_curl, CURLOPT_URL, $this->_url);                                            // Define a URL original (do formulário de login)
        curl_setopt($this->_curl, CURLOPT_PORT, $this->_port);                                          // Define a URL original (do formulário de login)
        
        if(!empty($this->curlopt['postfields'])) curl_setopt($this->_curl, CURLOPT_POSTFIELDS, $this->curlopt['postfields']);
        
        return $this;
        
    }
    
   /**
    * Método para execução do curl
    * 
    * @access exec
    * @method __construct
    * @param
    * 
    * @return $this
    */      
    public function exec() {
        
        $this->_response = curl_exec($this->_curl); // Executa a requisição

        $this->error_curl = curl_getinfo($this->_curl);

        $this->error_code_http = (int)curl_getinfo($this->_curl, CURLINFO_HTTP_CODE);

        $this->error_code_curl = curl_errno($this->_curl);
        
        return $this;
        
    }    
    
   /**
    * Método para execução do curl
    * 
    * @access exec
    * @method __construct
    * @param
    * 
    * @return $this
    */      
    public function response() {
        
        switch ($this->error_code_http) {
            case 200:

                if(is_array($this->_response)) {

                    $e = json_decode($this->_response,true);

                    foreach ($e as $key => $error) {

                        $this->error->set("# [curl] {$error['message']}",E_FRAMEWORK_WARNING);

                    }   

                }

                break;
            case 400:
               $this->error->set(_("# [curl] O servidor não cosseguiu processar a requisição do arquivo {$this->url()}"),E_FRAMEWORK_ERROR); 
               break;
            case 403:
               $this->error->set(_("# [curl] Acesso negado na execução do arquivo {$this->url()}"),E_FRAMEWORK_ERROR); 
               break;
            case 404:
               $this->error->set(_("# [curl] Não foi localizado o arquivo em {$this->url()}"),E_FRAMEWORK_ERROR); 
               break;
            case 500: 
               $this->error->set(_("# [curl] Erro no servidor ao executar o arquivo em {$this->url()}"),E_FRAMEWORK_ERROR); 
               break;
            default:
               $this->error->set(_("# [curl] Erro ($this->error_code_http) não documentado na execução do arquivo {$this->url()}"),E_FRAMEWORK_NOTICE); 
               break;
        }
        
        curl_close($this->_curl);
        
        return $this->_response;
            
        
    }    
    
   /**
    * Método de definição do domínio
    * 
    * @access public
    * @method protocols
    * @param
    * 
    * @return $this
    */      
    public function protocols($value = null) {
        
        if(isset($value)) {
            
            $this->_protocols = $value;
            
            return $this;
        
        }
        else {
            
            return $this->_protocols;
            
        }
        
    }      

    
   /**
    * Método de definição do domínio
    * 
    * @access public
    * @method domain
    * @param
    * 
    * @return $this
    */      
    public function domain($value = null) {
        
        if(isset($value)) {
            
            $this->_domain = $value;
            
            return $this;
        
        }
        else {
            
            return $this->_domain;
            
        }
        
    }  

    
   /**
    * Método de definição do usuário
    * 
    * @access public
    * @method user
    * @param
    * 
    * @return $this
    */      
    public function user($value = null) {
        
        if(isset($value)) {
            
            $this->_user = $value;
            
            return $this;
        
        }
        else {
            
            return $this->_user;
            
        }
        
    }  
    
   /**
    * Método de definição do password
    * 
    * @access public
    * @method pass
    * @param
    * 
    * @return $this
    */      
    public function pass($value = null) {
        
        if(isset($value)) {
            
            $this->_pass = $value;
            
            return $this;
        
        }
        else {
            
            return $this->_pass;
            
        }
        
    } 
    
    
   /**
    * Método de definição da porta de comunicação
    * 
    * @access public
    * @method port
    * @param
    * 
    * @return $this
    */      
    public function port($value = null) {
        
        if(isset($value)) {
            
            $this->_port = $value;
            
            return $this;
        
        }
        else {
            
            return $this->_port;
            
        }
        
    }
    
   /**
    * Método de definição do caminho do recurso
    * 
    * @access public
    * @method path
    * @param
    * 
    * @return $this
    */      
    public function path($value = null) {
        
        if(isset($value)) {
            
            $this->_path = $value;
            
            return $this;
        
        }
        else {
            
            return $this->_path;
            
        }
        
    }   
    
   /**
    * Método de definição da query da url
    * 
    * @access public
    * @method query
    * @param
    * 
    * @return $this
    */      
    public function query($value = null) {
        
        if(isset($value)) {
            
            $this->_query = $value;
            
            return $this;
        
        }
        else {
            
            return $this->_query;
            
        }
        
    }    
    
}
