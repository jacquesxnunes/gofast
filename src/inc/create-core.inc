<?php

/**
 * Código para incorporação e criação de Core
 * 
 * @file      create-core.inc.php
 * @license   http://www.gnu.org/licenses/gpl-3.0.txt GNU General Public License
 * @link      
 * @copyright 2016 F71
 * @author    Jacques <jacques@f71.com.br>
 * @package   createCoreClass
 * @access    public  
 * @version:  3.00.7788L - 03/05/2016 - Jacques - Versão Inicial
 * @version:  3.00.7788L - 07/05/2016 - Jacques - Adicionado validação de inclusão de arquivo
 * @version:  3.00.0170F - 11/07/2016 - Jacques - Adicionado opção de carga de arquivo de configuração com nome personalizado
 * @todo 
 * @example:   
 * 
 * 
 */

try {

    if(empty($this->getIdMaster())){
        \GoFast\Lib\Config::_id(\GoFast\Lib\Config::ID_FW);
        \GoFast\Lib\Config::_setFile(\GoFast\Lib\Config::PATH_FW);
        \GoFast\Lib\Config::_title('master_domain');
        \GoFast\Lib\Config::_key(DOMAIN);
        $value['id_master'] = \GoFast\Lib\Config::_val();
    }
    else {
        $value['id_master'] = $this->getIdMaster();
    }


    $value['name_master'] = $this->getNameMaster();    
    $value['calling_class'] = $this->getStaticClassName();

    if (!include_once($file = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'const.inc')) die("# Não foi possível incluir {$file} a partir de " . __FILE__);
    if (!include_once($file = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'functions.inc')) die("Não foi possível incluir {$file} a partir de " . __FILE__);

    if (!isset($this->version) && property_exists($this, 'version') && (new ReflectionProperty($this,'version'))->isPublic()) {

        $value['core'] = 'Version';

        $this->version = (new \GoFast\Lib\Version($value))->getInstance();

        if (!is_object($this->version)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->version");
    }

    if (!isset($this->git) && property_exists($this, 'git') && (new ReflectionProperty($this,'git'))->isPublic()) {

        $value['core'] = 'Git';

        $this->git = (new \GoFast\Lib\Git($value))->getInstance();

        if (!is_object($this->git)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->git");
    }

    if (!isset($this->error) && property_exists($this, 'error') && (new ReflectionProperty($this,'error'))->isPublic()) {

        $value['core'] = 'Error';        

        $this->error = (new \GoFast\Lib\Error($value))->getInstance();

        if (!is_object($this->error)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->error");
    }

    if (!isset($this->config) && property_exists($this, 'config') && (new ReflectionProperty($this,'config'))->isPublic()) {
        
        $value['core'] = 'Config';

        $this->config = (new \GoFast\Lib\Config($value))->getInstance();

        if (!is_object($this->config)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->config");

        $value['name_master'] = $this->config->id(\GoFast\Lib\Config::ID_FW)->setFile(ROOT_ETC . \GoFast\Lib\Config::ID_FW)->title('master_name')->key($value['id_master'])->val();
        
        $file = $value['name_master'].".ini";           
   
        if(!file_exists($path = ROOT_ETC . $file))  
            $this->error->set("# Não foi possível definir o arquivo de configuração [{$path}] para o domínio [{$_SERVER['HTTP_HOST']}], id_master = {$value['id_master']}, name_master = {$value['name_master']}",E_FRAMEWORK_ERROR);                

        $this->config->id($value['name_master'])->setFile($path);

    }

    if (!isset($this->construct) && property_exists($this, 'construct') && (new ReflectionProperty($this,'construct'))->isPublic()) {

        $value['core'] = 'Construct';        

        $this->construct = (new \GoFast\Lib\Construct($value))->getInstance();

        if (!is_object($this->construct)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->construct");
    }    

    if (!isset($this->db) && property_exists($this, 'db')) {

        $value['core'] = 'Db'; 

        /**
         * A questão crítica da definição do charset no instânciamento da classe db está relacionada aos procedimentos que retornam json
         * e que se retornarem charset diferente de utf-8 irão falhar.
         */
        //$this->db = new \GoFast\Lib\Db(method_exists($this, 'getCharset') ? array('charset' => $this->getCharset()) : array());
        $this->db = new \GoFast\Lib\Db($value);

        if (!is_object($this->db)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->db");
    }


    if (!isset($this->user) && property_exists($this, 'user')) {

        $value['core'] = 'User'; 

        $this->user = (new \GoFast\Auth\User($value))->getInstance();

        if (!is_object($this->user)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->user");
    }


    if (!isset($this->date) && property_exists($this, 'date') && (new ReflectionProperty($this,'date'))->isPublic()) {

        $value['core'] = 'Date'; 

        $this->date = new \GoFast\Lib\Date($value);

        if (!is_object($this->date)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->date");
    }



    if (!isset($this->file) && property_exists($this, 'file') && (new ReflectionProperty($this,'file'))->isPublic()) {

        $value['core'] = 'File'; 

        $this->file = new \GoFast\Lib\File($value);

        if (!is_object($this->file)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->file");
    }

    if (!isset($this->cmbBox) && property_exists($this, 'cmbBox') && (new ReflectionProperty($this,'cmbBox'))->isPublic()) {

        $value['core'] = 'CmbBox'; 
        $value['db'] = $this->db;
        
        $this->cmbBox = new \GoFast\Lib\CmbBox($value);

        //$this->cmbBox->link($this->db);

        if (!is_object($this->cmbBox)) die("Não foi possível instânciar a classe {$value['class']}->createCoreClass()->cmbBox");
    }

    if (!isset($this->log) && property_exists($this, 'log') && (new ReflectionProperty($this,'log'))->isPublic()) {

        $value['core'] = 'Log'; 

        $this->log = (new \GoFast\Lib\Log($value))->getInstance();

        if (!is_object($this->log)) die("Não foi possível instânciar a classe {$value['class']}->createCoreClass()->log");
    }

    if (!isset($this->lib) && property_exists($this, 'lib') && (new ReflectionProperty($this,'lib'))->isPublic()) {

        $value['core'] = 'Lib'; 

        $this->lib = new \GoFast\Lib\Lib($value);

        if (!is_object($this->lib)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->lib");
    }


    if (!isset($this->curl) && property_exists($this, 'curl') && (new ReflectionProperty($this,'curl'))->isPublic()) {

        $value['core'] = 'Curl'; 

        $this->curl = (new \GoFast\Lib\Curl($value))->getInstance();

        if (!is_object($this->curl)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->curl");
    }

    //    if(!isset($this->memcache) && property_exists($this, 'memcache')){ 
    //        
    //        if(class_exists('Memcache') && $this->config->title('memcache')->key('class_expiration_times')->val()) {
    //            
    //            $this->memcache = new \Memcache();
    //
    //            if($this->memcache->connect(
    //                                     strlen($this->config->title('memcache')->key('connect')->val()) ? $this->config->title('memcache')->key('connect')->val() : 'localhost',
    //                                     strlen($this->config->title('memcache')->key('port')->val()) ? $this->config->title('memcache')->key('port')->val() : '11211'
    //                                    )) die("# Não foi possível se conectar ao serviço memcache");
    //
    //        }            
    //        
    //    }


    // if (!isset($this->accessControl) && property_exists($this, 'accessControl') && (new ReflectionProperty($this,'accessControl'))->isPublic()) {

    //     $value['core'] = 'AccessControl'; 

    //     $this->accessControl = (new \GoFast\Lib\AccessControl($value))->getInstance();

    //     if (!is_object($this->accessControl)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->accessControl");
    // }

    $this->setValue(1);
} catch (\Exception $ex) {

    $arr = debug_backtrace(2);

    $trace = array_reverse($arr);

    print_array($trace);

    // print_array($ex);

    // foreach ($trace as $key => $error) {

    //     $stack = sprintf("%03d", $key);

    //     $this->log->info(vsprintf("%02d", 0) . " INFO     - # [tracert:{$stack}] {$error['class']}->{$error['function']}() call in {$error['file']}:{$error['line']}");
    // }

    die("# Erros interromperam o processo no instânciamento dos arquivos de core");
}
