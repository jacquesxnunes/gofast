<?php

$class = $_REQUEST['class'];

if(isset($class)) {
    
    if($class=='construct') {
        
        $path = ROOT_LIB.$class.'.class.php';
        
    }
    else {
        
        $path = ROOT_APP_CONTROLLER.$class.'.class.php';
        
    }

    if(!file_exists($path)) die("A classe não existe no caminho {$path}");
    
    include($path);
    
    exit();
}