<?php 

/**
 * Classe para manipulação de string
 * 
 * @version: 3.00.0000 - 00/00/0000 - Jacques - 
 * 
 * https://packagist.org/packages/spatie/string
 * 
 * @Jacques
 */
namespace GoFast\Lib;

class String {
    
    private $value_return;
    private $value;
    
    public function __construct($value = null) {
        
        
    }

    /**
     * Define um valor de retorno caso a referência ao objeto seja feita em um procedimento de retorno de valor
     * 
     * @access protected
     * @method __toString
     * @param 
     * 
     * @see 
     * 
     * @return $this;
     */     
    protected function __toString()
    {
        
        return (string)$this->value_return; 
        
    }
    
    /**
     * Define valores default para a classe data
     * 
     * @access public
     * @method setDefault
     * @param 
     * 
     * @see 
     * 
     * @return $this;
     */     
    public function setDefault(){
        
        $this->value = $this->value_return = '';
        
        return $this;        
        
    }
    
    /**
     * Define o valor de uma string
     * 
     * @access public
     * @method set
     * @param 
     * 
     * @see 
     * 
     * @return $this;
     */     
    public function set($value = null){
        
        
    }    
    

}
