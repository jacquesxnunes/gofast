<?php

/**
 * Classe para conexão a banco de dados MySql moldando a conexão a orientação a objetos 
 * 
 * @file      MySqlClass.php
 * @license   http://www.gnu.org/licenses/gpl-3.0.txt GNU General Public License
 * @link      
 * @copyright 2015 F71
 * @author    Jacques <jacques@f71.com.br>
 * @package   MySqlClass
 * @access    public  
 * @version: 3.00.L0000 - 25/05/2016 - Jacques - Versão Inicial 
 * @version: 1.00.L0000 - 00/00/0000 - Jacques - Versão Beta da classe de conexão
 * @version: 1.01.L0000 - 00/00/0000 - Jacques - Acrescentado o método kill para evitar muitas conexões 
 * @version: 1.02.L0000 - 00/00/0000 - Jacques - Acrescentado parámetro no método getQuery($index = NULL)
 * @version: 1.03.L0000 - 00/00/0000 - Jacques - Acrescentado o vetor de private  $query_string_default
 * @version: 1.04.L0000 - 00/00/0000 - Jacques - Acrescentado o vetor collection mais métodos relacionados a ele 03-07-2015
 * @version: 1.05.L0000 - 00/00/0000 - Jacques - Acrescentado método de montagem de string de buscas 06/07/2015
 * @version: 1.06.L0000 - 00/00/0000 - Jacques - Alterado o método setSearch para montagem da query direto no método setQuery
 * @version: 1.07.L0000 - 00/00/0000 - Jacques - Adicionado novo método setQuery2 para montagem mais intuitiva da query
 * @version: 1.08.L0000 - 14/09/2015 - Jacques - setQuery recebeu a funcionalidade de setQuery2
 * @version: 1.09.L0000 - 14/09/2015 - Jacques - Criada a função de retorno de array no lugar do rs, criada também a função de retorno de json e controle de versão interno
 * @version: 1.10.L0000 - 22/09/2015 - Jacques - Adicionado verificação de elemento em um array do método getRow() e adicionado o método getQueryLast()
 * @version: 1.11.L0000 - 06/10/2015 - Jacques - Acrescentado método para controle de operações via RollBack
 * @version: 1.12.L0000 - 12/11/2015 - Jacques - Acrescentado verificação de campo nulo na query de inserção para caso algum campo possua valor vazio ou nulo então seta 0 para valores numéricos
 * @version: 1.13.L4479 - 27/11/2015 - Jacques - Aprimoramento do método setRs() com fechamento da conexão ao Banco de Dados
 * @version: 1.14.L4511 - 30/11/2015 - Jacques - Criação do método de adição de campos calculados por linha, subgrupos e geral retornados para a coleção
 * @version: 1.15.L4601 - 03/12/2015 - Jacques - Adicionado ao método setCol() a opção de adicionar campo com atribuição de valor numérico além de índices de vetor
 * @version: 1.16.L4669 - 07/12/2015 - Jacques - Adicionado ao método setCol() a opção de adicionar string para comparação
 * @version: 1.17.L4778 - 09/12/2015 - Jacques - Correção no método setCol() que estava retornando campo de registro sem adição de referência a vetor com a condição preg_match("/\"([^\"]*)\"/",$matches[0]) || preg_match("([0-9.]+)",$matches[0])
 * @version: 1.18.L4778 - 02/01/2016 - Jacques - Adicionando arquivo metodo para carregar arquivo com valores de conexão ao banco de dados
 * @version: 1.19.L5676 - 21/01/2016 - Jacques - Método setQuery adicionado aos métodos MakeField* para evitar trabalhar com retorno de valor
 * @version: 1.20.L5884 - 26/01/2016 - Jacques - Adicionado método que obtem o número de linhas afetadas em operação de INSERT, UPDATE e DELETE
 * @version: 1.20.L6068 - 29/01/2016 - Jacques - Adicionado a verificação de NULL nos métodos de montagem de query makeFieldUpdate e makeFieldInsert
 * @version: 1.20.L6460 - 16/02/2016 - Jacques - Adicionado a verificação da carga do arquivo de configuração da classe
 * @version: 1.20.L6882 - 22/02/2016 - Jacques - Adicionado o total de linhas retornadas na execução do método setRs
 * @version: 1.20.L8762 - 01/04/2016 - Jacques - Acrescentado a posibilidade de leitura do setup.ini de forma vetorizada por índice de agrupamento
 * @version: 1.20.L8762 - 20/06/2016 - Jacques - Acrescentado método de validação para execução da query.
 * @version: 1.20.F0224 - 08/09/2016 - Jacques - Incrementado o método get[] para receber dois parâmetros e retornar chave/valor
 * @version: 1.20.F0239 - 10/10/2016 - Jacques - Adicionado o registro em log da última query executada
 * @version: 1.20.F0246 - 19/10/2016 - Jacques - Adicionado o registro de uma propriedade para controle do código de página default ou definido pelo usuário e o setQuery permitir métodos encadeados
 * @version: 1.20.F0000 - 21/02/2017 - Jacques - Fix do problema de acentuamento para o tipo de conexão ao banco de dados mysqli usando em setEnvironment $this->conn[$this->getNameMaster()]->set_charset("utf8") e ajustando outros sets para mysqli
 * 
 * @todo 
 * @example:  
 *  
 * 
 *  
 * setNumRow,getNumRow
 * 
 * OBS: Implementar a op??o de getRow das classes baseadas nos campos do SELECT passados para setRs para uso din?mico
 * 
 * @author jacques@f71.com.br
 * 
 * @copyright www.f71.com.br
 */

namespace GoFast\Lib;

use \GoFast\Kernel\Core;

class Db extends Core {

    
    /* 
     * Passar para carga de arquivo MySqlClass.ini os valores de set de conex?o 
     */

    public    $config;
    public    $error;
    public    $crypt;
    public    $_memcache;
    public    $row;
    public    $rs;
    public    $origin_class;

    private   $pdo;
    private   $conn;
    private   $host = "";
    private   $dbname = "";
    private   $_user = "";  
    private   $pass = "";  
    private   $definer = "";            
    private   $connected = 0;
    private   $result_memory = 0;
    private   $search = '';
    private   $versao = 'tag_ver';
    private   $num_rows = 0;
    private   $dbcharset = ['utf-8' => 'utf8'];
    private   $group_concat_max_len = 1048576;
    private   $tag;

    private   $query_string_value = array(
                             QUERY => '',
                             SELECT => 'SELECT ',
                             FROM => 'FROM ',
                             UPDATE => 'UPDATE ',
                             INSERT => 'INSERT ',
                             DELETE => 'DELETE',
                             WHERE => 'WHERE ',
                             GROUP => 'GROUP BY',
                             HAVING => 'HAVING ',
                             ORDER => 'ORDER BY ',
                             LIMIT => 'LIMIT ',
                             CALL => 'CALL ');    

    private   $query_string_default = array(
                             QUERY => '',
                             SELECT => '',
                             FROM => '',
                             UPDATE => '',
                             INSERT => '',
                             DELETE => '',
                             WHERE => '',
                             GROUP => '',
                             HAVING => '',
                             ORDER => '',
                             LIMIT => '',
                             CALL => '');    
    
    private   $query_string = [];   
    private   $last_query_string = '';    
    private   $collection = [];   
    private   $result = [];   
    private   $array = [];
    private   $key = [];
    
    use \GoFast\Lib\Bridge;

    /*
     * PHP-DOC 
     * 
     * @name __construct
     * 
     * @internal - Executa alguns métodos na construção da classe
     */    
    public function __construct($value = null){
          
        parent::__construct($value);  

        $this->createCoreClass($value);
                
        $this->setDefault();     
        
        if(!$this->connect()->isOk()) $this->error->set(_("# Não foi possível realizar a conexão ao banco de dados"),E_FRAMEWORK_ERROR);

        
    }  
             
    
    /**
     * Método que para ferificar se chegou ao final do array
     * 
     * @access public
     * @method eoa
     * @param  
     * 
     * @return int
     */      
    public function eoa() {

        return $this->eof;

    }      
    
    
    
    /**
     * Método que define o tamanho máximo de concatenção de string via tag query GROUP_CONCAT
     * 
     * @access public
     * @method setGroupConcatMaxLen
     * @param  
     * 
     * @return $this
     */      
    public function setGroupConcatMaxLen($value = null) { 
        
        $this->group_concat_max_len = $value;
        
        return $this;

    }
       
    
    /**
     * Método que define Define valores default da classe
     * 
     * @access public
     * @method setDefault
     * @param  
     * 
     * @return $this
     */     
    public function setDefault(){

        //$this->setResultMemory();
        $this->row = [];
        $this->rs = [];      
        $this->array = [];
        $this->collection = [];
        $this->query_string = $this->query_string_default;
             
        $this->setConnect();

        
        return $this;
        
    }
    
    /**
     * Método que define o host de conexão
     * 
     * @access public
     * @method setHost
     * @param  
     * 
     * @return $this
     */      
    public function setHost($value = null) {
        
        $this->host = $value;
            
        return $this;
        
    }	

    /**
     * Método que define a conta de usuário para conexão
     * 
     * @access public
     * @method setUser
     * @param  
     * 
     * @return $this
     */      
    public function setUser($value = null) {
        
        $this->_user = $value;
        
        return $this;
            
    }	

    /**
     * Método que define a senha de conexão
     * 
     * @access public
     * @method setPass
     * @param  
     * 
     * @return $this
     */        
    public function setPass ($value = null) {
        
        $this->pass = $value;
            
        return $this;
        
    }	
    
    /**
     * Método que define o definer padrão do banco de dados
     * 
     * @access public
     * @method setDefiner
     * @param  
     * 
     * @return $this
     */        
    private function setDefiner ($value = null) {
        
        $this->definer = $value;
            
        return $this;
        
    }    

    /**
     * Método que define o nome do banco de dados
     * 
     * @access public
     * @method setDbName
     * @param  
     * 
     * @return $this
     */     
    public function setDbName($value = null) {
        
        $this->dbname = $value;
        
        return $this;
            
    }	
    
    /**
     * Método que define o ambiente de conexão do banco de dados
     * 
     * @access public
     * @method setConnect
     * @param  
     * 
     * @return $this
     */     
    public function setConnect(){
        
        try {

            if(empty($this->config->id($this->getNameMaster())->title('conn')->key('host')->val()) || empty($this->config->id($this->getNameMaster())->title('conn')->key('db_name')->val()) || empty($this->config->id($this->getNameMaster())->title('conn')->key('user')->val())) 
                $this->error->set('Host, Db ou User não definido para '.$this->getNameMaster(),E_FRAMEWORK_ERROR);

            // $this->error->set("setConnect Host={$this->config->id($this->getNameMaster())->title('conn')->key('host')->val()}, Db={$this->config->id($this->getNameMaster())->title('conn')->key('db_name')->val()}, User={$this->config->id($this->getNameMaster())->title('conn')->key('user')->val()} definido para {$this->getNameMaster()}",E_FRAMEWORK_LOG);

            $this->setHost($this->config->id($this->getNameMaster())->title('conn')->key('host')->val());
            $this->setDbName($this->config->id($this->getNameMaster())->title('conn')->key('db_name')->val());
            $this->setUser($this->config->id($this->getNameMaster())->title('conn')->key('user')->val());
            $this->setPass($this->config->id($this->getNameMaster())->title('conn')->key('pass')->val());            
            $this->setDefiner($this->config->id($this->getNameMaster())->title('conn')->key('definer')->val());           
            
        }    
        catch (\Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }          
        
        return $this;
        
    }

    /**
     * Define variáveis de ambiente para execução de transação distribuída
     * 
     * @access public
     * @method setTransaction
     * @param  
     * 
     * @return $this
     */      
    public function setTransaction(){
        
        switch ($this->getMySqlEngine()) {
            case 'mysqli':
                
                $this->conn[$this->getNameMaster()]->query("START TRANSACTION;");
                $this->conn[$this->getNameMaster()]->query("SET AUTOCOMMIT = 0;");

                break;

            default:

                mysql_query("START TRANSACTION;");
                mysql_query("SET AUTOCOMMIT = 0;");
                
                break;
        }           
        
        return $this;
        
    }
    
  
    /**
     * Efetiva as transações distribuídas feitas no banco de dados
     * 
     * @access public
     * @method commit
     * @param  
     * 
     * @return $this
     */     
    public function commit(){
        
        switch ($this->getMySqlEngine()) {
            case 'mysqli':
                
                $this->conn[$this->getNameMaster()]->query("COMMIT;");
                $this->conn[$this->getNameMaster()]->query("SET AUTOCOMMIT = 1;");

                break;

            default:

                mysql_query("COMMIT;");
                mysql_query("SET AUTOCOMMIT = 1;");

                break;
        }        
        
        return $this;
        
    }
    
    /**
     * Desfaz as transações distribuídas feitas no banco de dados
     * 
     * @access public
     * @method commit
     * @param  
     * 
     * @return $this
     */       
    public function rollBack(){
        
        switch ($this->getMySqlEngine()) {
            case 'mysqli':

                $this->conn[$this->getNameMaster()]->query("ROLLBACK;");
                $this->conn[$this->getNameMaster()]->query("SET AUTOCOMMIT = 1;");
                
                break;

            default:

                mysql_query("ROLLBACK;");
                mysql_query("SET AUTOCOMMIT = 1;");

                break;
        }        
        
        $this->error->set("# Rollback executado",E_FRAMEWORK_LOG,$ex);        
        
        return $this;
        
    }

    /**
     * Seta parâmetros de uma Query para consulta ao banco de dados
     * 
     * @access public
     * @method setQuery
     * @param  $index
     * @param  $value
     * @param  $add
     * 
     * @return int
     */     
    public function setQuery($index  = null, $value = null, $add = FALSE){
        
        ($add) ? $this->query_string[$index] .= " {$value} " : $this->query_string[$index] = " {$value}";
        
        return $this;
    
    }    
    
    /**
     * Verifica as condições para execução da query
     * 
     * @access public
     * @method chkQuery
     * @param
     * 
     * @return int
     */      
    public function chkQuery(){
        
        try {

            //if(empty($this->query_string[WHERE])) $this->error->set('Pelo menos uma condição deve ser especificada para execução da query',E_FRAMEWORK_ERROR,$ex);

            return 1;
            
        } 
        catch (\Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);

            return 0;
            
        }        

    }    
    
    /**
     * Seta se o resultado da consulta irá para memória ou ficará no ponteiro da consulta ao banco de dados
     * 
     * @access public
     * @method setResultMemory
     * @param  
     * 
     * @return $this
     */         
    public function setResultMemory($value = 0){
        
        $this->result_memory = $value;
        
        return $this;
        
    }

    
    /**
     * Executa a query montada
     * 
     * @access public
     * @method execute
     * @param  
     * 
     * @return $this
     */     
    public function execute(){    
        
        $this->setRs();
        
        return $this;
        
    }
    
    /**
     * Avança para a próxima linha
     * 
     * @access public
     * @method next
     * @param  
     * 
     * @return $this
     */     
    public function next(){    
        
        $this->setRow();
        
        return $this;
        
    }    
    
    /**
     * Obtem os valores da linha de registro corrente
     * 
     * @access public
     * @method current
     * @param  
     * 
     * @return $this
     */     
    public function current(){    
        
        return $this->getRow();
        
    }   
    
    /**
     * Reseta o ponteiro do array apontando para o primeiro registro
     * 
     * @access public
     * @method reset
     * @param  
     * 
     * @return $this
     */     
    public function reset(){    
        
        reset($this->rs);
        
        return $this;
        
    }      
    
    /**
     * Obtem a linha retornada do objeto
     * 
     * @access public
     * @method fetch
     * @param  
     * 
     * @return $this
     */     
    public function fetch(){    
        
        $this->setRow();
        
        return $this->getRow();
        
    }
    
    /**
     * Seta array ou ponteiro de um conjunto de registros baseados na query de consulta 
     * 
     * Obs: o connect sendo chamado no método setRs aumenta o tempo de execução do código entretanto evita erro quando se usa o
     *      método para save() para salvar o estado de execução.
     * 
     * É importante notar que mysql_query() apena retorna um resource para consultas SELECT, SHOW, EXPLAIN, e DESCRIBE.
     * 
     * @access private
     * @method setRs
     * @param  
     * 
     * @return $this
     */     
    public function setRs(){
        
        try {
    //        $this->setNumRow();
            
            $this->setValue(0);

            if(!$this->connect()->isOk()) $this->error->set(_("# Não foi possível realizar a conexão ao banco de dados"),E_FRAMEWORK_ERROR);

            $query = trim($this->getQuery());
            
            $multi_query = substr_count($query, ';') > 1 ? 1 : 0;
            
            /**
             * Adição de eventos para execução de código extra antes da execução da query
             */            
            $this->on(array('event' => 'before','query' => $query));
            
            $key = md5($query);
            
//            $this->rs = is_object($this->memcache) ? $this->memcache->get($key) : null; 
            
            if(strpos($query,'INSERT')===false && strpos($query,'UPDATE')===false && strpos($query,'DELETE')===false) $this->rs = null;
                
            if(!$this->chkQuery()) $this->error->set('# Erro na validação da Query',E_FRAMEWORK_ERROR);

            $engine = $this->getMySqlEngine();

            switch ($engine) {
                case 'pdo':

                    $result = $this->conn[$this->getNameMaster()]->query($query);

                    $this->setNumRows($result->num_rows);

                    $error = $this->conn[$this->getNameMaster()]->error;
                    
                    $state = $this->conn[$this->getNameMaster()]->sqlstate;

                    break;

                case 'mysqli':

                    $result = $multi_query ? $this->conn[$this->getNameMaster()]->multi_query($query) : $this->conn[$this->getNameMaster()]->query($query);

                    $this->setNumRows($result->num_rows);

                    $error = $this->conn[$this->getNameMaster()]->error;
                    
                    $state = $this->conn[$this->getNameMaster()]->sqlstate;

                    break;
                default:

                    $result = $multi_query ?  $this->error->set('# A engine mysql_query não suporta multi query',E_FRAMEWORK_ERROR) : mysql_query($query,$this->conn[$this->getNameMaster()]);

                    $this->setNumRows(mysql_num_rows($result));

                    $error = mysql_error($result);
                    
                    $state =  mysql_sqlstate;

                    break;
            }    

            $hash = md5(date('his'));
            $date = date('Y-m-d h:i:s');

            if(!$result) {
                
                $this->error->dump(array('message' => "Hash...: <a href=\"#{$hash}\">{$hash}</a>\nDate...: {$date}\nMessege: {$error}",'code' => 'Query..: '.trim(str_replace("\n", "", $this->getLastQuery()))));

                $this->error->set("# Uma rotina de consulta ao banco de dados falhou e interrompeu o processo verifique o dump na hash <a href=\"/intranet/?class=api/log&method=showDump&hash={$hash}\" target=\"_blank\">{$hash}</a>",E_FRAMEWORK_NOTICE);
                
                /**
                 * Mapeamento de erro amigável para usuário final
                 */
                if(strpos('foreign key',$error)!==false) {
                    $this->error->set("O banco de dados gerou uma excessão de chave estrangeira",E_FRAMEWORK_NOTICE);
                }
                elseif(strpos('Prepared statement needs to be re-prepared',$error)!==false) {
                    $error.=". Isso significa que sua configuração <a href=\"https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html#sysvar_table_definition_cache\">table_definition_cache</a> está abaixo do ideal. O valor que o GoFast sugere é de 1000";
                }

                /**
                 * Identificação de estado do erro mysql. Caso seja mensagem de estado 45000 gerada por mensagem customizada então não aplica a tag #
                 */
                $this->error->set(($state=='45000' ? "":"# ")."{$error} - id_master={$this->getIdMaster()}",E_FRAMEWORK_ERROR);

            }
            
            if(!$multi_query) {

                if(strpos(strtoupper(substr($query,0,6)),'SELECT')!==false || strpos(strtoupper(substr($query,0,4)),'CALL')!==false || strpos(strtoupper(substr($query,0,4)),'SHOW')!==false || strpos(strtoupper(substr($query,0,4)),'DESC')!==false) {

                    switch ($engine) {
                        case 'mysqli':
                            /**
                             * Verifica pela existência do método fetch_all na classe mysqli para execução do fetch_all
                             * caso contrário executa a carga tradicional
                             */
                            if (method_exists('mysqli_result', 'fetch_all')){
                                $array = $result->fetch_all(MYSQLI_ASSOC);
                            }
                            else {
                                while ($row = $result->fetch_assoc()){
                                    $array[] = $row;
                                }                             
                            }

                            break;

                        default:

                            while ($row = mysql_fetch_assoc($result)){
                                $array[] = $row;
                            }

                            break;
                    }   

                    if(strpos(strtoupper(substr($query,0,4)),'SHOW')!==false  || strpos(strtoupper(substr($query,0,4)),'DESC')!==false ){

                        if(strpos(strtoupper(substr($query,0,4)),'SHOW')!==false){

                            foreach ($array as $k => $v) {
                                foreach ($v as $v_k => $v_v) {
                                    $this->rs[$k]['table'] = $v_v;
                                }    
                            }

                        }
                        else {
                            /**
                             * Normalizando o índice de nome entre uma consulta com mysqli e mysql
                             */
                            $field = array(0 => 'name',1 => 'type',2 => 'null',3 => 'key',4 => 'default',5 => 'extra','Field' => 'name','Type' => 'type','Null' => 'null','Key' => 'key','Default' => 'default', 'Extra' => 'extra');
                            foreach ($array as $k => $v) {
                                foreach ($v as $v_k => $v_v) {
                                    $this->rs[$k]['field'][$field[$v_k]] = $v_v;
                                }    
                            }
                        }

                    }  
                    else {
                       $this->rs = $array; 
                    }


                    //if($this->getNumRows() != count($this->rs)-1) $this->error->set('# A consulta não retornou o mesmo número de linhas carregadas no array: '.$this->getNumRows().','.count($this->rs)-1,E_FRAMEWORK_ERROR);

                    if(is_object($this->_memcache)) $this->_memcache->set($key, $this->rs, 0, $this->config->title('framework')->key('query_expiration_times')->val() ? $this->config->title('framework')->key('query_expiration_times')->val() : 0);            

                }
                else {

                    if(strpos(strtoupper(substr($query,0,6)),'INSERT')!==false && strpos(strtoupper(substr($query,0,6)),'UPDATE')!==false && strpos(strtoupper(substr($query,0,6)),'DELETE')!==false) $this->rs = $result;

                }
                
            }
            
            $this->setKey();

            /**
             * Libera recurso do servidor de banco de dados para evitar problema de sincronismo com curso
             */

            /**
             * Adição de eventos para execução de código extra depois da execução da query
             */            
            $this->on(array('event' => 'after','query' => $query));

            $this->close();
            
            $this->setValue(1);
            
        } 
        catch (\Exception $ex) {
            
            $this->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
            $this->setValue(0);
            
        }   
        
        return $this;
                
    }
    
    /**
     * Set uma linha de registros a partir de um recordset
     * 
     * @access public
     * @method setRow
     * @param  
     * 
     * @return $this
     */      
    public function setRow(){  
        
        try {
            
            $this->setValue(0);
            
            $this->row = current($this->rs);

            next($this->rs);
            
            $this->setValue(1);
            
        } catch (Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);

        }
        
        return $this->row;
        
    }  
    
    /*
     * Set Cols acrescenta elementos (Colunas) ao vetor de linha de registros com op??o de campos calculados
     */
    public function setCol($value = null) {
        
        $callback_fields = function($matches) {
            
                                if (preg_match("/\"([^\"]*)\"/",$matches[0]) || is_numeric($matches[0])) { 
                                    
                                    return $matches[0];
                                        
                                }
                                else {
                                    
                                    return '$this->row["'.$matches[0].'"]';
                                    
                                }                                 
            
                            };        
        
        $callback_number = function($matches) {
            
                                return $matches[0];
            
                            };        
        
        $fields = explode(',',$value);
        
        foreach ($fields as $key => $value) {
            
            if(empty($value)) continue;
        
            switch (true) {
                case strpos($value,'+='):
                    $key = '+=';
                    $offset = 2;
                    break;
                case strpos($value,'-='):
                    $key = '-=';
                    $offset = 2;
                    break;
                case strpos($value,'*='):
                    $key = '*=';
                    $offset = 2;
                    break;
                case strpos($value,'/='):
                    $key = '/=';
                    $offset = 2;
                    break;
                default:
                    $offset = 1;
                    $key = '=';
                    break;
            }
            
            $arr = explode($key,$value);
            
            $idx = strpos($value,$key);
            
            $len = strlen($value);
            
            $stored = substr($value,0,$idx);
            
            $command = substr($value,$idx+$offset,$len);
            
            if(is_numeric($command)){
                
                $line = preg_replace_callback('([0-9.]+)',$callback_number,$command);
                        
            }
            else {

                $line = preg_replace_callback('([0-9a-zA-Z_"]+)',$callback_fields,$command);
                
            }
            
            if($offset==2){
                
                $command = '$this->result["'.$stored.'"]'.$key.$line.';';
                
            }
            else {
                
                $command = '$this->result = array("'.$stored.'" => '.$line.');';
                
            }
            
            eval($command);
            
            $this->row = array_merge((array)$this->row,(array)$this->result);
            
        }
        
    }
    
    public function setNumRows($value = null){
        
        $this->num_rows = $value;
        
    }

    /**
     * Monta uma coleção de registros baseada nos valores passados por parêmetros em forma de string que defininem os fields de um select
     * 
     * @access public
     * @method setCollection
     * @param  keys: determina as chaves e subchaves de agrupamento
     * 
     * @return $this
     */       
    protected function setCollection($value = null,$value_sum_row='',$value_sum_group='',$value_when_merging_sum = 0){
        
        $value = str_replace(' ','', $value);
        $value_sum_row = str_replace(' ','', $value_sum_row);
        $value_sum_group = str_replace(' ','', $value_sum_group);
        
        $cols = explode(',',$value);
        
        $this->collection["charset"] = $this->getDbCharset(); 

        $command_rs    = '$this->array[] = $this->row;';
        
        $command_array = '$this->collection["data"]';
        $command_group = '';
        $command_value = ' = $this->getRow();';

        $cols_sum_row = empty($value_sum_row) ? [] : explode(',',$value_sum_row);
        $cols_sum_group = empty($value_sum_group) ? [] : explode(',',$value_sum_group);
        
        $command_sum_array = '$this->collection["sum"]';
        $command_sum_group = '';
        $command_sum_general = '';
        $command_sum_group_value = '';
        $command_sum_general_value = '';
        
        $command_count_array = '$this->collection["count"]';
        $command_count_group = '';
        $command_count_general = '';
        $command_count_group_value = '';
        $command_count_general_value = '';
        
        $cols_sum_row = explode(',',$value_sum_row);
        
        $this->setCol($value_sum_row);
        
        foreach ($cols as $key => $value) {
            
            $command_group .= '["'.$this->getRow($value).'"]';
                
            foreach ($cols_sum_group as $key_sum => $value_sum) {
                
                if(empty($value_sum) || empty($this->getRow($value_sum))) continue;
                
                /*
                 * Executa o somatório de campos com grupos e subgrupos
                 */
                
                $command_sum_group = '["group"]'.$command_group.'["'.$value_sum.'"]';
                
                $command_sum_group_value = '+='.(is_numeric($this->getRow($value_sum)) ? $this->getRow($value_sum) : 0).';';

                eval($command_sum_array.$command_sum_group.$command_sum_group_value);
                
                /*
                 * Executa a contagem de campos com grupos e subgrupos
                 */
                
                $command_count_group = '["group"]'.$command_group.'["'.$value_sum.'"]';
                
                $command_count_group_value = '+=1;';

                eval($command_count_array.$command_count_group.$command_count_group_value);
                
            }
            
            
        }
        
        foreach ($cols_sum_group as $key_sum => $value_sum) {
            
            $command_sum_general = '["general"]["'.$value_sum.'"]';

            $command_sum_general_value = '+='.(is_numeric($this->getRow($value_sum)) ? $this->getRow($value_sum) : 0).';';

            eval($command_sum_array.$command_sum_general.$command_sum_general_value);

            $command_count_general = '["general"]["'.$value_sum.'"]';

            $command_count_general_value = '+=1;';

            eval($command_count_array.$command_count_general.$command_count_general_value);
            
        }
        
        /**
         * Permite que valores pre-existente a serem juntados (merge) sejam somados caso numéricos
         */
        if($value_when_merging_sum) {
            
            /**
             * Caso a variável $value_when_merging_sum tenha sido carregada com um array 
             * então e porque a soma deverá ser apenas dos campos mapeados
             */
            $merging_map = $value_when_merging_sum;
            
            eval('$array_merge = '.$command_array.$command_group.';');
            
            if(isset($array_merge)){
                
                $array_sum = $this->getRow();
                
                foreach ($array_sum as $k => $v) {
                    if((is_numeric($v) && $merging_map===1) 
                        || (is_numeric($v) && in_array($k, $merging_map))
                        || (is_numeric($v) && preg_match($merging_map, $k))        
                       ) {
                      $array_merge[$k]+=$v;
                    }
                }

                $command_value = ' = $array_merge;';
                
            }
            
        }

        eval($command_array.$command_group.$command_value);
        
        eval($command_rs);
        
        return $this;
        
    }
    
    /**
     * Define a configuração de ambiente para execução das querys
     * 
     * @access public
     * @method setEnvironment
     * @param  
     * 
     * @return $this
     */     
    private function setEnvironment(){ 
        
        switch ($this->getMySqlEngine()) {
            case 'mysqli':

                /**
                 * Set de ambiente para operações via mysqli_query
                 */
                $this->conn[$this->getNameMaster()]->set_charset($this->getDbCharset());
                $this->conn[$this->getNameMaster()]->query("SET TIME_ZONE = '-03:00'");
                $this->conn[$this->getNameMaster()]->query("SET @@GROUP_CONCAT_MAX_LEN={$this->getGroupConcatMaxLen()}");

                break;

            default:

                /**
                 * Set de ambiente para operações via mysql_query
                 */
                mysql_query("SET NAMES '{$this->getDbCharset()}'");
                mysql_query("SET character_setconn_connect={$this->getDbCharset()}");
                mysql_query("SET character_set_client={$this->getDbCharset()}");
                mysql_query("SET group_concat_max_len={$this->getGroupConcatMaxLen()}");    
                
                break;
        }           
        
        return $this;
    }
    
    /**
     * Método para montar string de busca
     * 
     * Parâmetros:
     * 
     * $value    = string para busca Ex.: Jacques Nunes 
     * $key     = campo ou fields para busca
     * $operand = Operadores l?gicos da busca
     * $inline  = Opera??es em linha
     * $add     = Indica se ? uma montagem de busca concatenada ou n?o
     */
    public function setSearch($value = null, $key = null, $operand = 'OR', $inline = null, $add = 0){
        
        $words = explode(' ', $value);
        $fields = explode(',',$key);
        $search = '';
        
        foreach ($words as $key_word => $word) {
                
            foreach ($fields as $key_field => $field) {
                    
                $search .= " {$operand} {$field} LIKE '%{$word}%'";
                
            }
             
        }  
        
        return $this->setQuery(WHERE," $search $inline ",$add);
        
    }
    
    /**
     * Método para carregar a classe Memcache
     * 
     * @access private
     * @method setMemcache
     * @param  
     * 
     * @return $this
     */   
    private function setMemcache(){ 
        
        try {

            $this->setValue(0);
            
            if(class_exists('Memcache') && $this->config->title('memcache')->key('query_expiration_times')->val()) {

                $this->_memcache = new \Memcache();
                
                $this->_memcache->connect(
                                         strlen($this->config->title('memcache')->key('connect')->val()) ? $this->config->title('memcache')->key('connect')->val() : 'localhost',
                                         strlen($this->config->title('memcache')->key('port')->val()) ? $this->config->title('memcache')->key('port')->val() : '11211'
                                        );            

            }            
            else {

                //$this->error->set("# Não foi possível carregar MEMCACHE pois a classe ".($this->config->title('framework')->key('expiration_times_memcache')->val() ? "não existe" : "está desativada") ,E_FRAMEWORK_NOTICE);                

            }
            
            $this->setValue(1);
            
        } catch (Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }
        
        return $this;
            
    }    

    /**
     * 
     * 
     * @param  
     * 
     * @return $this
     */   
    public function getDbCharset(){ 

        if(isset($this->dbcharset[$this->getCharset()])){
            $charset = $this->dbcharset[$this->getCharset()];
        }
        else {
            $charset = $this->dbcharset[\GoFast\Hub\Fw::CHARSET_DEFAULT];
        }

        return $charset;

    }    
    
    /**
     * Método para obter a definição da engine de acesso ao banco de dados
     * 
     * @access private
     * @method getMySqlEngine
     * @param  
     * 
     * @return $this
     */   
    private function getMySqlEngine(){ 
        
        try {
            
            $this->setValue(0);
            
            if($this->config->title('conn')->key('engine')->val()){
                $value = $this->config->title('conn')->key('engine')->val();
            }
            else {
                $value = class_exists('mysqli') ? 'mysqli' : 'mysql';
            }
            
            //$this->error->set("# Usando engine {$value}",E_FRAMEWORK_LOG);            
            
            $this->setValue(1);
            
        } catch (Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }
        
        return $value;
            
    }    
    
    
    /**
     * Método para carregar a classe Memcache
     * 
     * @access private
     * @method setMySqlEngine
     * @param  
     * 
     * @return $this
     */   
    private function setMySqlEngine(){ 
        
        try {
            
            $this->setValue(0);

            global $id_master;

            $this->id_master = $id_master;
            
            switch ($this->getMySqlEngine()) {
                case 'pdo':
                    $this->conn[$this->getNameMaster()] = new \PDO("mysql:host={$this->getHost()};dbname={$this->getDbName()}", $this->getUser(), $this->getPass()); 
                    
                    $this->conn[$this->getNameMaster()]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);                     

                    break;
                    
                case 'mysqli':

                    if(@!$this->conn[$this->getNameMaster()] = new \mysqli($this->getHost(), $this->getUser(), $this->getPass(), $this->getDbName())) $this->error->set("# Nenhuma conexção pôde ser feita porque a máquina de destino as recusou ativamente",E_FRAMEWORK_WARNING);  

                    if($this->conn[$this->getNameMaster()]->connect_errno) {

                        $this->error->set("# {$this->conn[$this->getNameMaster()]->connect_error}, verifique o arquivo {$this->config->getFilename()} [db_name = {$this->getDbName()}, host = {$this->getHost()}, user = {$this->getUser()}]",E_FRAMEWORK_ERROR);

                    }  
                    
                    // $this->error->set("# Conexão [db_name = {$this->getDbName()}, host = {$this->getHost()}, user = {$this->getUser()}]",E_FRAMEWORK_LOG);
                    
                    break;
                    
                default:
                    
                    /**
                     * Se eu não passar o 4 parâmetro (new_link) com true (1) eu não estabeleço uma nova conexão e vou ter problema 
                     * de sincronismo utilizando em execuções que usam simultaneamente a mesma conexão na engine mysql. 
                     * Entretanto esse parâmetro true (1) não funciona no Mac (Darwin).
                     */
                    
                    if(!$this->conn[$this->getNameMaster()] = mysql_connect($this->getHost(), $this->getUser(), $this->getPass(),strpos('darwin', strtolower(php_uname()))!==false ? 0 : 1)) $this->error->set("# ".mysql_error().", verifique o arquivo {$this->config->getFilename()} [db_name = {$this->getDbName()}, host = {$this->getHost()}, user = {$this->getUser()}]",E_FRAMEWORK_ERROR);

                    if(!mysql_selectconn($this->getDbName(), $this->conn[$this->getNameMaster()])) $this->error->set("# Não foi possível selecionar o banco de dados {$this->getDbName()} - ".mysql_error()." ",E_FRAMEWORK_ERROR);

                    break;
            }
                
            $this->setValue(1);
            
        } catch (Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }
        
        return $this;
            
    }    
    

    
    /**
     * Método que execução a conexão ao banco de dados
     * 
     * @access public
     * @method connect
     * @param  
     * 
     * @return $this
     */   
    public function connect(){ 
        
        try {
            
            $this->setMemcache();
            $this->setConnect();

            if(!$this->setMySqlEngine()->isOk()) $this->error->set(_("Não foi possível connectar ao banco de dados"),E_FRAMEWORK_ERROR);

            $this->setEnvironment();
            $this->connected = 1;            
            $this->setValue(1);
                
        } 
        catch (\Exception $ex) {
            
            $this->connected = 0;
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }     
        
        return $this;
        
    }
    
    
    /**
     * Método de conexão ao banco de dados via PDO
     * 
     * @access public
     * @method connectPDO
     * @param  
     * 
     * @return this;
     */    
    public function connectPDO(){ 
        
        try {
            
            if (!isset($this->pdo)) {
                $this->pdo = new PDO(
                    "mysql:host={$this->getHost()};dbname={$this->getDbName()}",
                    "{$this->getUser()}",
                    "{$this->getPass()}",
                    array(PDO::ATTR_PERSISTENT => false)
                );
                $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->pdo->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
            }
   
            return $this->pdo;                

        } 
        catch (\Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }      
        
        return $this;
        
    }
              
    
   /**
     * Método que retorna o tamanho máximo de concatenção de string via tag query GROUP_CONCAT
     * 
     * @access public
     * @method getGroupConcatMaxLen
     * @param  
     * 
     * @return $this
     */      
    public function getGroupConcatMaxLen() { 
        
        return $this->group_concat_max_len;
        
    }    
    
    /**
     * Método que obtem o IP ou DNS do host
     * 
     * @access public
     * @method getHost
     * @param  
     * 
     * @return string
     */      
    public function getHost() {
        
        return $this->host;
            
    }	

    /**
     * Método para obter o nome do usuário de conexão ao banco de dados
     * 
     * @access public
     * @method getUser
     * @param  
     * 
     * @return string
     */       
    public function getUser() {
        
        return $this->_user;
            
    }	

    /*
     * Obtem a senha de acesso
     */
    public function getPass() {
        
        return $this->pass;
            
    }	
    
    /**
     * Método que obtem o definer padrão do banco de dados
     * 
     * @access public
     * @method getDefiner
     * @param  
     * 
     * @return $this
     */        
    public function getDefiner() {
        
        return $this->definer;
        
    }     

    /*
     * Obtem o nome do banco de dados
     */
    public function getDbName() {
        
        return $this->dbname;
            
    }    
    
    /*
     * Obtem a vers?o da classe
     */
    private function getVersao(){
        
        return $this->versao;
        
    }     
    
    private function getResultMemory(){
        
        return $this->result_memory;
        
    }    

    public function getRs(){
        
        return $this->rs;
        
    }    
    
    public function getRow($value = null){
        
        if(!empty($value)) {
            
            if (!array_key_exists($value,$this->row) && empty($this->collection)) {
                
                return NULL;
                
            }
            else {
                
                return $this->row[$value];

            }
            
        }
        
        
        return $this->row;
        
    }    
    

    /**
     * Retorna uma query montada
     * 
     * @access public
     * @method getQuery
     * @param  int
     * 
     * @return string
     */    
    public function getQuery($index = NULL){
        
        $query = '';
        
        if(empty($index)) {
            
            $start = 0;
            $off_set = count($this->query_string_value)+1;
            
        }
        else {

            $start = $index;
            $off_set = $start + 1;
            
        }
        
            
        for ($i = $start; $i < $off_set; $i++) {

            $command = $this->query_string_value[$i];

            $string = $this->query_string[$i];

            if(strlen($string) > 0){

                $query .= "  $command {$string} ";
                
                $this->query_string[$i] = '';

                
            }

        }
        
        $this->last_query_string  = $query;
        
        return $query;
        
    } 
    
    /**
     * Retorna a última query executada pela classe
     * 
     * @access public
     * @method getLastQuery
     * @param  
     * 
     * @return $this
     */     
    public function getLastQuery(){
        
        return $this->last_query_string;
    
    }    
    
    /**
     * Obtem uma coleção de registro agrupados por chaves, que podem ser somadas a partir do segundo conjunto de parâmetros
     * 
     * @access public
     * @method getCollection
     * @param  value1 = keys               = Campos a serem agrupados (Regra de agrupamento)
     * @param  value2 = cols_extends       = Campos calculados a partir dos campos de cada linha de registros
     * @param  value3 = cols_overall       = Campos com os valores totais de cada agrupamento
     * @param  value4 = clear              = Inicializa o vetor novamente para evitar futuros agrupamentos em consultas sequenciais
     * @param  value5 = when_merging_sum   = Define que quando a chave de um array já existir e for se sobreposta que os valores se somem
     *                                       Pode conter o valor 0 (default), 1 (somar todos os campos numéricos), array (com listagem dos campos a serem somados)
     *                                       ou uma expressão regular que case com os campos a serem somados
     * 
     * @return array
     */      
    public function getCollection($value1 = null,$value2 = null,$value3 = null,$value4 = null,$value5 = null){
        
        $this->result  = [];
        
        if(is_array($value1)) {

            foreach ($value1 as $k => $v) {

                switch ($k) {
                    case 'keys':
                        $value1 = $v;
                        break;
                    case 'cols_extends':
                        $value2 = $v;
                        break;
                    case 'cols_overall':
                        $value3 = $v;
                        break;
                    case 'clear':
                        $value4 = $v;
                        break;
                    case 'when_merging_sum':
                        $value5 = $v;
                        break;                        
                    default:
                        break;
                }

            }

        }         
        
        if(isset($value4)) {
            
            $this->collection = [];
            
        }    
        
        while ($this->setRow()) {
            
            $this->setCollection($value1,$value2,$value3,$value5);
            
        }
        
        $this->rs = $this->array;
        
        return $this->collection;    
        
    }
    
    /**
     * Retorna o índice da posição corrente do registro
     * 
     * @access public
     * @method getIndex
     * @param  
     * 
     * @return $this
     */      
    public function getIndex(){  
        
        return key($this->rs);
        
    }  
    
    /**
     * Retorna o Json de um array
     * 
     * @access public
     * @method getJson
     * @param  
     * 
     * @return $this
     */      
    public function getJson($value = null){
        
        return json_encode($this->getArray($value));          
        
    }
    
    /**
     * Retorna em forma de array os dados do recordset
     * 
     * 08/01/2020 - refatoração em parte desse processo para permitir a chamada ao método várias vezes como por exemplo o uso indireto
     *              da classe de core cmbBox em um loop. Dessa forma o método interage diretamente com o array recorset sem intervenção
     *              da posição do ponteiro
     * 
     * 09/03/2020 - Adicionado a opção de carga simples de array com definição de chave e valor
     * 
     * @access public
     * @method getArray
     * @param  $key, $value
     * 
     * @return array
     */     
    public function getArray($key='',$value=''){
        
        if(is_array($key)) {

            $class = $name = '';

            foreach ($key as $k => $v) {

                switch ($k) {
                    case 'key':
                        $key_index = $v;
                        break;
                    case 'field':
                        $_field = $v;
                        break;                    
                    case 'fields':
                        $_fields = explode(',',$v);;
                        break;
                    default:
                        break;
                }

            }

        }        
        
        $this->array = [];
        
        if(empty($key) && empty($value)){
            
                foreach ($this->rs as $k => $v) {
                    
                    $this->array[] = $v;
                    
                }            
            
        }
        else {
            
            if(is_array($key)) {
                
                foreach ($this->rs as $k => $v) {
                    
                    if(is_array($_fields) && !isset($_field)) {
                        
                        foreach ($_fields as $k_f => $v_f) {

                            $array[trim($v_f)] = $v[trim($v_f)];

                        }

                        $this->array[$v[$key_index]] = $array;
                        
                    }
                    else {
                        
                        $this->array[$v[$key_index]] = $v[$_field];
                        
                    }
                    
                    
                }                  
                
            }
            else {
                
                foreach ($this->rs as $k => $v) {
                    
                    $this->array[$v[$key]] = $v[$key].' - '.$v[$value];
                    
                }
            
            }
            

        }
        
            
        return $this->array;
        
    }
    
    /**
     * Este método retorna uma string com as linhas retornadas separadas por vírgula por padrão
     * 
     * @access public
     * @method getRowGroupConcat
     * @param  string (Nome do Campo)
     * 
     * @return $this
     */     
    public function getRowGroupConcat($field = null,$glue=','){
        
        $this->array = [];
        
        while ($this->setRow()) {
            
            $this->array[$this->getRow($field)] = $this->getRow($field);

        }
        
        return implode($glue, $this->array);
            
    }   
    
    /**
     * Este método retorna uma string com os caracteres concatenados pelo campo da última consulta
     * 
     * @access public
     * @method getRowGroupConcat
     * @param  string (Nome do Campo)
     * 
     * @return $this
     */     
    public function getGroupConcat($field = null,$glue=','){
        
        $this->array = [];
        
        foreach ($this->rs as $key => $value) {
            $this->array[$value[$field]] = $value[$field];
        }

        return implode($glue, $this->array);
            
    }    
    
    public function getTotCollection($value = null){
        
        $cols = explode(',',$value);

        $command = '$this->collection';
        
        foreach ($cols as $key => $value) {
            
            $command .= '[$this->getRow('.$value.')]';
            
        }
        
        return eval("count{$command};");
        
    }    
    
    // Para verificar as Querys processadas
    // A ideia inicial aqui e verificar UPDATE sem WHERE
    // Evitar tamb?m inje??o SQL
    // Sintaxe com aspas duplas no meio da instru??o
    private function check(){
        
    }
    
    /**
     *  Define o valor da chave primária inserida
     * 
     * @access public
     * @method getKey
     * @param  
     * 
     * @return $this
     */      
    public function setKey(){
        
        switch ($this->getMySqlEngine()) {
            case 'mysqli':
                
                $this->key[$this->getNameMaster()] = $this->conn[$this->getNameMaster()]->insert_id;
                // $this->key = mysqli_insert_id($this->conn[$this->getNameMaster()]);
                
                break;

            default:

                $this->key[$this->getNameMaster()] = mysql_insert_id($this->conn[$this->getNameMaster()]);

                break;
        }
        
        return $this;
        
    }

    /**
     *  Obtem a nova chave de registro incluído
     * 
     * @access public
     * @method getKey
     * @param  
     * 
     * @return $this
     */      
    public function getKey(){
               
        return $this->key[$this->getNameMaster()];
        
    }    
    
    /**
     * Obtem o número de linhas afetadas para uma determinada operação
     * 
     * @access public
     * @method getRowAffected
     * @param  
     * 
     * @return $this
     */      
    public function getRowAffected(){
        
       $info = explode(' ',mysql_info());
       
       return (int)$info[4];
        
    }
    
    /**
     * Obtem o número de linhas compatíveis a execução da operação
     * 
     * @access public
     * @method getRowMatched
     * @param  
     * 
     * @return $this
     */      
    public function getRowMatched(){ 
        
       $info = explode(' ',mysql_info());
       
       return (int)$info[2];
        
    }
    
    /**
     * Método que seta a query para criação de uma coluna com contagem de linhas
     * 
     * @access public
     * @method setNumRow
     * @param  
     * 
     * @return $this
     */      
    public function setNumRow(){
        
        $this->setQuery(SELECT,',@rownum:=@rownum+1 as num_row',ADD);
        
        $this->setQuery(FROM,', (SELECT @rownum:=0) r',ADD);
        
    }
    
    /**
     * Método para obter o número de linhas retornados pela consulta
     * 
     * @access public
     * @method getNumRows
     * @param  
     * 
     * @return $this
     */      
    public function getNumRows(){
        
        return $this->num_rows;
	
    }

    public function getObj(){
		
        return ($this->getResultMemory() ?  (object)$this->getRs() : mysql_fetch_object($this->getRs()));

	
    }
    
    public function getSearch(){
        
        return $this->search;
        
    }
    
    public function kill() {

        $result = mysql_query("SHOW FULL PROCESSLIST");

        if (count($result) >= 5) {
                while ($this->row=mysql_fetch_assoc($result)) {
                $process_id=$this->row["Id"];
                        if ($this->row["Time"] > 20000 && $this->row["Command"]=="Query") {
                                $sql="KILL $process_id";
                                return mysql_query($sql);
                        }
                }
                
                //$this->error->setError(mysql_error());
        
                return 0;
                
        }
        
        return 0;
    }	
    
    /**
     * Constroi uma query de atualização de registro a partir de uma array
     * 
     * @access public
     * @method makeFieldUpdate
     * @param  
     * 
     * @return $this
     */      
    public function makeFieldUpdate($table = null,$array = null){
        
        $fields = '';
        
        foreach ($array as $key => $value) {
            
            if(gettype($value['value']) == 'string' || is_null($value['value'])){

                $fields .= (empty($fields) ? " {$key}='{$value['value']}' " : ", {$key}='{$value['value']}' ");
                
            }
            else {
                
                $fields .= (empty($fields) ? " {$key}={$value['value']} " : ", {$key}={$value['value']} ");
                
            }    
            
        }
        
        
        $string = " {$fields} ";
        
        $this->setQuery(UPDATE," {$table} SET {$string}",ADD);
        
        return $this;
        
    }
    
    /**
     * Constroi uma query de inserção de registro a partir de uma array
     * 
     * @access public
     * @method makeFieldInsert
     * @param  
     * 
     * @return $this
     */      
    public function makeFieldInsert($table = null,$array = null){
        
        $fields = '';
        $values = '';
        
        foreach ($array as $key => $value) {
            
            $fields .= (empty($fields) ? " {$key} " : ", {$key} ");
            
            if($value['type'] == 'text' || $value['type'] == 'time' || is_null($value['value'])){

                $values .= (empty($values) ? " '{$value['value']}' " : ", '{$value['value']}' ");
                
            }
            else {
                
                /**
                 * Caso algum campo possua valor vazio ou nulo ent?o seta 0 para valores num?ricos
                 */
                if(empty($value['value'])) {

                    $values .= (empty($values) ? " 0 " : ", 0 ");

                }
                else {
                    
                    $values .= (empty($values) ? " {$value['value']} " : ", {$value['value']} ");
                    
                }
                
            }    
            
        }
        
        
        $string = " ({$fields}) VALUES ({$values}) ";
        
        $this->setQuery(INSERT," {$table} {$string} ;");
        
        return $this;
        
    }
    
    public function close() {
        
        switch ($this->getMySqlEngine()) {
            case 'mysqli':

                $this->conn[$this->getNameMaster()]->close();

                break;

            default:

                mysql_close($this->conn[$this->getNameMaster()]);

                break;
        }
        
        return $this;
        
    }
	
    /**
     * Método para execução de código extendido na execução dos eventos da classe
     * 
     * @access public
     * @method makeFieldInsert
     * @param  
     * 
     * @return $this
     */      
    public function on($value = null){
        
        try {
            
            if (is_array($value)) {

                foreach ($value as $k => $v) {
                    switch ($k) {
                        case 'event':
                            $event = $v;
                        case 'query':
                            $query = $v;
                            break;
                        default:
                            break;
                    }
                }
                
            }
                           
            $file['insert'] = ROOT_LIB.'inc'.DS.'db'.DS."{$event}Insert.inc";
            $file['update'] = ROOT_LIB.'inc'.DS.'db'.DS."{$event}Update.inc";
            $file['delete'] = ROOT_LIB.'inc'.DS.'db'.DS."{$event}Delete.inc";
            $file['select'] = ROOT_LIB.'inc'.DS.'db'.DS."{$event}Select.inc";

            if(strpos($query,'INSERT')!==false && file_exists($file['insert'])){
                if(!include_once($file['insert'])) die(_("# Não foi possível incluir ").$file['insert']); 
            }
            elseif(strpos($query,'UPDATE')!==false && file_exists($file['update'])){
                if(!include_once($file['update'])) die(_("# Não foi possível incluir ").$file['update']); 
            }
            elseif(strpos($query,'DELETE')!==false && file_exists($file['delete'])){
                if(!include_once($file['delete'])) die(_("# Não foi possível incluir ").$file['delete']); 
            }
            elseif(strpos($query,'SELECT')!==false && file_exists($file['select'])){ 
                if(!include_once($file['select'])) die(_("# Não foi possível incluir ").$file['select']); 
            }
            
            $this->setValue(1);
            
        } catch (Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }
        
        
        return $this;
        
    }
    
}
