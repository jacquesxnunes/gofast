<?php   

/**
 *  
 * @version: 3.0.0001L - 04/05/2015 - Jacques - Versão Inicial
 * @version: 3.0.0001L - 13/10/2015 - Jacques - Implementado a possibilidade de uso de métodos encadeados e uso simplificado do set e get
 * @version: 3.0.5251L - 30/12/2015 - Jacques - Adicionado método de controle de erro na carga das classes do framework 
 * @version: 3.0.5408L - 07/01/2016 - Jacques - Implementação de pilha de erros com todos os parâmetros e uso dele atrav�s de throw new Exception
 * @version: 3.0.6637L - 18/01/2016 - Jacques - Adicionado os métodos chkInCode e getAllMsgCode para verificação de erros e obtenção dos erros lançados na classe ErrorClass
 * @version: 3.0.7765L - 18/01/2016 - Jacques - Adicionado padronização de mensagens de erro
 * @version: 3.0.7765L - 01/06/2016 - Jacques - Adicinado método getAllMsgCodeJson para retorno de lista de error no formato Json.
 * @version: 3.0.0186F - 18/07/2016 - Jacques - Comentado o código: header("Content-Type: text/html; charset=UTF-8",true); Pois estava gerando problema de codificação no uso do framework no sistema legado quando 
 *                                              utilizado de forma híbrida. Importante verificar as consequências desse código comentado.
 * @version: 3.0.0233F - 30/09/2016 - Jacques - Adicionado o método para gerar log dos erros do framework com nova opção de uso de E_FRAMEWORK_LOG para título apenas de registro em log de alguma operação  
 * @version: 3.0.0237F - 07/10/2016 - Jacques - Adicionado a identificação do SERVER_NAME no log de error
 * @version: 3.0.0237F - 04/01/2017 - Jacques - Adicionado value na mensagem default do vetor index 8
 * @version: 3.0.0000F - 13/02/2017 - Jacques - Adicionado o registro no log do nome de usuário
 * @version: 3.0.0000F - 02/04/2017 - Jacques - Retirei o uso da extensão getText da classe error para evitar que haja alguma excessão quando a extensão não estiver ativa no PHP
 * 
 * @todo
 * ATEN��O: 1. O sistema dever� sempre interromper o avanão encadeado dos processos quando encontrar erros <= code = 2 (E_ERROR, E_WARNING)
 * 
 * E_LOG               = 0      (ERROR-FWK - Não é código de erro e sim instrução para registro em log)
 * E_ERROR             = 1      (ERROR-PHP - Erros fatais em tempo de execução. Estes indicam erros que não podem ser recuperados, como problemas de aloca��o de mem�ria. A execução do script � interrompida)
 * E_WARNING           = 2      (ERROR-PHP - Avisos em tempo de execução (erros não fatais). A execução do script não � interrompida)
 * E_FRAMEWORK_ERROR   = 3      (ERROR-FWK - Erro fatal que viola alguma l�gica do framework)
 * E_PARSE             = 4      (ERROR-PHP - Erro em tempo de compila��o. Erros gerados pelo interpretador)
 * E_FRAMEWORK_WARNING = 5      (ERROR-FWK - Avisos em tempo de execução (erros não fatais). A execução do script do framework não � interrompida mas exige atenção)
 * E_FRAMEWORK_NOTICE  = 6      (ERROR-FWK - Not�cia em tempo de execução. Indica que o script do framework encontrou alguma coisa que pode indicar um erro, mas que tamb�m possa acontecer durante a execução normal do script e gerar consequencias no processamento padr�o)
 * E_NOTICE            = 8      (ERROR-PHP - Not�cia em tempo de execução. Indica que o script encontrou alguma coisa que pode indicar um erro, mas que tamb�m possa acontecer durante a execução normal do script)
 * E_CORE_ERROR        = 16     (ERROR-PHP - Erro fatal que acontece durante a inicializa��o do PHP. Este � parecido com E_ERROR, exceto que � gerado pelo n�cleo do PHP)
 * E_ALL               = 30719  (ERROR-PHP - Todos erros e avisos, como suportado, exceto de n�vel E_STRICT 
 * 
 * métodos do objeto de error
 * 
 * $obj->getMessage();
 * $obj->getFile();
 * $obj->getLine();
 * $obj->getCode();
 * $obj->getPrevious();
 * $obj->getTrace();
 * $obj->getTraceAsString();
 * 
 * Ordem de chamada dos eventos e métodos em uma excess�o
 * 
 * shutdown_handler
 * FrameWorkException
 * FrameWorkException->__construct
 * exception_handler
 *  
 * @author jacques@f71.com.br
 * 
 * @copyright www.f71.com.br 
 * 
 */
//header("Content-Type: text/html; charset=UTF-8",true); 


namespace GoFast\Lib;

use GoFast\Kernel\Core;

/**
 * Classe de exception extendida para controle de erros
 * 
 * @access extends
 * @param
 * 
 * @return $this
 */  
class FrameWorkException extends \Exception {
    
    public function __construct($message = null, $code = null, $file = null, $line = null) {
        
        if ($code === null) {
            
            parent::__construct($message);
            
        } else {
            
            parent::__construct($message, $code);
            
        }
        
        if ($file !== null) {
            
            $this->file = $file;
            
        }
        
        if ($line !== null) {
            
            $this->line = $line;
            
        }
        
    }
    
}

class FileException extends FrameWorkException {};

class SystemException extends FrameWorkException {};

class IOError extends FrameWorkException {}; 

class AccessControl extends FrameWorkException {}; 

class Error extends Core { 

    const E_ERROR = E_ERROR;
    const E_WARNING = E_WARNING;
    const E_PARSE = E_PARSE;
    const E_NOTICE = E_NOTICE;
    const E_FRAMEWORK_LOG = 0;
    const E_FRAMEWORK_ERROR = 3;
    const E_FRAMEWORK_WARNING = 5;
    const E_FRAMEWORK_NOTICE = 9;
    const E_CORE_ERROR = 16;
    const E_ALL = E_ALL;
    const E_DEPRECATED = E_DEPRECATED;

    public static $instance;     
    public static $error_array = [];        

    public $version;
    public $log;    
    public $lines;
    public $line;
   
    private static  $array_default = array('error' => 
                                            array(
                                                'code' => 0,
                                                'message' => 'Unknown exception',
                                                'class' => '',
                                                'path_class' => '',
                                                'file' => '',
                                                'line' => 0,
                                                'string' => ''
                                            )    
                            );                    
    
    private $mail = array('header' => '',
                          'to' => 'jacques@f71.com.br',
                          'from' => 'postmaster@f71.com.br',
                          'subtitle' => 'LOG de erros do Framework',
                          'message' => ''
                          );
    
    private $msg_erro = array();
    private $buffer;
    private $core_version;

    use \GoFast\Lib\Bridge;    
    public function __construct($value = null) {

        parent::__construct($value);    
        
        $this->createCoreClass($value);
        
        $this->msg_erro[0] = "# Mensagem de erro";
        $this->msg_erro[1] = "# Uma exceção em <value> impediu a finalização do processo";
        $this->msg_erro[2] = "# Houve um erro ao selecionar os registros da classe <value>";
        $this->msg_erro[3] = "# Tipo do parâmetro do método <value> inválido";
        $this->msg_erro[4] = "# Pelo menos uma propriedade precisa ser definida no método <value>";
        $this->msg_erro[5] = "# Não existe nenhum parâmetro definido para execução do método <value>";
        $this->msg_erro[6] = "# O método <value> possui dependência de classes sem as quais sua execução não é possível";
        $this->msg_erro[7] = "# Um vetor impressindível para execução do método <value> está fazio ou sua chave não tem correspondência";
        $this->msg_erro[8] = "# Não existe uma chave associada ao keymaster da interface em <value>";
        $this->msg_erro[9] = "# Não foi possível carregar o arquivo pelo método <value>";
        $this->msg_erro[10]= "# A propriedade ou método da classe <value> não existe";
        $this->msg_erro[11]= "# A classe <value> não possui esse método declarado";
        $this->msg_erro[12]= "# A classe <value> não está definida na tabela de instânciamento dinâmico";
        $this->msg_erro[13]= "# Não foi possível incluir o arquivo <value> ";
        $this->msg_erro[14]= "# Acesso Negado <value> ";

        $this->core_version = $this->version->getCoreVersionComposer();

        if(!is_object($this->log)) die(_("Não foi possível instânciar a classe log"));
        
        
    }    

    
    public function set($value = null, $code = E_ERROR, \Exception $previous = null){
        
        if(is_array($value)){
            
            $class = $value[1];

            $value = str_replace('<value>', $value[1],$this->getMsgDefault($value[0]));
            
        }
        else {
            
            $class = '';
            
        }
        
        if(count(self::$error_array)==0) $this->log->info(vsprintf("%02d", $code)." REQUEST  - # {$this->getServerSnapShotString()}");
        
        self::$error_array[] = array(
                                'code' => $code,
                                'message' => $value,
                                'class' => $class,
                                'path_class' => '',
                                'file' => '',
                                'line' => 0,
                                'string' => ''
                                );
        
        if($code==E_ERROR || $code==E_FRAMEWORK_ERROR){
            
            $arr = debug_backtrace(2);
            
            $trace = array_reverse($arr);
            
            foreach ($trace as $key => $error) {
                
                $stack = sprintf("%03d", $key);
                
                $this->log->info(vsprintf("%02d", 0)." INFO     - # [tracert:{$stack}] {$error['class']}->{$error['function']}() call in {$error['file']}:{$error['line']}");
                
            }
            
        }
        
        switch ($code) {
            case E_ERROR:
            case E_FRAMEWORK_ERROR:
                $this->log->error(vsprintf("%02d", $code)." ERROR    - {$value}");
                break;
            case E_WARNING:
            case E_FRAMEWORK_WARNING:
                $this->log->warning(vsprintf("%02d", $code)." WARNING  - {$value}");
                break;
            case E_NOTICE:
            case E_FRAMEWORK_NOTICE:
                $this->log->warning(vsprintf("%02d", $code)." NOTICE   - {$value}");
                break;
            default:
                $this->log->info(vsprintf("%02d", $code)." INFO     - {$value}");
                break;
        }
        
        /**
         * Nem todos os erros na class ErrorClass geram uma exceção, portanto faz-se necessário criar uma pilha de erros
         */
        if($code === E_ERROR || $code === E_FRAMEWORK_ERROR) throw new \Exception($value, $code);

        
        return $this;
        
    }
    
    public function setError($value, $code = E_ERROR, \Exception $previous = null) {

        $this->set($value,$code,$previous);
        
        return $this;
        
    }
    
    public function getServerSnapShotArray(){
        
        $srv['name'] = $_SERVER['SERVER_NAME'];        
        $srv['host'] = $_SERVER['HTTP_HOST'];     
        $srv['agent'] = $_SERVER['HTTP_USER_AGENT'];     
        $srv['string'] = $_SERVER['QUERY_STRING'];       
        $srv['uri'] = explode('?', $_SERVER['REQUEST_URI'])[0];        
        $srv['php_version'] = phpversion();
        $srv['gofast_version'] = $this->core_version;
        $srv['session'] = session_id();
        $srv['login'] = isset($_SESSION['login'])?$_SESSION['login']:'';
        $srv['logado'] = isset($_SESSION['logado'])?$_COOKIE['logado']:'';        
        $srv['ipv4'] = isset($_SESSION['ipv4'])?$_COOKIE['ipv4']:'';       
        $srv['ipv6'] = isset($_SESSION['ipv6'])?$_COOKIE['ipv6']:'';
        
        $query = '';
        
        $i = 0;

        foreach ($_REQUEST  as $key => $value) {

            $query.= (++$i==1 ? '?' : '&')."{$key}={$value}"; 

        }
        
        $srv['query'] = $query;
        
        return $srv;
        
    }    
    
    public function getServerSnapShotString(){
        
        $srv = $this->getServerSnapShotArray();
        
        $url = urlencode($srv['host'].$srv['uri'].$srv['query']);
        
        return "{$srv['agent']} - PHP/{$srv['php_version']} - goFast/{$srv['gofast_version']} [ http://{$url} ]";
        
    }


    public function get(){
        
        return $this;

    }
    
    public function getMsgDefault($value){
        
        $this->setValue($value[0] < count($value) ? $this->msg_erro[$value] : $this->msg_erro[0]);
                
        return $this;
        
    }
    
   /**
    * Percorre todo o vetor de erros concatenando para retornar uma string. Caso haja par�metro filtra apenas erros contidos nele
    * 
    * @access public
    * @method getAll
    * @param 
    * 
    * @return $this
    */       
    public function getAll(){
        
        $this->get();
        
        return $this;

    }
     
    public function getError(){

        $this->get();
        
        return $this;

     }
     
    public function getArr(){
         
        return exception_handler();
         
    }

   /**
    * Verifica se foi gerado um determinado c�digo de erro
    * 
    * @access public
    * @method chkInCode
    * @param int
    * 
    * @return $this
    */       
    public function chkInCode($code){
        
        
        foreach(self::$error_array as $key => $value) {      
            
            if($value['code']==$code) return 1;
            
        }
        
        return 0;
        
    }

   /**
    * Retorna uma string com todas mensagens de um determinado código
    * 30/06/2016 - Adicionado tags de marcação de erro que não deve ser apresentado para o usuário final
    * 
    * @access protected
    * @method getAllMsgCode
    * @param int
    * 
    * @return $this
    */     
    public function getAllMsgCode($code, $break = "\n"){
        
        $sendMail = 0;
        
        $error_reporting = $_COOKIE["error_reporting"];
        
        $this->value = ''; 
        
        foreach(self::$error_array as $key => $value) {                
            
            $message .= $value['message'].$break;
            
            if(($value['code']==$code && $code > 0) || strpos($value['message'],'#')===false || ($error_reporting=='E_ALL')){
                
                $this->value .= $value['message'].$break;
                
            }
            
            $sendMail = ($sendMail || $value['code']==E_ERROR || $value['code']==E_FRAMEWORK_ERROR);
            
        }
        
        //if($sendMail) $this->setMailSuport($message)->sendMailSuport();
        
        return $this;
        
    }
    
   /**
    * Retorna um JSON com todos os erros reportados na classe error ou definido como error code
    * 
    * @access public
    * @method getAllMsgCodeJson
    * @param  (int)   $code    - Código específico para filtragem de erros específicos
    * @param  (string)$charset - Define o charset de retorno
    *   ou 
    * @param  (array)  code           => Código específico para filtragem de erros específicos
    *                  charset        => Define o charset de retorno
    *                  reverse_order  => Define se a listagem irá ser retornada na ordem inversa
    * 
    * @return json
    */    
    public function getAllMsgCodeJson($value,$charset = ''){
        
        $status = 0;
        
        $reverse_order = 0;
        
        $sendMail = 0;
        
        $error_reporting = $_COOKIE["error_reporting"];
        
        if (is_array($value)) {

            foreach ($value as $k => $v) {

                switch ($k) {
                    case 'code':
                        $code = $v;
                        break;
                    case 'charset':
                        $charset = $v;
                        break;
                    case 'reverse_order':
                        $reverse_order = $v;
                        break;
                    case 'status':
                        $status = $v;
                        break;
                    case 'data_ext':
                        $data_ext = $v;
                        break;
                    default:
                        break;
                }

            }

        }      
        else {
            
            $code = $value;
            
        }
        
        foreach(self::$error_array as $key => $value) {                
            
            if(($value['code']==$code && $code > 0) || strpos($value['message'],'#')===false || ($error_reporting=='E_ALL')){
                
                $return[] = array('code' => $value['code'],'message' => mb_convert_encoding($value['message'],$charset));
                
            }
            
        }  
        
        if($reverse_order) $return = array_reverse($return);
        
        ob_clean();
        
        return json_encode(array('status' => $status,'data' => $return,'data_ext' => $data_ext));
        
        
    }
    
     
    /**
     * Captura o erro em uma instrução Eval executada para uma classe do FrameWork
     * 
     * @access public
     * @method getCodeLastError
     * @param  
     * 
     * @return array
     */     
     public function getCodeLastError(){
         
        $error = error_get_last();

        return $error['type'];
        
     }
     
    /**
     * Define todos os parâmetros para envio de email de suport
     * 
     * @access public
     * @method setMailSuport
     * @param  
     * 
     * @return $this;
     */     
     public function setMailSuport($value){
         
        $break = "\n";
        
        $cols = 50;
         
        $srv = $this->getServerSnapShotArray();
        
        $this->mail['subtitle'] .= " - {$srv['login']}";
        
        $this->mail['message'] = str_repeat("-=", $cols).$break
                                .'Data/Hora..: '.date('d-m-Y H:i:s ').$break
                                .'Domínio....: '.vsprintf("%-20s", substr($srv['s_name'],0,20)).$break
                                .'Usuário....: '.vsprintf("%04d", $srv['logado']).' - '.vsprintf("%-15s",substr($srv['login'],0,15)).' '.$break
                                .'User Agent.: '.$srv['u_agent'].$break
                                .'PHP/Versão.: '.$srv['php_version'].$break
                                .'URL........: '.'http://'.$srv['s_name'].$srv['s_url'].$break
                                .str_repeat("-=", $cols).$break
                                .$break
                                .$value.$break;
        
        return $this;
         
     }
     
    /**
     * Informa ao suporte sobre lote de erros
     * 
     * @access public
     * @method sendMamilSuport
     * @param  
     * 
     * @return $this
     */     
     public function sendMailSuport(){
         
        try {
             
            $this->mail['header']  = "MIME-Version: 1.1\n"
                                   . "Content-type: text/plain; charset=UTF-8\n"
                                   . "From: {$this->mail['to']}\n"
                                   . "Return-Path: {$this->mail['to']}\n";            
            
            if(mail("{$this->mail['to']}", "{$this->mail['subtitle']}", "{$this->mail['message']}", "{$this->mail['header']}", "-f{$this->mail['to']}")){
            
                $this->set("# Notificação de erro enviada para {$this->mail['to']}",E_FRAMEWORK_LOG);            
            
            }
            else{
                
                $this->set("# Não foi possível enviar email de notificação de erro para {$this->mail['to']}",E_FRAMEWORK_LOG);                
                
            }    
             
        } catch (\Exception $ex) {
             
             $this->set("# Falhou geral da notificação por email",E_FRAMEWORK_WARNING,$ex);

        }
        
        return $this;
         
        
    }
     
    /**
     * Método que registra um log de erros do sistema
     * 
     * @access public
     * @method log
     * @param  
     * 
     * @return 
     */     
//     public function log($msg){
//         
//        $file = ROOT_DIR.'error.log';
//         
//        if (!$handle = fopen($file, "a+"))  exit("Erro abrindo arquivo ({$file})");
//        
//        chmod($file, 0777);        
//        
//        if (!fwrite($handle, $msg)) exit("Erro escrevendo no arquivo ({$filename2})");
//
//        fclose($handle);         
//        
//     }
     
    /**
     * Método para criar o arquivo de dump
     * 
     * @access public
     * @method createFile
     * @param
     * 
     * @return 
     */     
    public function createFile($value) {    
        
        try {
             
            $this->setValue(0);
            
            if(!file_exists($value)) {

                if($handle = fopen($value,'wb')) {
                    
                    if(!chmod($handle, 0777)) $this->set(sprintf(_("Não foi possível alterar o atribudo do arquivo de dump criado em %s"),$value),E_FRAMEWORK_ERROR);
                    
                    fclose($handle);
                    
                }
                else {
                    
                    $this->set(sprintf(_("Não foi possível criar o arquivo de dump em %s"),$value),E_FRAMEWORK_ERROR);

                }
                

            }        

            $this->setValue(1);
                    
        } catch (\Exception $ex) {
             
            $this->setValue(0)->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);

        }   
        
        return $this;
        
    }
    
    /**
     * Método que salva um dump de arquivo 
     * 
     * @access public
     * @method dump
     * @param  string
     * 
     * @return string or $this
     */     
     public function dump($value){
         
        if(isset($value)){         
         
            if(is_array($value)) {

                foreach ($value as $k => $v) {

                    switch ($k) {
                        case 'message':
                            $message = $v;
                            break;
                       case 'code':
                            $code = $v;
                            break;                        
                        default:
                            break;
                    }
                }
                    
            } else {

                $code = $value;
                
            }
            
            $this->buffer = (isset($message) ? "{$message}\n" : "" )."{$code}\n\n";
         
            /**
             * verifica se existe o arquivo e caso não exista cria
             */            
            if($this->createFile($file = ROOT_DIR.'dump.log')->isOk()) {
                
                if (!$handle = fopen($file, "a+"))  $this->set(sprintf(_("Erro abrindo arquivo (%s)"),$file),E_FRAMEWORK_WARNING);        

                fwrite($handle, $this->buffer);

                fclose($handle); 
                
            }  
            else {
                
                $this->set(sprintf(_("Não foi possível criar o arquivo de dump em %s"),$file),E_FRAMEWORK_WARNING);
                
            }

            return $this;
        
        }
        else {
            
            return $this->buffer;
             
        }
        
     }
     
     public function showLastDumpFile($value) {
        
        if (is_array($value)) {

             foreach ($value as $k => $v) {

                 switch ($k) {
                     case 'lines':
                         $qtd_rows = $v;
                         break;
                     case 'session':
                         $this->session = $v;
                         break;
                     case 'highlight':
                         $this->highlight = $v; 
                         break;
                     default:
                         break;
                 }

             }

         } 
         else {

             $qtd_rows = empty($value) ? 100 : $value;

         }         

        $file = ROOT_DIR.'dump.log';
        $handle = fopen($file, "r");
        $linecounter = $qtd_rows;
        $pos = -2;
        $beginning = false;
        $this->lines = array();
        
        while ($linecounter > 0) {
            $t = " ";
            while ($t != "\n") {
              if(fseek($handle, $pos, SEEK_END) == -1) {
                   $beginning = true; break; 
              }
              $t = fgetc($handle);
              $pos --;
            }

            if($beginning) rewind($handle);
            
            $this->line = fgets($handle);
            
            if(!empty($this->line) && count($this->lines) <= $qtd_rows ) {
                $linecounter--;
                $this->lines[$lines-$linecounter-1] = $this->line;
            }
            
            if($beginning) break;
        }
        fclose ($handle);
        
        $this->showLogInHtmlTable();

    }
    
    public function showLogInHtmlTable(){
         
         try {
        
            echo '<pre>';
            echo '<table>';
            foreach($this->lines as $key => $value) {
                echo $value;
            };
            echo '</table>';
            echo '</pre>';
             
            $this->setValue(1);
                    
         } catch (\Exception $ex) {
             
            $this->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);

         }
        
        return $this;
        
    }      
     
}

// assert_options(ASSERT_ACTIVE, 1); 
// assert_options(ASSERT_WARNING, 0);
// assert_options(ASSERT_BAIL, 0);
// assert_options(ASSERT_QUIET_EVAL, 0);
// assert_options(ASSERT_CALLBACK, '\GoFast\Lib\assert_callcack'); // Função do usuário para chamar quando uma afirmação falhar

// set_error_handler('\GoFast\Lib\error_handler');                 // Função de usuário que controla erros irrecuperáveis
// set_exception_handler('\GoFast\Lib\exception_handler');         // Função de usuário que controla excessões

// register_shutdown_function('\GoFast\Lib\shutdown_handler');     // Primeira função a ser executada ao final de um erro ou excessão

/**
 * Função do usuário a ser chamada para tratar erro quando uma afirmação do usuário falhar
 * 
 * @access public 
 * @method assert_callcack
 * @param
 * 
 * @return 
 */   
function assert_callcack($file, $line, $message) {
    
    echo "assert_callcack - {$message}<br/>";
    
    //$error = new \lib\ErrorClass($message, E_FRAMEWORK_LOG, $file, $line);
    
    
}

/**
 * Primeira função a ser executada ao final de um erro ou excessão
 * 
 * Obs: Simular erro com a função: assert('mysql_query("")');
 * 
 * @param
 * 
 * @return 
 */ 
function shutdown_handler() {
    
    if (null !== $error = error_get_last()) {

        $code     = $error["type"];
        $file     = $error["file"];
        $line     = $error["line"];
        $message  = $error["message"];
        
    }            
    else {
        
        $code     = E_CORE_ERROR;
        $file     = "unknown file";
        $line     = 0;
        $message  = "shutdown";
        
    }

    if ((error_reporting() & $code) || $code == E_ERROR || $code == E_FRAMEWORK_ERROR) {
        $value = [];
        $error = (new \GoFast\Lib\Error($value))->set("# shutdown_handler - {$message}  {$file}:{$line}",E_FRAMEWORK_LOG);    
    }   
    
    // echo "shutdown_handler - {$code} - {$message}<br/>";
        
    //$error = new \lib\ErrorClass($message, $code, $file, $line);
    
    
}



/**
 * Função que controla erros irrecuparáveis
 * 
 * @param
 * 
 * @return 
 */  
function error_handler($code, $message, $file, $line, $vars) {

    // echo '<pre>';
    // debug_print_backtrace();
    // echo '</pre>';
    // foreach(debug_backtrace() as $key => $value) {
    //     echo "{$value['file']} - {$value['line']} - {$value['message']}<br/>";
    // } 
    
    // echo "# error_handler - {$message}  {$file}:{$line}<br/>";

    // if ((error_reporting() & $code) || $code == E_ERROR || $code == E_FRAMEWORK_ERROR) {
    //     $value = [];
    //     $error = (new \GoFast\Lib\Error($value))->set("# error_handler - {$message}  {$file}:{$line}",E_FRAMEWORK_LOG);    
    // }
    
    
}

/**
 * Função que controla excessões e manipula a classe Exception
 * 
 * @param
 * 
 * @return 
 */  
function exception_handler($e) {

    // foreach(debug_backtrace() as $key => $value) {
    //     echo "{$value['file']} - {$value['line']} - {$value['message']}<br/>";
    // } 
    
    // echo "# exception_handler - {$e->getMessage()} {$e->getFile()}:{$e->getLine()}<br/>";
    
    // if ((error_reporting() & $e->getCode()) || $e->getCode() == E_ERROR || $e->getCode() == E_FRAMEWORK_ERROR) {
        
    //     $value = [];
    //     return (new \GoFast\Lib\Error($value))->set("# exception_handler - {$e->getMessage()} {$e->getFile()}:{$e->getLine()}",E_FRAMEWORK_LOG,$e);    
    // }
    
    
}

