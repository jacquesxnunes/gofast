<?php

namespace GoFast\Kernel;

class Core
{
    public static $instance;
    
    public $fw;
    public $super_class;


    protected $value;
    protected $names = array(); 
    protected $id_master;     // Variável que define o cliente que esta instanciando a classe
    protected $name_master = '';  // Variável que registra o nome do cliente master    
    protected $calling_class = 'nothing';
    protected $charset;

    /**
     * Médodo que constroi a classe com um vetor contendo as configurações do framework
     * 
     * @access public
     * @method __construct
     * @param
     * 
     * @return $this
     */    
    public function __construct($value = null) {
        
        try {

            \GoFast\Lib\Config::_id(\GoFast\Lib\Config::ID_FW);
            \GoFast\Lib\Config::_setFile(\GoFast\Lib\Config::PATH_FW);
            \GoFast\Lib\Config::_title('master_domain');
            \GoFast\Lib\Config::_key(DOMAIN);;

            $this->setIdMaster(\GoFast\Lib\Config::_val());

            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    switch ($k) {
                        case 'charset':
                            $this->setCharset($v);
                            break;            
                        case 'id_master':
                            $this->setIdMaster($v);
                            break;
                        case 'name_master':
                            $this->setNameMaster($v);
                            break; 
                        case 'calling_class':
                            $this->setCallingClass($v);                              
                        default:
                            break;
                    }
                }
            }            
            
            $this->setValue(1);
          
            
        }    
        catch (\Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }          
        
        return $this;
            
    }   
    
   /**
    * Define o método setDefault padrão em todas classes de core

    * @param
    * 
    * @return $this
    */      
    public function setDefault() {
        
        return $this;

    }

   /**
    * Define o Handle da Super Classe
    * 
    * @access public
    * @method setSuperClass
    * @param
    * 
    * @return $this
    */      
    public function setSuperClass(&$value = null) {

        $this->fw = $this->super_class = $value;
        
        return $this;

    }

    public function setCallingClass($value = null)
    {

        $this->calling_class = $value;

        return $this;
    }      

    public function setIdMaster($value = null)
    {

        $this->id_master = $value;

        return $this;
    }    


    /**
     * PHP-DOC 
     * 
     * @name setMaster
     * 
     * @internal - Identifica o dom�nio que est� acessando a classe para poder definir o id_master do cliente e permitir execu��es espec�ficas na rotinas de classe
     * 
     */
    public function setMaster($value = null)
    {

        $this->setIdMaster($value);

        return $this;
    }

    

    /**
     * Método para definir o nome master corrente
     * 
     * @access public
     * @method setNameMaster
     * @param  
     * 
     * @return $this
     */
    public function setNameMaster($value = null)
    {

        $this->name_master = $value;

        return $this;
    }

      /**
     * Método que define um código de pagina a ser utilizado pelo banco de dados
     * 
     * @param  
     * 
     * @return $this
     */      
    public function setCharset($value = null) { 
        
        $this->charset = $value;
        
        return $this;

    }
    
    /**
     * Método que obtem o código de página definido de forma padrão ou pelo usuário
     * 
     * @param  
     * 
     * @return $this
     */      
    public function getCharset() { 
        
        return strtolower($this->charset);
        
    }
    /**
     * Método para obter o id do cliente em execução
     * 
     * @access public
     * @method getMaster
     * @param
     * 
     * @return 
     */
    public function getMaster()
    {

        return $this->getIdMaster();
    }

    /**
     * Método para obter o id do cliente em execução
     * 
     * @access public
     * @method getIdMaster
     * @param
     * 
     * @return 
     */
    public function getIdMaster()
    {

        return $this->id_master;
    }

    /**
     * Método para retorna o nome do master corrente
     * 
     * @access public
     * @method getNameMaster
     * @param  
     * 
     * @return string
     */
    public function getNameMaster()
    {

        return $this->name_master;
    }

    /**
     * Método para retorna o nome do master corrente
     * 
     * @access public
     * @method getNameMaster
     * @param  
     * 
     * @return string
     */
    public function getStaticClassName()
    {

        return static::class;
    }    


    public function getCallingClass()
    {

        return $this->calling_class;
    }     

    /**
     * Médodo para obter a correspondência de código para o master em execução
     * 
     * @access public
     * @method getKeyMasterInArray
     * @param
     * 
     * @return $this
     */
    public function getKeyMasterInArray($value = null)
    {

        try {

            $this->setValue(0);

            if (!$array = explode(',', $this->getKeyMaster($value))) $this->error->set(array(8, __METHOD__ . ' em $this->fw->getKeyMaster(' . $value . ')'), E_FRAMEWORK_ERROR);

            foreach ($array as $k => $v) {

                $keys[] = explode('=', $v);
            }

            /**
             * A forma de carga do array passou a ser feita por eval para permitir o uso de constantes do sistema no uso de chaves
             */
            foreach ($keys as $k => $v) {

                eval('$return[' . $v[0] . '] = $v[1];');
            }

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);

            $this->setValue(0);
        }

        return $return;
    }    
    // public static function getInstance() {
    //     if (self::$instance === null) {
    //         self::$instance = new self();
    //     }
    //     return self::$instance;
    // }    


    public static function getInstance() {
        if (!isset(static::$instance)) {
            static::$instance = new static;
        }
        return static::$instance;
    }    
    
   /**
    * Obtem o ponteiro da Super Classe
    * 
    * @access public
    * @method getSuperClass
    * @param
    * 
    * @return $this
    */       
    public function getSuperClass() {

        return $this->super_class;

    }     

    /**
     * Define valor para retorno no uso de métodos encadeados
     * 
     * @param $value
     * 
     * @return $this
     */
    protected function setValue($value = null)
    {

        $this->value = $value;

        return $this;
    }

    public function __toString()
    {

        return (string)$this->value;
    }

    /**
     * Define valor para retorno no uso de métodos encadeados
     * 
     * @return integer $this->value
     */
    public function isOk()
    {

        return (int)$this->value;
    }

    /**
     * Serializa todas as classes do framework em uma sessão de usuário para salvar seu estado atual
     * 
     * @access public
     * @method save
     * @param
     * 
     * @return $this    
     * 
     */
    public function save($value = null)
    {

        try {

            $data = $this;
            $session = session_id();
            $state = 1;


            if (is_array($value)) {

                foreach ($value as $k => $v) {

                    switch ($k) {
                        case 'id':
                            $id = $v;
                            break;
                        case 'data':
                            $data = $v;
                            $state = 0;
                            break;
                        case 'session':
                            $session = $v;
                            break;
                        default:
                            break;
                    }
                }
            } else {

                $id = empty($value) ? 'var' : $value;
            }

            $ext = $state ? '.obj' : '.var';

            $file = sys_get_temp_dir() . DIRECTORY_SEPARATOR . sha1("store-{$session}-{$id}-{$ext}");

            $buffer = is_object($data) ? $data = serialize(get_object_vars($data)) : $data;

            if (!file_put_contents($file, gzdeflate($buffer, 9))) $this->error->set('Nâo foi possível salvar o estado do framework em ' . $file, E_FRAMEWORK_ERROR);

            if (!file_exists($file)) $this->error->set('Arquivo de ' . $state ? 'estado do servidor' : 'cache' . ' inexistente no momento do registro', E_FRAMEWORK_ERROR);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }

    /**
     * Unserializa todas as classes do framework em uma sessão de usuário para recuperar o seu estado
     * 
     * @access public
     * @method load
     * @param
     * 
     * @return $this || string   
     * 
     */
    public function load($value = null)
    {

        try {

            $session = session_id();
            $objects = NULL;
            $state = 1;

            if (is_array($value)) {

                foreach ($value as $k => $v) {

                    switch ($k) {
                        case 'id':
                            $id = $v;
                            break;
                        case 'state':
                            $state = $v;
                            break;
                        case 'session':
                            $session = $v;
                            break;
                        default:
                            break;
                    }
                }
            } else {

                $id = empty($value) ? 'var' : $value;
            }

            $ext = $state ? '.obj' : '.var';

            $file = sys_get_temp_dir() . DS . sha1("store-{$session}-{$id}-{$ext}");

            if (file_exists($file)) {
                $buffer = gzinflate(file_get_contents($file));

                if ($state) {

                    $objects = unserialize($buffer);

                    foreach ($objects as $k => $v) {

                        $this->$k = $v;
                    }
                }
            } else {
                $buffer = '';
                if ($state) $this->error->set('Arquivo de estado de servidor inexistente', E_FRAMEWORK_ERROR);
            }

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $state ? $this : (string)$buffer;
    }

    /**
     * Método para redirecionamento de requisição
     * 
     * @method setDefault
     * @param  
     * 
     * @return $this
     */
    public function changeFieldNameToClassMethod($value = null)
    {

        if (array_key_exists($value, $this->names)) {       
            
            return $this->names["{$value}"];
        }


        $array = str_split($value);

        $string = '';

        $upper = 0;

        foreach ($array as $key => $value) {

            if ($upper || $key == 0) {
                $array[$key] = ucfirst($array[$key]);
                $upper = 0;
            }

            switch ($value) {
                case '-':
                case '_':
                    unset($array[$key]);
                    $upper = 1;
                    break;
                default:
                    $string .= $array[$key];
                    break;
            }
        }

        return $string;
    }

    /**
     * Cria um nome de tabela de acordo com o Alias
     * 
     * @access protected
     * @method changeClassMethodToFieldName
     * @param
     * 
     * @return $this
     */
    public function changeClassMethodToFieldName($value = null)
    {

        $array = str_split($value);

        $string = '';

        $upper = 0;

        foreach ($array as $k => $v) {

            if ($k == 0) {

                $array[$k] = strtolower($array[$k]);
            } else {

                if ($v == strtoupper($v) && !is_numeric($v)) $array[$k] = '_' . strtolower($array[$k]);
            }

            $string .= $array[$k];
        }

        $string = str_replace('set_', '', $string);

        return $string;
    }

    /**
     * Cria um nome de tabela de acordo com o Alias
     * 
     * @access protected
     * @method makeTableName
     * @param
     * 
     * @return $this
     */
    protected function createTableNameFrom($value = null)
    {

        try {

            $array = str_split($value);

            $string = '';

            $upper = 0;

            foreach ($array as $k => $v) {

                if ($k == 0) {

                    $array[$k] = strtolower($array[$k]);
                } else {

                    if ($v == strtoupper($v) && !is_numeric($v)) $array[$k] = '_' . strtolower($array[$k]);
                }

                $string .= $array[$k];
            }
        } catch (Exception $ex) {
        }

        return $string;
    }

    /**
     * Cria um alias a partir de nomes de tabela
     * 
     * @access protected
     * @method makeTableName
     * @param
     * 
     * @return $this
     */
    protected function createAliasNameFrom($value = null)
    {

        $array = str_split($value);

        $string = '';

        $next_upper = 0;

        foreach ($array as $k => $v) {

            if ($k == 0 || $next_upper) {
                $array[$k] = strtoupper($array[$k]);
                $next_upper = 0;
            } else {
                if ($v == '_' ) {
                    $next_upper = 1;
                    $array[$k] = '';
                }
                else {
                    $array[$k] = strtolower($array[$k]);
                }    
            }

            $string .= $array[$k];
        }

        return $string;
    }    

    /** 
     * Esse método adiciona uma propriedade dinamicamente a uma classe
     * 
     * @access public
     * @method createDynamicClassMethod
     * @param  array
     * 
     * @return $this
     */
    public function createDynamicClassMethod($value = null)
    {

        try {

            $this->setValue(0);

            if (is_array($value)) {

                foreach ($value as $k => $v) {

                    switch ($k) {
                        case 'class_name':
                            $class_name = $v;
                            break;
                        case 'method_name':
                            $method_name = $v;
                            break;
                        case 'param':
                            $param = $v;
                            break;
                        case 'code':
                            $code = $v;
                            break;
                        default:
                            break;
                    }
                }
            }


            \classkit_method_add(
                $class_name,
                $method_name,
                $param,
                $code,
                CLASSKIT_ACC_PUBLIC
            );
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);

            $this->setValue(0);
        }

        return $this;
    }


    /**
     * Método para redirecionamento de requisição
     * 
     * @param (array || strig) - $value 
     * 
     * @return
     */
    public function redirect($value = null)
    {


        $code = 200;

        if (is_array($value)) {

            foreach ($value as $k => $v) {

                switch ($k) {
                    case 'url':
                        $url = $v;
                        break;
                    case 'code':
                        $code = $v;
                        break;
                    default:
                        $k = $v;
                        break;
                }
            }
        } else {
            $url = $value;
        }

        http_response_code($code); // Define o status de retorno do servidor
        header("Location: {$url}");
        die(); // Adição para redirecionamento seguro no caso de rastreadores que ignorem o header.

    }


    /**
     * Método para redirecionamento de requisição
     * 
     * @param 
     * 
     * @return object $this
     */
    public function sendRequestGlobalVariables()
    {

        foreach ($_REQUEST as $k => $v) {

            if (isset($$k)) $this->error->set("Não é possível declarar a variável {$k} como global pois já existe", E_FRAMEWORK_ERROR);

            global $$k;

            $$k = $v;

            //$$k = iconv(mb_detect_encoding($$k, 'auto'), $charset, $$k);

            /**
             * Caso o parámetro saidas seja um array então converte em uma string separada por vírgula e coloca um underline no início da variável
             */
            if (is_array($v)) {

                foreach ($v as $f_key => $f_value) {

                    if (is_array($f_value)) {

                        /**
                         * Verifica se a requisição está encode
                         */
                        //if (urlencode(urldecode($f_value['value'])) === $f_value['value']) $f_value['value'] = urldecode($f_value['value']);

                        //$f_value['value'] = iconv(mb_detect_encoding($f_value['value'], 'auto'), $charset, $f_value['value']); 

                        $array[str_replace('[]', '', $f_value['name'])][] = $f_value['value'];
                    } else {

                        /**
                         * Verifica se a requisição está encode
                         */
                        //if (urlencode(urldecode($f_value)) === $f_value) $f_value = urldecode($f_value);

                        //$f_value = iconv(mb_detect_encoding($f_value, 'auto'), $charset, $f_value);

                        $array[str_replace('[]', '', $k)][] = $f_value;
                    }
                }

                foreach ($array as $f_key => $f_value) {

                    $f_key = "_{$f_key}";

                    global $$f_key;

                    $$f_key = implode(',', $f_value);
                }
            }
        }

        return $this;
    }
}
