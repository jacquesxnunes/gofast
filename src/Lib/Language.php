<?php 
/**
 * Classe para ativação da language
 * 
 * Via cmd do windows: xgettext --from-code=utf-8 -o c:\projetos\f71framework\locale\es_ES\framework.pot -s -D c:\projetos\f71framework\class\
 * Recursive xgettext: find . -iname "*.php" | xargs xgettext --from-code=utf-8 -o c:\projetos\f71framework\locale\es_ES\LC_MESSAGES\framework.pot
 * 
 * @file      language.class.php
 * @license   
 * @link      
 * @copyright 2017 F71
 * @author    Jacques <jacques@f71.com.br>
 * @package   Curl
 * @access    public  
 * @version:  3.0.0000 - 25/05/2016 - Jacques - Versão Inicial 
 * @todo 
 * @example:  
 * 
 * 
 */
namespace GoFast\Lib;

use GoFast\Kernel\Core;

class Language extends Core 
{
        

    public static $instance;      
    
    public  $error;
    public  $translator;

    private $locales = array(
        'gettext' => array(
                            'charset' => 'utf8',
                            'domain' => 'messages',
                            'default' => '',
                            'all' => LOCALES_GETTEXT
                          )
    );    
    
    use \GoFast\Lib\Bridge;    
    
   /**
    * Método construtor de classe que pode iniciar via array de parâmetros
    * 
    * @access private
    * @method __construct
    * @param
    * 
    * @return $this
    */      
    public function __construct($value = null) {
  
        $this->createCoreClass($value);
        
        $this->init($value);
        
    }    
    
    /**
     * Método que define Define valores default da classe
     * 
     * @access public
     * @method setDefault
     * @param  
     * 
     * @return $this
     */
    public function setDefault() {

        $this->locales['gettext']['all']     = LOCALES_GETTEXT;
        $this->locales['gettext']['default'] = LOCALES_GETTEXT_DEFAULT;
        
        return $this;
    }       
    
   /**
    * Método executado no instânciamento da classe para execuções de procedimentos iniciais
    * 
    * Obs: O Cookie de localização tem precedência no set de Locale
    * 
    * @access public
    * @method init
    * @param
    * 
    * @return $this
    */      
   public function init($value = null) {
       
       try {
           
            if (is_array($value)) {

                foreach ($value as $k => $v) { 

                    switch ($k) {
                        case 'charset':
                            $this->charset($v);
                            break;                    
                        case 'locale':
                            $this->locale($v);
                            break;
                        case 'domain':
                            $this->domain($v);
                            break;                    
                        default:
                            break;
                    }

                }

            }   
            else {

                $this->locale($value);

            } 

            $this->setEnvironment();           
           
            $this->setValue(1);
            
        } catch (\GoFast\Lib\FrameWorkException $ex) {

            $this->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);

            $this->setValue(0);
        }
        
        return $this;
        
    }
    
    
   /**
    * Método utilizado pela classe language para determinar o charset de linguagem I18N a ser utilizado
    * 
    * @access public
    * @method charset
    * @param
    * 
    * @return $this
    */      
   public function charset($value = null) {   
       
        if(isset($value)) {
            
            $this->locales['gettext']['charset'] = $value;
            
            return $this;
        }
        else {
            
            return str_replace('-', '', $this->locales['gettext']['charset']);
            
        }
       
   }    
    
   /**
    * Método utilizado pela classe language para determinar o padrão de linguagem I18N a ser utilizado
    * 
    * @access public
    * @method locale
    * @param
    * 
    * @return $this
    */      
   public function locale($value = null) {   
       
        if(isset($value)) {
            
            $this->locales['gettext']['default'] = $value;
            
            return $this;
        }
        else {
            
            return str_replace('-', '_', $this->locales['gettext']['default']);
            
        }
       
   }
   
   /**
    * Método utilizado pela classe language para determinar o domain de linguagem I18N a ser utilizado
    * 
    * @access public
    * @method domain
    * @param
    * 
    * @return $this
    */      
   public function domain($value = null) {   
       
        if(isset($value)) {
            
            $this->locales['gettext']['domain'] = $value;
            
            return $this;
        }
        else {
            
            return $this->locales['gettext']['domain'];
            
        }
       
   }   
   
   /**
    * Método utilizado pela classe language para determinar o padrão de linguagem I18N a ser utilizado via ambiente emulado
    * 
    * @access public
    * @method setEnvironmentEmulated
    * @param
    * 
    * @return $this
    */      
    public function setEnvironmentEmulated() {   
        
        try {
            
            if(!class_exists('\Gettext\Translator')) $this->error->set("# Não é possível usar o modo emulado de gettext pois a classe não está instalada na pasta vendor", E_FRAMEWORK_ERROR);
                
            if(empty($this->locale())) $this->locale(empty($_COOKIE['locale']) ? isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE'])[0] : LOCALES_GETTEXT_DEFAULT : $_COOKIE['locale']);            

            $this->locale(strpos(LOCALES_GETTEXT,$this->locale()) ? $this->locale() : LOCALES_GETTEXT_DEFAULT);

            $this->translator = new \Gettext\Translator();
            $this->translator->loadTranslations(\Gettext\Translations::fromPoFile(ROOT_LOCALE.$this->locale().DS.'LC_MESSAGES'.DS.'messages.po'));
                
            $this->setValue(1);
            
        } catch (\GoFast\Lib\FrameWorkException $ex) {

            $this->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);

            $this->setValue(0);
        }

        return $this;
        
    }    
   
   
   /**
    * Método utilizado pela classe language para determinar o padrão de linguagem I18N a ser utilizado
    * 
    * @access public
    * @method setEnvironment
    * @param
    * 
    * @return $this
    */      
    public function setEnvironment() {   
        
        try {
            
            if(false===function_exists('gettext')) {
                
                if($this->setEnvironmentEmulated()->isOk()) {
                    $this->error->set("# A extensão gettext não está ativa em seu PHP, portanto será usado o modo emulado de gettext para tradução",E_FRAMEWORK_NOTICE);                   
                }
                else {
                    $this->error->set("# A extensão gettext não está ativa em seu PHP nem a classe emuladora, portanto a tradução não poderá ser executada",E_FRAMEWORK_WARNING);
                }
                
            }
            else {
                
                if(empty($this->locale())) $this->locale(empty($_COOKIE['locale']) ? isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE'])[0] : LOCALES_GETTEXT_DEFAULT : $_COOKIE['locale']);            

                $this->locale(strpos(LOCALES_GETTEXT,$this->locale()) ? $this->locale() : LOCALES_GETTEXT_DEFAULT);

                if(strpos(strtolower(php_uname()),'linux')!==false){

                    $os = "Linux";

                    if(empty($result = setlocale(LC_MESSAGES, $this->locale()))) $this->error->set("# Não foi possível definir o ambiente LC_MESSAGES={$this->locale()}",E_FRAMEWORK_NOTICE);                           

                }
                elseif(strpos(strtolower(php_uname()),'windows')!==false){

                    $os = "Windows";

                    if(!putenv('LC_MESSAGES='.$this->locale().'.'.$this->charset())) $this->error->set("# Não foi possível definir o ambiente LANGUAGE={$this->locale()}.{$this->charset()}",E_FRAMEWORK_NOTICE);
                    //if(!putenv('LANGUAGE='.$this->locale().'.'.$this->charset())) $this->error->set("# Não foi possível definir o ambiente LANGUAGE={$this->locale()}.{$this->charset()}",E_FRAMEWORK_NOTICE);

                }
                elseif(strpos(strtolower(php_uname()),'darwin')!==false){

                    $os = "MAC";

                }
                else {

                    $os = "Sistema operacional não reconhecido";

                }

                if(empty($result = bindtextdomain($this->domain(), ROOT_LOCALE))) $this->error->set("# Não foi possível encontrar locale no caminho {ROOT_LOCALE} para o domínio {$this->domain()}",E_FRAMEWORK_NOTICE);
                if(empty($result = bind_textdomain_codeset($this->domain(), $this->charset()))) $this->error->set("# Não foi possível definir o charset {$this->charset()} para o domínio {$this->domain()}",E_FRAMEWORK_NOTICE);
                if(empty($result = textdomain($this->domain()))) $this->error->set("# Não foi possível definir o domínio para {$this->domain()}",E_FRAMEWORK_NOTICE);

                $this->error->set("# A extensão gettext está ativa em seu PHP e será usado para tradução em ambiente {$os}",E_FRAMEWORK_NOTICE);
                
                
            }


            $this->setValue(1);
            
        } catch (\GoFast\Lib\FrameWorkException $ex) {

            $this->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);

            $this->setValue(0);
        }

        return $this;
        
    }  
    
   /**
    * Método utilizado pela classe para echoar um texto traduzido
    * 
    * @access public
    * @method _
    * @param  string
    * 
    * @return $this
    */      
    public function _($value = null) {   
        return $this->translator->gettext($value);
    }     
    
}

