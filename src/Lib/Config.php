<?php
/**
 * Procedimento para geração de arquivo remessa
 * 
 * @file      remessa.class.php
 * @license   http://www.gnu.org/licenses/gpl-3.0.txt GNU General Public License
 * @link      http://www.f71lagos.com/intranet/?class=financeiro/cnab240/remessa
 * @copyright 2016 F71
 * @author    Jacques <jacques@f71.com.br>
 * @package   ConfigClass
 * @access    public  
 * @version:  3.0.0000 - 13/01/2016 - Jacques - Versão Inicial 
 * @version:  3.0.9999 - 11/07/2016 - Jacques - Adicionado opção de enviar parâmetro na criação da classe para definir o nome do arquivo de configuração
 * @todo 
 * @example:  
 * 
 * 
 */
namespace GoFast\Lib;

use GoFast\Kernel\Core;

class Config extends Core
{   

    const   ID_FW =    'fw.ini';
    const   ID_SETUP = 'setup.ini';
    const   PATH_FW = ROOT_DIR . 'etc' . DIRECTORY_SEPARATOR . self::ID_FW;
    const   DOMAIN = DOMAIN;

    public static $instance;     

    public  $error;    
    
    public  $setup;
    private $title;             
    private $key;
    private $file_name;
    private $id;

    public static $_setup;
    public static $_title;
    public static $_key;
    public static $_file_name;
    public static $_id;
    
    
    use \GoFast\Lib\Bridge;    
    
    /**
     * Médodo que constroi a classe com um vetor contendo as configurações do framework
     * 
     * @access public
     * @method __construct
     * @param
     * 
     * @return $this
     */    
    public function __construct($value = null) {
        
        try {
                  
            parent::__construct($value);         
            
            $this->createCoreClass($value);            
            
        }    
        catch (\Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }          
        
        return $this;
            
    }   
    
    /**
     * Define o nome de arquivo de configuração utilizado pela classe
     * 
     * @param $value
     * 
     * @return $this
     */       
    public function setFile($value = null){      

        $this->file_name = $value;

        if(isset($this->setup[$this->id])) return $this;

        if(!file_exists($this->file_name)) {
            $this->error->set(array(9,__METHOD__."({$this->file_name})"),E_FRAMEWORK_ERROR);
        }    

        $this->setup[$this->id] = parse_ini_file($this->file_name,true);

        return $this;

    }

    

    /**
     * Define o nome de arquivo de configuração utilizado pela classe
     * 
     * @param $value
     * 
     * @return $this
     */       
    public function reload($value = null){      

        $this->file_name = $value;

        if(!file_exists($this->file_name)) {
            $this->error->set(array(9,__METHOD__."({$this->file_name})"),E_FRAMEWORK_ERROR);
        }    

        $this->setup[$this->id] = parse_ini_file($this->file_name,true);

        return $this;

    }

    /**
     * Define o nome de arquivo de configuração utilizado pela classe
     * 
     * @access public
     * @method setFilename
     * @param $value
     * 
     * @return $this
     */
    public static function _id($value = null)
    {

        self::$_id = $value;

    }

    /**
     * Define o nome de arquivo de configuração utilizado pela classe
     * 
     * @param $value
     * 
     * @return 
     */
    public static function _setFile($value = null)
    {

        self::$_file_name = $value;

        if (!isset(self::$_setup[self::$_id])) {
            self::$_setup[self::$_id] = parse_ini_file(self::$_file_name, true);
        }

    }

    /**
     * Define um vetor com as configurações do framework
     * 
     * @access public
     * @method title
     * @param
     * 
     * @return $this
     */
    public static function _title($value = null)
    {

        self::$_title = $value;


    }

    /**
     * Define uma chave
     * 
     * @access public
     * @method key
     * @param
     * 
     * @return $this
     */
    public static function _key($value = null)
    {

        self::$_key = $value;

    }

    /**
     * Retorna um valor de configuração
     * 
     * @access public
     * @method val
     * @param
     * 
     * @return $this
     */

    public static function _val()
    {

        return self::$_setup[self::$_id][self::$_title][self::$_key];
    }    
    
    /**
     * Define o nome de arquivo de configuração utilizado pela classe
     * 
     * @access public
     * @method setFilename
     * @param $value
     * 
     * @return $this
     */       
    public function id($value = null){      

        $this->id = $value;

        return $this;

    }     
  
    /**
     * Define valores padrão na classe
     * 
     * @access public
     * @method setDefault
     * @param
     * 
     * @return $this
     */      
    public function setDefault() {
        
        return $this;
        
    }    
    
    /**
     * Método que lê o arquivo de log e retorna as últimas 100 entradas
     * 
     * @access public
     * @method showLast
     * @param  
     * 
     * @return 
     */      
     public function showLast(){
         
         try {
             
            $file = ROOT_DIR.$this->file;
            
            $qtd_rows = 100;

            $length = 40;

            $offset_factor = 1;
            
            $bytes=filesize($file);

            $fp = fopen($file, "r") or die("Não foi possível abrir o arquivo {$file}");

            $complete=false;
            
            while (!$complete)
            {
                
                $offset = $qtd_rows * $length * $offset_factor;
                
                fseek($fp, -$offset, SEEK_END);

                if ($offset < $bytes) fgets($fp);

                $lines = array();
                
                while(!feof($fp))
                {
                    
                    $line = fgets($fp);
                    
                    array_push($lines, $line);
                    
                    if (count($lines) > $qtd_rows)
                    {
                        array_shift($lines);
                        
                        $complete = true;
                        
                    }
                    
                }

                if ($offset >= $bytes)
                    
                    $complete = true;
                
                else
                    
                    $offset_factor *= 2; 
                

            }
            
            fclose($fp);
            
//            exec("tail -n $qtd_rows $file",$lines);
            
            echo "<pre>";
            
            for($i=count($lines)-1;$i > 0;$i--){

                echo str_pad($i,3, "0", STR_PAD_LEFT).' - '.$lines[$i-1];

            }        
            
            echo "</pre>";
            
             
         } catch (\Exception $ex) {
             
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
            echo $this->error->getAllMsgCodeJson(0,'iso-8859-1');

         }
        
        return $this;
        
     }     
    
    /**
     * Obtem o nome do arquivo de configuração utilizado pela classe
     * 
     * @access public
     * @method getNameFile
     * @param $value
     * 
     * @return $this
     */       
    public function getFilename(){

        return $this->file_name;

    }     
    
    /**
     * Obtem o array de configuração
     * 
     * @access public
     * @method setup
     * @param 
     * 
     * @return array
     */       
    public function setup(){
        
        if(!file_exists($this->getFilename())) $this->error->set(array(9,__METHOD__." -> {$file}"),E_FRAMEWORK_ERROR);
        
        $setup = file($this->getFilename());
        
        $total_linhas = count($setup); 

        $linha_offset = \hub\array_find("[framework]",$setup)-1;  

        /**
         *  Exclui as linhas que precedem a configuração do framework
         */
        for ($i = 0; $i < $linha_offset; $i++) {

            unset($setup[$i]);                 

        }

        $setup = \array_values($setup);

        $setup = implode("",$setup); 

        //$setup = file_get_contents($this->getFilename(),FILE_TEXT,null);
        
        return $setup;

    }     
    
    /**
     * Define um vetor com as configurações do framework
     * 
     * @access public
     * @method title
     * @param
     * 
     * @return $this
     */       
    public function title($value = null){
        
        try {
            
            $this->title = $value;
            
        }    
        catch (\Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }          
        
        return $this;
            
    }
    
    /**
     * Define uma chave
     * 
     * @access public
     * @method key
     * @param
     * 
     * @return $this
     */      
    public function key($value = null){
        
        try {
            
            $this->key = $value;
            
        }    
        catch (\Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }          
        
        return $this;
            
    }
    
    /**
     * Médodo para obter a correspondência de código para o master em execução
     * 
     * @access public
     * @method arrayVal
     * @param
     * 
     * @return $this
     */    
    public function arrayVal(){
        
        try {
            
            $this->setValue(0);
            
            if(!$array = explode(',',$this->val())) $this->error->set('Não foi possível obter o valor da chave',E_FRAMEWORK_ERROR);

            foreach ($array as $k => $v) {

                $keys[] = explode('=',$v);

            }

            foreach ($keys as $k => $v) {

                $return[$v[0]] = $v[1];

            }    

            $this->setValue(1);
            
        } catch (\Exception $ex) {
            
            $this->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
            $this->setValue(0);
        
        }

        return $return;
        
    }   
    
    /**
     * Método para obter todos os valores do grupo
     * 
     * @access public
     * @method arrayGroupValue
     * @param
     * 
     * @return $this
     */       
    public function arrayGroupValue(){
        
        return $this->setup[$this->id][$this->title];

    }     
    
    /**
     * Retorna um valor de configuração
     * 
     * @access public
     * @method val
     * @param
     * 
     * @return $this
     */      
    
    public function val(){
        
        try {

            
            $val = "{$this->setup[$this->id][$this->title][$this->key]}";

            return $val;
            
        }    
        catch (\Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }          
        
        return 0;
            
    }
    
    

}



