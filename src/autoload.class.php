<?php
namespace GoFast;

/**
 * Classe responsável pelas classes do GoFast
 *
 * @file                autoload.class.php
 * @license		F71
 * @link
 * @copyright           2020 F71
 * @author		Jacques <jacques@f71.com.br>
 * @package             webClass
 * @access              public
 *
 * @version: 3.0.0000 - 18/09/2020 - Jacques - Versão Inicial
 *
 */
        
class Autoload {
    
    private $directorys = array(
                                'lib' =>  ROOT_VENDOR_GOFAST_SRC .'Lib', 
                                'hub' =>  ROOT_VENDOR_GOFAST_SRC .'Hub',
                                'gofast' =>  ROOT_VENDOR_GOFAST_SRC,
                                ); 
    
    public function __construct() {
        spl_autoload_extensions('.php');
        spl_autoload_register(array($this, 'load'));
    
    }
    
    private function load($value) {
        
        $extension = spl_autoload_extensions();
        $parts = explode('\\', $value);
        $vendor = strtolower(array_shift($parts));
        $file_prefix = strtolower(str_replace('Class','',array_pop($parts)));
        $namespace = implode(DS,$parts);
        $namespace .= empty($namespace)?'':DS;
        
        $file = $this->directorys[$vendor] . $namespace . $file_prefix . $extension;
        
        /**
         * Include do framework
         */
        

        if(is_file($file)) 
            include_once $file;

    }
}

if(!include_once($file = dirname(__FILE__). DIRECTORY_SEPARATOR .'inc' . DIRECTORY_SEPARATOR . 'const.inc')) die("# Não foi possível incluir {$file} a partir de ".__FILE__);

$Autoload = new Autoload();

if(!include_once($file = dirname(__FILE__). DIRECTORY_SEPARATOR .'inc' . DIRECTORY_SEPARATOR . 'dominio.inc')) die("# Não foi possível incluir {$file} a partir de ".__FILE__);

if(!include_once($file = dirname(__FILE__). DIRECTORY_SEPARATOR .'inc' . DIRECTORY_SEPARATOR . 'functions.inc')) die ("Não foi possível incluir {$file} a partir de ".__FILE__); 



