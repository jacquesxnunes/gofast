<?php
/**
 * Classe para controle de versão baseado no Git
 * 
 * @file                version.class.php
 * @license		F71
 * @link		
 * @copyright           2016 F71
 * @author		Jacques <jacques@f71.com.br>
 * @package             Version
 * @access              public  
 * 
 * @version: 3.0.0000 - 29/06/2017 - Jacques - Versão Inicial 
 * 
 * @todo 
 * 
 * (Maior).(Menor).(Compilação).(Revisão)
 * ((major.minor[.build[.revision]]) no original em inglês )
 * 
 */
namespace GoFast\Lib;

use GoFast\Kernel\Core;

class Version extends Core
{

    const FILE = 'composer.json';
    
    public static $instance;

    private $_version;
    private $major;
    private $minor;
    private $build;

    use \GoFast\Lib\Bridge;


    /**
     * Método construtor da classe
     * 
     * @access public
     * @method __construct
     * @param string $release
     * @param string $path
     * 
     * @return 
     */     
    public function __construct($value = null) {
        
        parent::__construct($value);

        $this->createCoreClass($value);
        
        $this->path = isset($value['root_dir'])?$value['root_dir']:'';
        
    }
    
    /**
     * Método que inicializa a classe com valores default
     * 
     * @access public
     * @method setDefault
     * 
     * @return 
     */     
    public function setDefault() {
        
        $this->path    = '';
        
    }   
   

    /**
     * Método que retorna a versão corrente
     * 
     * @access public
     * @method get
     * 
     * @return 
     */     
    public function get()  {
        
        if(empty($this->_version)) {

            $this->_version = $this->getGitInformation();

            print_array($this->_version);
            exit('chegou aqui');
            
            if($this->_version ) {
                
                $git = explode('-', $this->_version);
                
                $v = explode('.',$git[0]);
                
                $this->major = (int)$v[0];
                $this->minor = (int)$v[1];
                
                $this->build = (int)$git[1];
                $this->commit = (int)$git[2];
                
            }
            
        }
        
        return $this->_version;
    }
    
    /**
     * Método que retorna o número maior da versão
     * 
     * @access public
     * @method major
     * 
     * @return 
     */     
    public function major()  {
        
        return $this->major;
        
    }    
    
    /**
     * Método que retorna o número menor da versão
     * 
     * @access public
     * @method minor
     * 
     * @return 
     */     
    public function minor()  {
        
        return $this->minor;
        
    }    
    
    
    /**
     * Método que retorna a versão corrente
     * 
     * @access public
     * @method get
     * 
     * @return 
     */     
    public function build()  {
        
        return $this->build;
        
    }

        /**
     * Método que consulta o git para verificar a versão
     * 
     * @access public
     * @method getGitInformation
     * 
     * @return bool|string
     */     
    public function getCoreVersionComposer()
    {

        $file = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . self::FILE;

        if(file_exists($file)){
            return json_decode(file_get_contents($file),true)['version'];
        }
        else {
            return [];
        }

    }    
    

    /**
     * Método que consulta o git para verificar a versão
     * 
     * @access public
     * @method getGitInformation
     * 
     * @return bool|string
     */     
    private function getGitInformation($path = null)
    {
        return '';
        
        if (!is_dir($path . DIRECTORY_SEPARATOR . '.git')) {
            
            return "Diretório do git {$path} inexistente";
            
        }
        

        $process = proc_open(
            'git describe --tags ',
            array(
                0 => array("pipe", "r"),  // stdin
                1 => array("pipe", "w"),  // stdout -> we use this
                2 => array("pipe", "w")   // stderr 
            ),
            $pipes,
            $path
        );

        if (!is_resource($process)) {
            return 'Os pipe não é um recurso disponível';
        }
        
        $result = trim(stream_get_contents($pipes[2]));

        fclose($pipes[0]);        
        fclose($pipes[1]);
        fclose($pipes[2]);

        $returnCode = proc_close($process);

        if ($returnCode !== 0) {
            
            return $result;
             
        }

        return $result;
    }
}
