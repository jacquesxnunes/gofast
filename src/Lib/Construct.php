<?php
/**
 * @brief Classe para montagem de objetos de banco de dados dinamicamente 
 * 
 * @code
 * 
 * @endcode
 * 
 * @file                constructClass.php
 * @license		http://www.gnu.org/licenses/gpl-3.0.txt GNU General Public License
 * @link		http://www.f71lagos.com/intranet/?class=construct
 * @copyright           2016 F71
 * @author		Jacques <jacques@f71.com.br>
 * @package             constructClass
 * @access              public  
 * 
 * @version: 3.0.0000L - 30-11-2015 - Jacques - Versão Inicial
 * @version: 3.0.6994L - 25/02/2016 - Jacques - Adicionado o método getRowExt para execução de adição de colunas adicionais a um select
 * @version: 3.0.6994L - 26/02/2016 - Jacques - Adicionado a classe mãe file
 * @version: 3.0.7132L - 29/02/2016 - Jacques - Adicionado a classe de bibliotecas para automatização do framework
 * @version: 3.0.7222L - 02/03/2016 - Jacques - Adicionado o mmétodotodo GetFields para uso no método select 
 * @version: 3.0.7322L - 04/03/2016 - Jacques - Adicionado o mmétodotodo isOk para verificar estado do objeto apôs a execução de um método
 * @version: 3.0.7474L - 08/03/2016 - Jacques - Adicionado o método getRowExt para inclusão de propriedades extendidas da classe
 * @version: 3.0.7773L - 10/03/2016 - Jacques - Adicionado o método para verificação de uso de chaves magnéticas na jun��o de classes
 * @version: 3.0.8166L - 16/03/2016 - Jacques - Adicionado o método padrão select e update com carga de métodos extendidos para refino da operação
 * @version: 3.0.8288L - 18/03/2016 - Jacques - Adicionado o método para salvar o estado padrão de uma classe e restaura-la
 * @version: 3.0.8738L - 31/03/2016 - Jacques - Adicionado o método setDateRangeQuery que monta a condição de busca por intervalo de forma padrão
 * @version: 3.0.8738L - 31/03/2016 - Jacques - Adicionado o método setWhereQuery que monta as condições do select de acordo com as propriedades definidas na classe
 * @version: 3.0.9398L - 09/05/2016 - Jacques - Adicionado a possibilidade de conversão do tipo de colação do banco para o charset utilizado nas páginas
 * @version: 3.0.0208F - 10/05/2016 - Jacques - Adicionado o método setDefaultExt para setar valores padrão para as propriedades extendidas da classe
 * @version: 3.0.0209F - 12/08/2016 - Jacques - Adicionado o método where que controla o vetor que irá fornecer os campos de condições da query
 * @version: 3.0.0212F - 19/08/2016 - Jacques - Adicionado o método o método setDefault das classes dinâmicas no instante da criação da classe
 * @version: 3.0.0212F - 16/01/2017 - Jacques - Adicionado a variável cmbBox classe para uso na classe dinâmica
 * @version: 3.0.0212F - 03/02/2017 - Jacques - Adicionado setDefault no início do método getRow() da classe dinâmica pois quando o retorno é vazio estava ficando registrado lixo da consulta nas propriedades da classe
 * @version: 3.0.0000F - 08/03/2017 - Jacques - Fix do erro de montagem de cláusula where com string na classe construct.class 
 * @version: 3.0.0000F - 11/04/2017 - Jacques - Adicionado o método val para obter o valor de uma classe
 * 
 * @todo iconv('<?=$this->getCollationNamexCharset()?>','<?=$this->config->title('framework')->key('charset')->val()?>',$value));	
 */
namespace GoFast\Lib;

use GoFast\Kernel\Core;

class Construct extends Core {

    public static $instance;     
    
    public    $config;
    public    $error;
    public    $db;
    public    $field_key;
    
    private   $db_name;
    private   $table;
    private   $alias;
    
    private   $charsets = array(
                            'latin1_swedish_ci' => 'iso-8859-1',
                            'latin1_general_ci' => 'iso-8859-1',
                            'utf8_general_ci' => 'utf-8'
                            );
    
    private   $obj = array(
                            'column_name' => '',
                            'data_type' => '',
                            'numeric_precision' => 0,
                            'collation_name' => ''
                            );

    use \GoFast\Lib\Bridge; 

    public function __construct($value = null) {

        parent::__construct($value); 

        $this->createCoreClass($value);
        
        foreach ($value as $k => $v) {

            switch ($k) {
                case 'table':
                    $this->setTable($v);
                    break;
                case 'alias':
                    $this->setAlias($v);
                    break;
                case 'schema':
                    $this->setDbName($v);
                    break;                    
                default:
                    break;
            }
            
        }   
                    
        return $this;

    }   
     
    
    public function chkAndSetTable(){

        if((empty($this->getTable()) || !$this->chkTableExist()) && !empty($this->getAlias())) $this->setTable($this->createTableNameFrom($this->getAlias()));
        
        return $this;
        
    }

    /**
    * Esse método define o nome da tabela que irá ser instânciada dinamicamente
    * 
    * @param
    * 
    * @return $this
    */      
    public function setDbName($value = null){
        
        $this->db_name = $value;
        
        return $this;
        
    }

   /**
    * Esse método define o nome da tabela que irá ser instânciada dinamicamente
    * 
    * @param
    * 
    * @return $this
    */      
    public function setTable($value = null){
        
        $this->table = $value;
        
        return $this;
        
    }
    
   /**
    * Esse método define o nome do alias de tabela que irá ser instânciado dinamicamente
    * 
    * @access public
    * @method setClass
    * @param
    * 
    * @return $this
    */      
    public function setAlias($value = null){
        
        $this->alias = $value;
        
        return $this;
        
    }    
    
    public function setColumnName($value = null){
        
        $this->obj['column_name'] = $value;

        return $this;
        
    }

    public function setDataType($value = null){
        
        $this->obj['data_type'] = $value;

        return $this;
        
    }

    public function setNumericPrecision($value = null){
        
        $this->obj['numeric_precision'] = $value;
        
        return $this;
        
    }
    
    public function setCollationName($value = null){
        
        $this->obj['collation_name'] = $value;
        
        return $this;
        
    }
    
    public function setColumnKey($value = null){
        
        $this->obj['column_key'] = $value;
        
        return $this;
        
    }
    
    /**
    * Método para verificar a existência da tabela no banco de dados
    * 
    * @access private
    * @method chkTableExist
    * @param
    * 
    * @return boolean
    */    
    private function chkTableExist(){
        
        $this->db->setIdMaster($this->getIdMaster());
        $this->db->setQuery(SELECT,"column_name, data_type, numeric_precision, collation_name, column_key");
        $this->db->setQuery(FROM,"information_schema.columns");                    
        $this->db->setQuery(WHERE,"table_schema = '{$this->getDbName()}' AND table_name = '{$this->getTable()}'"); 
        
        $this->db->setRs()->isOk();
       
        return $this->db->getNumRows();
        
    }   
    
    public function select(){

        $this->db->setIdMaster($this->getIdMaster());    
        $this->db->setQuery(SELECT,"column_name, data_type, numeric_precision, collation_name, column_key");
        $this->db->setQuery(FROM,"information_schema.columns");                    
        $this->db->setQuery(WHERE,"table_schema = '{$this->getDbName()}' AND table_name = '{$this->getTable()}'"); 
        
        if(!$this->db->setRs()->isOk()) $this->error->set("# Houve um erro na query de consulta do método select da classe constructClass",E_FRAMEWORK_ERROR);
        
        if(!$this->db->getNumRows()) $this->error->set("# A tabela [{$this->getTable()}] não foi encontrada no db {$this->getDbName()}",E_FRAMEWORK_WARNING);
        
        return $this;
        
    }
    
    public function getColumnName(){
        
        return $this->obj['column_name'];
        
    }
    
    public function getDataType(){
        
        return $this->obj['data_type'];
        
    }

    public function getNumericPrecision(){
        
        return $this->obj['numeric_precision'];
        
    }
    
    public function getCollationName(){
        
        return $this->obj['collation_name'];
        
    }
    
    public function getColumnKey(){
        
        return $this->obj['column_key'];
        
    }
    
    public function getCollationNamexCharset(){
        
        return $this->charsets[$this->getCollationName()];
        
    }
    
    private function getTypeDataGeneral(){
        
        switch($this->getDataType()){
            case "tinyint":
            case "smallint":
            case "mediumint":
            case "int":
            case "bigint":
            case "bit":
            case "enum";
                 return "integer";
                 break;
            case "float":
            case "double":
            case "decimal":
                 return "real";
                 break;
            case "varchar": 
            case "text":
            case "char":
            case "tinytext":
            case "mediumtext":
            case "longtext":
            case "longblob":
                 return "text";
                 break;
            case "binary":
            case "varbinary":
            case "tinyblob": 
            case "blob":
            case "mediumblob":
                 return "binary";
                 break;
            case "date":
            case "time":
            case "year":
            case "datetime":
            case "timestamp":
                 return "time";
                 break;
            case "point":
            case "linestring":
            case "polygon":
            case "geometry":
            case "multipont":
            case "multilinestring":
            case "multipolygon":
            case "geometrycollection":
                 return "geometry";
                 break;
            case "set":
                 return "other";
                 break;
            default :
                 return "void";
                 break;
        }
            
    }   
    
    /**
    * 
    * 
    * @param
    * 
    * @return $this
    */      
    public function getDbName(){
        
        return $this->db_name;
        
       
    }    
    
    public function getTable(){
        
        return $this->table;
        
    }
    

    public function getRow(){
    
        $this->db->setIdMaster($this->getIdMaster());

        if($this->db->setRow()){
            
            $this->setColumnName($this->db->getRow('column_name'));
            $this->setDataType($this->db->getRow('data_type'));
            $this->setNumericPrecision($this->db->getRow('numeric_precision'));
            $this->setColumnKey($this->db->getRow('column_key'));
            $this->setCollationName($this->db->getRow('collation_name'));

            return 1;


        }
        else{

            return 0;
        }
        
    }    
    
   /**
    * Esse método obtem um nome de alias que será utilizado pela classe para acessar uma tabela
    * 
    * @access public
    * @method getAlias
    * @param
    * 
    * @return $this
    */      
    public function getAlias(){
        
        return $this->alias;
        
    }      
    
    public function printClass($value = null){
        
        $countCol = 0;    
        $buffer = '';    

        $this->db->setIdMaster($this->getIdMaster());
        
        while ($this->getRow()){
            
            $countCol++;
            
            $table = $this->getTable();
            $column_key = $this->getColumnKey();
            $column_name = $this->getColumnName();
            $numeric_precision = $this->getNumericPrecision();
            $collation_name = $this->getCollationName();
            $type_data_general = $this->getTypeDataGeneral();
            $field_name_to_class_method = $this->changeFieldNameToClassMethod($column_name);

            switch($value){
                case 'key': 
                    if($column_key=='PRI'){
                        $buffer.='
        $this->setFieldKey("'.$field_name_to_class_method.'");';
                    } 
                    break;
                case 'array': 
                    switch ($type_data_general) {
                        case 'integer':
                        case 'real':
                            $buffer.="
                            '{$column_name}' => ['value' => 0,";
                            break;
                        default:
                            $buffer.="
                            '{$column_name}' => ['value' => '',";
                            break;
                    }
                    $buffer.="'type' => '{$type_data_general}']" . ($countCol < $this->db->getNumRows() ? "," : "");
                    break;   
                case 'select': 
                    $buffer.='`'.$column_name.'`' . ($countCol < $this->db->getNumRows() ? "," : "");
                    break;
                case 'chkFields': 
                    $buffer.='
        $ok = $ok || (!empty($this->get'.$field_name_to_class_method.'()) || $this->allowSetEmpty());';
                    break;
                case 'set': 
                    
                    switch ($type_data_general) {
                        case 'integer':
                        case 'real':
                        case 'enum':    
                            $buffer.='
    public function set'.$field_name_to_class_method.'($value = null) {';
                            break;
                        case 'time':
                            $buffer.='
    public function set'.$field_name_to_class_method.'($value = null,$format="Y-m-d") {

        if($value=="now") {
        
            $date = clone $this->date;
            
            $value = $date->now()->val();
        
        }    
        
        if($format!=="Y-m-d"){

            $date = clone $this->date;
            
            $value = $date->set($value,$format)->get("Y-m-d")->val();
        
        }
                                    ';
                            break;
                        default:
                            $buffer.='
    public function set'.$field_name_to_class_method.'($value = null) {
        $value = addslashes($value);
';
                    } 
                            $buffer.='
    
        if(isset($value) && ($this->allowSetEmpty()) || !empty($value)){
        
            $this->ptr_array[$this->stack]["'.$column_name.'"]["value"] = $value;
            $this->ptr_array[$this->stack]["'.$column_name.'"]["type"] = $this->default["'.$column_name.'"]["type"];

        }    
        else{
    
            if(!empty($this->save[$this->stack]["'.$column_name.'"]["value"])){

                $this->ptr_array[$this->stack]["'.$column_name.'"]["value"] = $this->save[$this->stack]["'.$column_name.'"]["value"];
                $this->ptr_array[$this->stack]["'.$column_name.'"]["type"] = $this->default["'.$column_name.'"]["type"];
            
            }
            
        }
    
        $this->last_field_set = "'.$column_name.'";
        $this->last_field_set_value = $value;

        return $this;

} ';       

                        break;
                case 'get': 
                    
                    switch ($type_data_general) {
                        case 'integer':
                        case 'real':
                        case 'enum':
                            $buffer.='
    public function get'.$field_name_to_class_method.'($value = null) {

        if(empty($value)){
            
            return $this->save[$this->stack]["'.$column_name.'"]["value"];
            
        }
        else {
            
            $array_format = explode("|",$value);
            
            $moeda = $array_format[0];
            $separador_unidades = $array_format[1];
            $separador_fracao = $array_format[2];
            $casas_decimais = $array_format[3];
            
            return $moeda.number_format($this->save[$this->stack]["'.$column_name.'"]["value"], $casas_decimais, $separador_fracao, $separador_unidades);
            
        } 
       
    } ';  
                            break;
                        case 'time':
                            $buffer.='
    public function get'.$field_name_to_class_method.'($value = null) {
    
        $date = clone $this->date;
    
        return $date->set($this->save[$this->stack]["'.$column_name.'"]["value"])->get($value);   
        
    }';    
    
                            break;
                        default:
                        $buffer.='
    public function get'.$field_name_to_class_method.'() {

        //return $this->save[$this->stack]["'.$column_name.'"]["value"];
        
        //mb_detect_encoding($string, "UTF-8, ISO-8859-1, ISO-8859-15", true)
        
        //return mb_convert_encoding($this->save[$this->stack]["'.$column_name.'"]["value"], $this->config->title("framework")->key("charset")->val(), "'.$this->getCollationNamexCharset().'");
        
        $charset = mb_detect_encoding($this->save[$this->stack]["'.$column_name.'"]["value"],$this->config->title("framework")->key("charset_supported")->val(), true);
        
        if(strtoupper($charset) !== strtoupper($this->config->title("framework")->key("charset")->val())) {
            
            return mb_convert_encoding(stripslashes($this->save[$this->stack]["'.$column_name.'"]["value"]), $this->config->title("framework")->key("charset")->val());    
           
        }    
        else {
            
            return stripslashes($this->save[$this->stack]["'.$column_name.'"]["value"]);
            
        }  
        
    }';    

                    } 
                     break;
                case 'getRow':                         
                    $buffer.='
                    $this->set'.$field_name_to_class_method.'($this->db->getRow("'.$column_name.'"));';
                     break;
                case 'function': 
                     break;
                
            }        
            
            
        }
        
        return $buffer;
        
    }    

    public function get(){

        try {

            if(!file_exists($file = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'inc' . DIRECTORY_SEPARATOR . 'template.inc'))
                $this->error->set("Não foi possível incluir o template de classe {$file}",E_FRAMEWORK_ERROR);          
        
       }    
        catch (\Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }         


        return $buffer = include($file);

        // exit("<pre>{$buffer}</pre>");

    }
   
}


   
