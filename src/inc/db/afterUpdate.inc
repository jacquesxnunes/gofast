<?php
$break = "\n";
$cols = 50;
$mails = array(
                'f71lagos.com' => 'michele@institutolagosrio.com.br',
                'f71.com.br' => 'sabinojunior@f71.fom.br',
                'f71reger.com' => ' ludmylla.bastos@institutoreger.org.br',
                'f71ibraceds.com' => 'leandro@ibraceds.org.br',
                'f71faespe.com' => 'alexandremorelli@fundacaoantares.org.br',
                'f71centeduc.com' => 'renatapmorbin@gmail.com',
                'f71cegecon.com' => 'thaine@cegecon.org.br',
                'f71itgm.com' => 'alexbaldace@gmail.com',
                'f71osunir.com' => 'jvecarvalho@yahoo.com.br'
               );
$replace = array(
                'www.',
                'des.',
                'tes.'
                );

$domain = str_replace($replace, '', DOMAIN);

if((strpos(explode('SET',$query)[0],'curso')!==false || strpos(explode('SET',$query)[0],'rh_clt')!==false) && strpos($query,'salario=')!==false){
    
    if(strpos(explode('SET',$query)[0],'curso')!==false){
        $key = 'id_curso';
        $table = 'curso';
    }
    else {
        $key = 'id_clt';
        $table = 'rh_clt';               
    }
    
    foreach (explode('AND',explode('WHERE',$query)[1]) as $k => $v) {

        if(strpos($v,$key)!==false){
            $arr1 = explode('=',$v);
            $arr2[trim($arr1[0])] = trim($arr1[1]);
        }

    }    
    
    $arr3 = $this->setDefault()->setQuery(QUERY,"SELECT IFNULL(c.salario,0) salario FROM {$table} c WHERE c.{$key}={$arr2[$key]}")->execute()->getArray();
    
    $this->tag['salario']['novo'] = number_format(isset($arr3[0]['salario']) ? (double)$arr3[0]['salario'] : 0,2,',','.');
    
    if($this->tag['salario']['atual'] !== $this->tag['salario']['novo']){
        

        $srv = $this->error->getServerSnapShotArray();

        $this->mail['to'] = $mails[$domain];
        $this->mail['from'] = 'postmaster@f71.com.br';
        $this->mail['subtitle'] = 'ATENÇÃO - UPDATE CRÍTICO';
        $this->mail['header']  = "MIME-Version: 1.1\n"
                               . "Content-type: text/plain; charset=UTF-8\n"
                               . "From: {$this->mail['to']}\n"
                               . "Return-Path: {$this->mail['to']}\n";  

        $this->error->set("{$this->mail['to']} - Alteração {$this->tag['nome']} de Salário de {$this->tag['salario']['atual']} para {$this->tag['salario']['novo']}",E_FRAMEWORK_LOG);
        
        $this->mail['message'] = str_repeat("-=", $cols).$break
                                .'Data/Hora..: '.date('d-m-Y H:i:s ').$break
                                .'Domínio....: '.vsprintf("%-20s", substr($srv['host'],0,20)).$break
                                .'Sessão.....: '.$srv['session'].$break
                                .'Usuário....: '.vsprintf("%04d", $srv['logado']).' - '.vsprintf("%-15s",substr($srv['login'],0,15)).' '.$break
                                .'IPv4 LAN...: '.$srv['ipv4'].$break
                                .'IPv6 LAN...: '.$srv['ipv6'].$break
                                .'PHP/Versão.: '.$srv['php_version'].$break
                                .'User Agent.: '.$srv['agent'].$break
                                .str_repeat("-=", $cols).$break
                                .'Nome.......: '.$this->tag['nome'].$break
                                .'Sal. Atual.: R$'.$this->tag['salario']['atual'].$break
                                .'Sal. Novo..: R$'.$this->tag['salario']['novo'].$break                                
                                .str_repeat("-=", $cols).$break
                                .'URL........: '.'http://'.$srv['host'].$srv['uri'].$srv['query'].$break
                                .$break;

        if(mail("{$this->mail['to']}", "{$this->mail['subtitle']}", "{$this->mail['message']}", "{$this->mail['header']}", "-f{$this->mail['to']}"));
        
    }    
    
    
}

