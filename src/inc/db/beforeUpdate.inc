<?php
$break = "\n";
$cols = 50;
$this->tag['nome'] = '';
$this->tag['salario']['atual'] = '0,00';
$this->tag['salario']['novo'] = '0,00';
        
if((strpos(explode('SET',$query)[0],'curso')!==false || strpos(explode('SET',$query)[0],'rh_clt')!==false) && strpos($query,'salario=')!==false){

    if(strpos(explode('SET',$query)[0],'curso')!==false){
        $key = 'id_curso';
        $table = 'curso';
    }
    else {
        $key = 'id_clt';
        $table = 'rh_clt';               
    }
    
    foreach (explode('AND',explode('WHERE',$query)[1]) as $k => $v) {

        if(strpos($v,$key)!==false){
            $arr1 = explode('=',$v);
            $arr2[trim($arr1[0])] = trim($arr1[1]);
        }

    }    
    
    $arr3 = $this->setDefault()->setQuery(QUERY,"SELECT c.nome, IFNULL(c.salario,0) salario FROM {$table} c WHERE c.{$key}={$arr2[$key]}")->execute()->getArray();

    $this->tag['nome'] = $arr3[0]['nome'];    
    $this->tag['salario']['atual'] = number_format((double)$arr3[0]['salario'],2,',','.');   
    
}

