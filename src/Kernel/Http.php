<?php

namespace GoFast\Kernel;

use GoFast\Kernel\Core;

class Http extends Core {
        
    use \GoFast\Lib\Bridge;  
    
    /**
     * Define valor para retorno no uso de métodos encadeados
     * 
     * @param $value
     * 
     * @return $this
     */
    protected function __construct($value = null) {

        
    }    
    
    /**
     * Define valor para retorno no uso de métodos encadeados
     * 
     * @param $value
     * 
     * @return $this
     */
    protected function setValue($value = null) {

        $this->value = $value;

        return $this;
    } 

}