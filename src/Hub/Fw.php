<?php
/*  
 * F71 Go Fast
 * 
 * 04/03/2016
 * 
 * Módulo Main de agrupamento do conjunto de classes de RhClt orientado ao FrameWork do sistema da F71 
 * 
 * Arquivos que Fazem parte do framework FwClass (Classes Mãe)
 * 
 *  -> MySqlClass.php
 *  -> ErrorClass.php
 *  -> DateClass.php
 *  -> WebClass.php
 *  -> EncryptClass.php
 *  -> LibClass.php
 *  -> ConstructClass.php
 *  -> FileClass.php
 * 
 * @tutorial
 * 
 * Padronização de acesso as classes do Framework. De um modo geral esses s�o os procedimentos padr�es de acesso as classes e manipulação do framework
 *
 * fw->setDefault()                         Define valores Padr�es para inicar todas as classes do framework
 *  
 * fw->obj->setDefault()                    Define valores Padr�es para iniciar opera��es na classe
 * fw->obj->set[nome do método]()           Define valores em elementos da classe, nunca uma operação de calculos ou procedimentos. Usar apenas para obter valores prim�rios.
 * fw->obj->setCalc[nome do método]()       Calcula e define valores em elementos da classe
 * fw->obj->setField[nome do campo]()       Define a inclus�o de um campo extra em um método select da classe
 * 
 * fw->obj->select()                        Seleciona um conjunto de registros de uma classe que ser� consultada com getRow() 
 * fw->obj->selectExt()                     Seleciona um conjunto de registros de acordo com as condi��es definidas nesse método extendido da classe
 * fw->obj->select[nome do método]()        Seleciona um conjunto de registros de um método de forma agrupada em conjunto de dados ou array de dados
 * 
 * fw->obj->getRow()                        Carrega os valores de registros de uma classe que foi selecionada com select() ou select[nome do método]
 * fw->obj->getRowExt()                     Carrega os valores de campos extendidos criados para as propriedades da classe
 * fw->obj->get[nome do método]()           Obtem o valor de um elemento da classe ou array, nunca uma operação de calculos ou procedimentos. Usar apenas para obter valores prim�rios.
 * fw->obj->getCalc[nome do método]()       Calcula e retorna um valor de resultado ou array
 * 
 * fw->obj->onUpdate()                      Gerador de evento na classe e todos registros relacionas (insert, update, delete)
 * 
 * fw->obj->chk[nome do método]()           Verifica alguma coisa e retorna verdadeiro ou falso
 * 
 * fw->obj->isOk()                          Verifica o status de execução do último método executado na classe
 *  
 * $this->db->
 * 
 * $this->error->
 * 
 * 1. Ao sefinir um elementro chave de uma consulta (ex: rh->Clt->setIdClt(5009)) todas as classes que possuem chave estrangeira relacionada a ele
 *    ir�o levar essa chave em consideração ao serem executados seus métodos (ex: rh->Ferias->setCalcInssFgtsIrrf()).
 * 2. Evite o uso de vari�veis dentro das classes, procurando sempre usar a propriedade da classe para evitar inconsist�ncia de informação e centralização
 *    dos valores.
 * 3. O deploy devera sempre propagar as atualizações de classes para todos os clientes
 * 
 * @version: 3.0.0000L - 04/03/2016 - Jacques - Vers�o Inicial
 * @version: 3.0.0000L - 17/03/2016 - Jacques - Adicionado buffer de sessão global para includes de classes dinâmicas 
 *                                           e est�ticas do framework a fim de dar celeridade a execu��o do c�digo
 * @version: 3.0.0000L - 22/03/2016 - Jacques - Adicionado método __toString e isOk para o controle da classe do framework
 * @version: 3.0.8864L - 08/04/2016 - Jacques - Adicionado rotinas de serialização das classes e controle de versionamento do framework
 * @version: 3.0.9006L - 20/04/2016 - Jacques - Implementado controle de erro mais detalhado desde concentrador de classes do framework
 * @version: 3.0.0248F - 20/10/2016 - Jacques - Adicionado informação da classe parent no evento de erro dos métodos mágicos
 * @version: 3.0.0251F - 21/10/2016 - Jacques - Implementado a opção de instanciamento de classe por alias opcional
 * 
 * @author: Jacques
 * 
 * @see:
 * 
 * Exemplo de composer para implementação de uso do framework pelo composer
 * 
 * {
 *   "name": "setasign/fpdf",
 *   "version": "1.8.1",
 *   "homepage": "http://www.fpdf.org",
 *   "description": "FPDF is a PHP class which allows to generate PDF files with pure PHP. F from FPDF stands for Free: you may use it for any kind of usage and modify it to suit your needs.",
 *   "type": "library",
 *   "keywords": ["pdf", "fpdf"],
 *   "license": "no usage restriction",
 *   "authors": [
 *       {
 *           "name": "Olivier Plathey",
 *           "email": "oliver@fpdf.org",
 *           "homepage": "http://fpdf.org/"
 *       }
 *   ],
 *   "autoload": {
 *       "classmap": [
 *           "fpdf.php"
 *       ]
 *   }
 *}
 * 
 * @todo 
 * 
 * Adicionar verficações no GoFast para ver se o ambiente está em condições mínimas de execução
 * 
 * Código PHP
 * max_execution_time = 1200
 * max_input_time = 1200
 * max_input_vars = 3000
 * memory_limit   = 2048M
 * 
 * Servidor de MySql
 * event_scheduler = ON
 * group_concat_max_len = 4294967295 - (32 bit) 18446744073709551615 - (64 bit) (Valor Máximo)
 * 
 */

namespace GoFast\Hub;

global $id_master;

use GoFast\Kernel\Core;

class Fw extends Core
{

    const DEBUG = 100;              // Informação para debug detalhada
    const INFO = 200;               // Eventos de interesse em logs
    const NOTICE = 250;             // Eventos não comuns
    const WARNING = 300;            // Ocorrências excepcionais que não são erros
    const ERROR = 400;              // Erros em tempo de execução
    const CRITICAL = 500;           // Condições críticas
    const ALERT = 550;              // A ação deve ser realizadas imediatamente
    const EMERGENCY = 600;          // Alerta de urgência
    const API = 1;                  // Registra a versão de API

    const CHARSET_DEFAULT = 'utf-8';
    # const DIR_TMP = ROOT_DIR . DIRECTORY_SEPARATOR . 'var' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR;
    const DIR_TMP = SYS_GET_TEMP_DIR;

    public static $instance;

    public      $construct;
    public      $user;
    public      $version;           // Objeto que controla a versão do framework
    public      $git;               // Objeto que controla o versionamento do frame
    public      $error;             // Objeto que carrega as sessão de erros em todas as instâncias do Framework
    public      $date;              // Objeto que carrega as sessão de date em todas as instâncias do Framework
    public      $db;                // Objeto que carrega as sessão de conexões com banco de dados em todas as inst�ncias do Framework
    public      $file;              // Objeto que carrega as sessão de manipulação de arquivos no Framework
    public      $cmbBox;            // Objeto que carrega os valores retornado pela classe em um select formatado em html <select>
    public      $log;               // Objeto que carrega a classe de error
    public      $lib;               // Objeto que carrega as libs de manipulação padr�o do Framework
    public      $config;            // Objeto que carrega as configurações do Framework
    public      $autoload;          // Objeto que carrega os arquivos de classes
    public      $curl;              // Objeto que carrega a classe de consulta
    public      $memcache;          // Objeto que carrega as configurações do memcache
    //    public      $accessControl;     // Objeto que carrega as configurações do memcache

    protected $environment;
    protected $base_path;           // Variável que registra a base da pasta raiz de execução do GoFast    
    protected $alias_map;
    protected $ws;
    protected $wp_id = 'default';

    /**
     * Definição de constante de log definido no RFC 5424
     *
     * @var array $levels Níveis de log
     */
    protected static $levels = array(
        self::DEBUG     => 'DEBUG',
        self::INFO      => 'INFO',
        self::NOTICE    => 'NOTICE',
        self::WARNING   => 'WARNING',
        self::ERROR     => 'ERROR',
        self::CRITICAL  => 'CRITICAL',
        self::ALERT     => 'ALERT',
        self::EMERGENCY => 'EMERGENCY',
    );

    //    protected   $master;          // Vetor que registrar os domínios que irão trabalhar com o framework
    //    protected   $table;           // Vetor que define as tabelas que irão ser instânciadas dinâmicamente

    private     $action = '';       // Variável que ativa a todas as classes  

    private     $build = 'tag_ver'; // Variável que controla o versionamento do framework
    private     $stack;             // Variável que controla o acesso as classes por pilha
    private     $time;              // Vetor para controle do tempo de execução de processos

    use \GoFast\Lib\Bridge;

    /**
     * Método construtor da classe do framework. Nele se define o dom�nio master para o modelo de neg�cio do framework.
     * 
     * @access public
     * @method __construct
     * @param
     * 
     * @return $this
     */
    public function __construct($value = null)
    {

        try {

            $this->setCharset(\GoFast\Hub\Fw::CHARSET_DEFAULT);

            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    switch ($k) {
                        case 'charset':
                            $this->setCharset($v);
                            break;
                        case 'base_path':
                            $this->setBasePath($v);
                            break;
                        default:
                            break;
                    }
                }
            }

            $value['class'] = static::class;


            if (!isset($value['id_master'])) {
                if (!$this->getIdMaster()) {
                    $config = (new \GoFast\Lib\Config($value))->getInstance();
                    if (!is_object($config)) die("# Não foi possível instânciar a classe {$value['class']}->createCoreClass()->config");
                    
                    $value['id_master']  = $config->id(\GoFast\Lib\Config::ID_FW)->setFile(\GoFast\Lib\Config::PATH_FW)->title('master_domain')->key(DOMAIN)->val();

                } else {
                    $value['id_master']  = $this->getIdMaster();
                }
            }

            parent::__construct($value);

            $this->createCoreClass($value);

            $this->ws('default');

            // self::$instance = $this;

            $this->sendRequestGlobalVariables();

            if (!$this->setEnvironment()->isOk()) $this->error->set("Não foi possível executar a configuração de ambiente de execução do GoFast", E_FRAMEWORK_ERROR);

            if (!$this->chkEnvironment()->isOk()) $this->error->set("O PHP não está configurado com o ambiente de execução mínimo", E_FRAMEWORK_ERROR);

            if (!$this->chkLoadedExtensions()->isOk()) $this->error->set("O GoFast não está configurado com as extensões necessárias para sua execução", E_FRAMEWORK_ERROR);

            if (!$this->chkVersionPHP()->isOk())  $this->error->set("O GoFast não está configurado com a versão adequada do PHP", E_FRAMEWORK_ERROR);

            //if(!array_key_exists($this->getDomain(),$this->master)) $this->error->set("Domínio [{$this->getDomain()}] não definido para execução no framework",E_FRAMEWORK_ERROR);

            // $version = $this->load(array('id' => $this->git->getCommitOrigHead() . '-version', 'state' => 0));

            //            if(is_null($version)){
            //                $this->setBuild($this->version->get());
            //                $this->save(array('id' => $this->git->getCommitOrigHead().'-version','data' => $this->version->get()));
            //            }
            //            else {
            //                $this->setBuild($version);
            //            }
            //            
            //            //\lib\webClass::setBuild($this->getBuild());


        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);

            die(str_replace("\n", '<br>', $this->getAllMsgCode()));
        }
    }
    /**
     * É disparado ao invocar métodos inacessíveis em um contexto de objeto.
     * 
     * @access public
     * @method __call
     * @param
     * 
     * @return $this
     */
    public function __call($name = null, $arguments = null)
    {

        try {

            $argument = implode(",", $arguments);

            $class = self::class;

            if (!is_object($this->$name) && !method_exists($this, $name))
                $this->error->set(array(10, "{$class}->{$name}({$argument}))"), E_FRAMEWORK_ERROR);

            if (is_object($this->$name) && !is_object($this->stack[$name][$arguments[0]]))
                $this->stack[$name][$arguments[0]] = new $this->$name;

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0);

            $this->error->set(array(1, $class), E_FRAMEWORK_WARNING, $ex);
        }

        return is_object($this->stack[$name][$arguments[0]]) ?  $this->stack[$name][$arguments[0]] : $this;
    }

    /**
     * É disparado quando invocando métodos inacessíveis em um contexto estático.
     * 
     * @access public
     * @method __callStatic
     * @param
     * 
     * @return $this
     */
    public static function __callStatic($name = null, $arguments = null)
    {

        if (strpos($name, 'upper') !== false) {

            return strtoupper($arguments[0]);
        }
    }

    /**
     * É executado ao escrever dados em propriedades inacessíveis.
     * 
     * @access public
     * @method __set
     * @param
     * 
     * @return $this
     */
    public function __set($name = null, $value = null)
    {

        try {

            $this->$name = $value;

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0);

            $this->error->set(array(1, array(1, $name)), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }

    /**
     * É utilizado para ler dados de propriedades inacessíveis.
     * 
     * @access public
     * @method __get
     * @param
     * 
     * @return $this
     */
    public function __get($value = null)
    {

        try {

            if (!is_object($this->$value) && isset($this->alias_map[$value])) {

                $this->$value = $this->addClassChildren($value);
            }

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0);

            $this->error->set(array(1, self::class), E_FRAMEWORK_WARNING, $ex);
        }




        //        $trace = debug_backtrace();
        //
        //        trigger_error(
        //            'Undefined property via __get(): ' . $name .
        //            ' in ' . $trace[0]['file'] .
        //            ' on line ' . $trace[0]['line'],
        //            E_USER_NOTICE);

        return $this->$value;
    }

    /**
     * Método utilizado para definir um cronômetro
     * 
     * @param
     * 
     * @return $this
     */
    public function ws($value = null)
    {

        try {

            $this->ws_id = $value;

            if ($value == 'default' || empty($value)) {
                $id_master = $this->config->id(\GoFast\Lib\Config::ID_FW)->setFile(\GoFast\Lib\Config::PATH_FW)->title('master_domain')->key(DOMAIN)->val();
            } else {
                $id_master = $this->config->id(\GoFast\Lib\Config::ID_FW)->setFile(\GoFast\Lib\Config::PATH_FW)->title('master_workspace')->key($value)->val();
            }

            $name_master = $this->config->id(\GoFast\Lib\Config::ID_FW)->title('master_name')->key($id_master)->val();

            $file = ROOT_ETC . strtolower("{$name_master}.ini");

            $this->error->set("# id_master = {$id_master}, name_master = {$name_master}, file = {$file}", E_FRAMEWORK_LOG);

            $this
                ->setIdMaster($id_master)
                ->setNameMaster($name_master);

            $this->config->id($name_master)->reload($file);

            $this->loadTables();
            $this->loadAlias();

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->error->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }



    /**
     * Método utilizado para definir um cronômetro
     * 
     * @access public
     * @method setStopSatch
     * @param
     * 
     * @return $this
     */
    public function setStopSatch()
    {

        try {

            $this->setValue(0);

            $this->time['start'] = microtime(true);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }

    /**
     * Método utilizado para verificar as configurações básicas para execução do framework
     * 
     * @access public
     * @method __get
     * @param
     * 
     * @return $this
     */
    public function chkEnvironment()
    {

        try {

            $this->setValue(0);

            $ok = 1;

            $haystack = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);

            $config["max_execution_time"] = ini_get("max_execution_time");
            $config["max_input_time"] = ini_get("max_input_time");
            $config["max_input_vars"] = ini_get("max_input_vars");
            $config["memory_limit"] = explode('M', ini_get("memory_limit"))[0];

            //SELECT @@group_concat_max_len

            if ($config["max_execution_time"] > -1 && $config["max_execution_time"] < 1200 && in_array($this->getMaster(), $haystack)) {

                $ok = $ok && 0;

                $this->error->set("# O MAX_EXECUTION_TIME está definido com {$config['max_execution_time']}, mas deve ser igual ou maior 1200", E_FRAMEWORK_WARNING);
            }
            if ($config["max_input_time"] > -1 && $config["max_input_time"] < 1200 && in_array($this->getMaster(), $haystack)) {

                $ok = $ok && 0;

                $this->error->set("# O MAX_INPUT_TIME está definido com {$config['max_input_time']}, mas deve ser igual ou maior que 1200", E_FRAMEWORK_WARNING);
            }
            if ($config["max_input_vars"] > -1 && $config["max_input_vars"] < 3000 && in_array($this->getMaster(), $haystack)) {

                $ok = $ok && 0;

                $this->error->set("# O MAX_INPUT_VARS está definido com {$config['max_input_vars']}, mas deve ser igual ou maior que 3000", E_FRAMEWORK_WARNING);
            }
            if ($config["memory_limit"] > -1 && $config["memory_limit"] < 2048 && in_array($this->getMaster(), $haystack)) {

                $ok = $ok && 0;

                $this->error->set("# O MEMORY_LIMIT está definido com {$config['memory_limit']}, mas deve ser igual ou maior que 2048M", E_FRAMEWORK_WARNING);
            }

            if (!$ok) $this->error->set("# Configuração mínima do PHP não definida para execução do GoFast", E_FRAMEWORK_WARNING);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }

    /**
     * Método utilizado para verificar a versão do PHP
     * 
     * @access private
     * @method chkVersionPHP
     * @param
     * 
     * @return $this
     */
    private function chkVersionPHP()
    {

        try {

            /**
             * Verifica a versão mínima do PHP para execuação
             */
            if (version_compare(phpversion(), '5.6.0', '<'))  $this->error->set("O GoFast é retrocompatível até versão do PHp 5.6", E_FRAMEWORK_ERROR);

            /**
             * Verifica a versão máxima do PHP para execuação
             */
            if (version_compare(phpversion(), '7.4.16', '>'))  $this->error->set("O GoFast é compatível e testado até versão do PHp 7.4.11", E_FRAMEWORK_ERROR);


            $this->setValue(1);
        } catch (Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }

    /**
     * Método utilizado para verificar as extenções que devem ser carregadas para execução do framework
     * 
     * @access private
     * @method chkLoadedExtensions
     * @param
     * 
     * @return $this
     */
    private function chkLoadedExtensions()
    {

        try {

            $this->setValue(0);

            $ok = 1;

            $haystack['master'] = array(0, 4);

            $haystack['extensions'] = array('opcache' => 'Zend OPcache');

            foreach ($haystack['extensions'] as $key => $value) {

                if (!extension_loaded($value) && in_array($this->getMaster(), $haystack)) {

                    $ok = $ok && 0;

                    $this->error->set("# A extensão {$value} não foi carregada no apache", E_FRAMEWORK_WARNING);
                }
            }

            $this->setValue(1);
        } catch (Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }

    /**
     * Método utilizado para definir condições de execução do GoFast
     * 
     * @access public
     * @method setEnvironment
     * @param
     * 
     * @return $this
     */
    public function setEnvironment()
    {

        try {

            ini_set('default_charset', $this->getCharset());

            $this->setValue(1);
        } catch (Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }


    /**
     * 
     * @name setDefault
     * 
     * @internal - Seta valores default em todos os objetos inst�nciados no framework
     */
    public function setDefault()
    {

        try {

            foreach ($this as $key => $value) {

                if (is_object($value)) {

                    if (method_exists($value, 'setDefault')) {
                        $this->$key->setDefault();
                    }
                }
            }
        } catch (\Exception $ex) {

            $this->error->set(_("Uma excessão na MACRO setDefault do framework impediu a execução do método"), E_FRAMEWORK_ERROR, $ex);
        }

        return $this;
    }

    public function loadTables()
    {

        $this->alias_map = [];

        $db_name = $this->config->id($this->getNameMaster())->title('conn')->key('db_name')->val();

        $this->db->setQuery(SELECT, "table_name");
        $this->db->setQuery(FROM, "information_schema.tables");
        $this->db->setQuery(WHERE, "table_schema = '{$db_name}'");

        if (!$this->db->setRs()->isOk()) $this->error->set("# Houve um erro na query de consulta do método select da classe loadTables", E_FRAMEWORK_ERROR);

        foreach ($this->db->getArray() as $key => $value) {
            $this->alias_map[$this->createAliasNameFrom($value['table_name'])] = $value['table_name'];
        }

        return $this;
    }

    public function loadAlias()
    {

        $this->alias_map = array_merge($this->alias_map, $this->config->id(\goFast\Lib\Config::ID_FW)->title('table_alias')->arrayGroupValue());

        return $this;
    }


    /**
     * Define a pasta raiz da aplicação
     *
     * @param  string  $value
     * 
     * @return $this
     */
    public function setBasePath($value = null)
    {

        $this->basePath = rtrim($value, '\/');

        return $this;
    }

    /**
     * @name setCalcBuild
     * 
     * @internal - Obtem o número de revisão atual do framework
     * 
     * http://consello.com.br/svn/lagos/?p=2000
     * 
     * svn info http://consello.com.br/svn/lagos/ --xml
     * 
     */
    public function chkBuild()
    {

        //        $url = $this->config->title('deploy')->key('url')->val();

        //        $ch = curl_init();
        //
        //        curl_setopt($ch, CURLOPT_HEADER, 0);
        //        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        //        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        //        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        //        curl_setopt($ch, CURLOPT_POST, 1);
        //        curl_setopt($ch, CURLOPT_URL, $url);
        //
        //        $response = curl_exec($ch);
        //
        //        $error_code = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //
        //        $error_msg = curl_error($ch);
        //
        //        $this->setBuild($response);

        return $this;
    }

    /*
     * PHP-DOC 
     * 
     * @name setBuild
     * 
     * @internal - Serializa todas as classes do framework em uma sessão de usu�rio
     * 
     */
    public function setBuild($value = null)
    {

        $this->build = $value;

        return $this;
    }





    /*
     * PHP-DOC 
     * 
     * @name select
     * 
     * @internal - Seleciona todos os registros de classe do framework de acordo com a ordem de carregamento das classes
     * 
     */
    function select()
    {

        foreach ($this as $key => $value) {

            if (is_object($value)) {

                if (method_exists($value, 'select')) {

                    $this->$key->select();
                }
            }
        }

        return $this;
    }

    /**
     * PHP-DOC 
     * 
     * @name getBuild
     * 
     * @internal - Obtem o n�mero do versionamento do framework
     * 
     */
    public function getBuild()
    {

        return $this->build;
    }

    /**
     * PHP-DOC 
     * 
     * @name getUri
     * 
     * @internal - Obtem a URI do framework
     * 
     */
    public function getUri($value = null)
    {

        return PATH_CLASS;
    }

    /*
     * PHP-DOC 
     * 
     * @name getRow
     * 
     * @internal - Carrega o primeiro registro do RS inst�nciado nas classes do framework
     * 
     */
    function getRow()
    {

        foreach ($this as $key => $value) {

            if (is_object($value)) {

                if (method_exists($value, 'getRow')) {

                    $this->$key->getRow();
                }
            }
        }

        return $this;
    }



    /*
     * PHP-DOC 
     * 
     * @name setAction
     * 
     * @internal - 
     * 
     */
    public function setAction($value = null)
    {

        $this->action = $value;

        return $this;
    }

    /*
     * PHP-DOC 
     * 
     * @name setError
     * 
     * @internal - 
     * 
     */
    public function setError($value = null)
    {

        $this->error = $value;

        return $this;
    }

    /*
     * PHP-DOC 
     * 
     * @name setDb
     * 
     * @internal - 
     * 
     */
    public function setDb($value = null)
    {

        $this->db = $value;

        return $this;
    }

    /**
     * Método para obter o domínio atual
     * 
     * @access public
     * @method exeAll
     * @param
     * 
     * @return 
     */
    public function getDomain()
    {

        return DOMAIN;
    }

    /**
     * Método para obter todos os domínios
     * 
     * @access public
     * @method getAllDomains
     * @param
     * 
     * @return 
     */
    public function getAllDomains()
    {

        global $_fw;

        return $_fw->title('master_domain')->arrayGroupValue();
    }

    public function getAction()
    {

        return $this->action;
    }

    /**
     * Médodo para obter a correspondência de código para o master em execução
     * 
     * @access protected
     * @method getKeyMaster
     * @param
     * 
     * @return $this
     */
    public function getKeyMaster($value)
    {

        try {

            $file = ROOT_ETC . 'keymaster.ini';

            if (!file_exists($file)) $file = ROOT_DIR . 'keymaster.ini';

            if (!file_exists($file)) $this->error->set(_("# Não existe o arquivo {$file} de configuração da classe"), E_FRAMEWORK_ERROR);

            $keymaster = parse_ini_file($file, true);

            if (!$keymaster) $this->error->set(_("# Não foi possível carregar o arquivo {$file} de configuração da classe"), E_FRAMEWORK_ERROR);

            $key = $keymaster[$value][$this->getMaster()];

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);

            $this->setValue(0);
        }

        return $key;
    }


    /**
     * Método construtor de classe dinâmica. Esse método é responsável por carregar o código de construção das classes filhas do hub
     * 
     * @access public
     * @method constructClass
     * @param
     * 
     * @return $this
     */
    public function constructClassCurl($value = null)
    {

        try {

            if (is_array($value)) {

                foreach ($value as $k => $v) {

                    switch ($k) {
                        case 'table':
                            $table = $v;
                            break;
                        case 'alias':
                            $alias = $v;
                            break;
                        default:
                            break;
                    }
                }
            } else {

                $table = $value;
            }

            $dominio = $this->getDomain();

            $port_define = $this->config->title('framework')->key('port')->val() ? $this->config->title('framework')->key('port')->val() : PORT;

            $port = empty($port_define) ? '' : ":{$port_define}";

            $url = "{$dominio}{$port}?class=construct&table={$table}&alias={$alias}";

            // Define uma nova URL para ser chamada (após o login)

            $curlopt_postfields = array(
                "status" => 1
            );

            $starttime = microtime(true);


            $response = gzinflate($this->curl->init(array('url' => $url, 'curlopt_postfields' => $curlopt_postfields))->exec()->response());

            $endtime = microtime(true);

            $s = $endtime - $starttime;

            $h = floor($s / 3600);
            $s -= $h * 3600;
            $m = floor($s / 60);
            $s -= $m * 60;
            $lapse = $h . ':' . sprintf('%02d', $m) . ':' . sprintf('%02d', $s);

            if ($s > 1) $this->error->set("# Tempo de execução da construção da classe {$alias} muito alto - {$lapse}", E_FRAMEWORK_LOG, $ex);

            if (empty($response) && $this->config->title('framework')->key('debug')->val()) $this->error->set(_("# O Curl não é um recurso disponível"), E_FRAMEWORK_ERROR);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $response;
    }

    /**
     * método construtor de classe dinâmica. Esse método � respons�vel por carregar a classe m�e de todas as classes do framework.
     * 
     */
    public function constructClassExec($value = null)
    {

        try {

            $file = dirname(dirname(dirname(__FILE__)))  . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Construct.php ';
            $command = 'php ' . $file . ($value['table'] ? ' -t ' . $value['table'] : '') . ($value['alias'] ? ' -a ' . $value['alias'] : '') . ($value['domain'] ? ' -d ' . $value['domain'] : '');
            exec($command, $buffer, $result_code);

            if (empty($output) && $this->config->title('framework')->key('debug')->val()) $this->error->set(_("# O Exec não é um recurso disponível"), E_FRAMEWORK_ERROR);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $output;
    }


    /**
     * método construtor de classe dinâmica. Esse método � respons�vel por carregar a classe m�e de todas as classes do framework.
     * 
     */
    public function constructClassShellExec($value = null)
    {

        try {

            $file = dirname(dirname(dirname(__FILE__)))  . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Construct.php ';
            $command = 'php ' . $file . ($value['table'] ? ' table=' . $value['table'] : '') . ($value['alias'] ? ' alias=' . $value['alias'] : '') . ($value['domain'] ? ' domain=' . $value['domain'] : '');
            $output = shell_exec($command);
            if (empty($output) && $this->config->title('framework')->key('debug')->val()) $this->error->set(_("# O Exec não é um recurso disponível"), E_FRAMEWORK_ERROR);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $output;
    }

    /**
     * método construtor de classe dinâmica. Esse método � respons�vel por carregar a classe m�e de todas as classes do framework.
     * 
     */
    public function constructClassProcOpen($value = null)
    {

        try {

            $path = '';
            $file['error'] = sys_get_temp_dir() . DS . "error.log";
            $file['exec'] = dirname(dirname(dirname(__FILE__)))  . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Lib' . DIRECTORY_SEPARATOR . 'Construct.php ';
            $command = 'php ' . $file['exec'] . ($value['table'] ? ' table=' . $value['table'] : '') . ($value['alias'] ? ' alias=' . $value['alias'] : '') . ($value['domain'] ? ' domain=' . $value['domain'] : '');

            $descriptorspec = [
                0 => array("pipe", "r"), // stdin
                1 => array("pipe", "w"), // stdout -> we use this
                2 => array("file", "a")  // stderr is a file to write to 
            ];

            $process = proc_open(
                $command,
                $descriptorspec,
                $pipes,
                $path
            );

            if (!is_resource($process) && $this->config->title('framework')->key('debug')->val()) $this->error->set(_("# O ProcOpen não é um recurso disponível"), E_FRAMEWORK_ERROR);

            $output['return'] = trim(stream_get_contents($pipes[2]));

            fclose($pipes[0]);
            fclose($pipes[1]);
            fclose($pipes[2]);

            $$output['code'] = proc_close($process);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $$output['return'];
    }

    /**
     * método construtor de classe dinâmica. Esse método � respons�vel por carregar a classe m�e de todas as classes do framework.
     * 
     */
    public function constructClass($value = null)
    {

        try {


            $output['key'] = 0;
            $output['return'] = '';
            $output['mode'] = ['proc_open', 'exec', 'shell_exec', 'curl'];



            foreach ($output['mode'] as $v) {

                switch ($v) {
                    case 'exec':
                        $output['return'] = $this->constructClassExec($value);
                        break;
                    case 'shell_exec':
                        $output['return'] = $this->constructClassShellExec($value);
                        break;
                    case 'curl':
                        $output['return'] = $this->constructClassCurl($value);
                        break;
                    case 'proc_open':
                        $output['return'] = $this->constructClassProcOpen($value);
                        break;
                        break;
                }

                if (!empty($output['return'])) break;
            }

            if (empty($output['return']))
                $this->error->set(_("# Nenhum módulo construtor teve éxito"), E_FRAMEWORK_ERROR);


            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $output['return'];
    }


    /**
     * Método responsável por comunicação com o framework via api
     * 
     * @param
     * 
     * @return $this
     */
    public function api($value = null)
    {

        try {

            foreach ($value as $k => $v) {

                switch ($k) {
                    case 'url':
                        $url = $v;
                        break;
                    case 'class':
                        $class = $v;
                        break;
                    case 'method':
                        $method = $v;
                        break;
                    case 'table_schema':
                        $table_schema = $v;
                        break;
                    case 'table_name':
                        $table_name = $v;
                        break;
                    case 'column_name':
                        $column_name = $v;
                        break;
                    default:
                        break;
                }
            }

            // Define uma nova URL para ser chamada (após o login)

            $curlopt_postfields = array(
                "class" => $class,
                "method" => $method,
                "table_schema" => $table_schema,
                "table_name" => $table_name,
                "column_name" => $column_name
            );

            $response = $this->curl->init(array('url' => $url, 'curlopt_postfields' => $curlopt_postfields))->exec()->response();
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $response;
    }

    /**
     * Método utilizado para obter a contagem de tempo do cronômetro
     * 
     * @access public
     * @method getStopSatch
     * @param
     * 
     * @return $this
     */
    public function getStopSatch()
    {

        $this->time['end'] = microtime(true);

        $s = $this->time['end'] - $this->time['start'];

        $h = floor($s / 3600);
        $s -= $h * 3600;
        $m = floor($s / 60);
        $s -= $m * 60;

        $lapse = $h . ':' . sprintf('%02d', $m) . ':' . sprintf('%02d', $s);

        return $lapse;
    }



    /*
     * PHP-DOC 
     * 
     * @name getError
     * 
     * @internal - Verifica se houve algum erro em alguma classe do Framework
     * 
     */
    function getError()
    {

        $msg = '';

        foreach ($this as $key => $value) {

            if (is_object($value)) {

                if (method_exists($value->error, 'getAll') && !empty($this->$key->error->getAll())) {

                    $msg .= $this->$key->error->getAll();
                }
            }
        }

        return $msg;
    }


    /**
     * Retorna uma string com todas mensagens de todas as classes de um determinado código
     * 
     * @access public
     * @method chkInCode
     * @param
     * 
     * @return $this
     */
    public function chkInCode($code = null)
    {

        foreach ($this as $key => $value) {

            if (method_exists($this->$key->error, 'chkInCode')) {

                if ($this->$key->error->chkInCode($code)) return 1;
            }
        }

        return 0;
    }

    /**
     * Retorna uma string com todas mensagens de todas as classes de um determinado código
     * 
     * @access public
     * @method getAllMsgCode
     * @param
     * 
     * @return $this
     */
    public function getAllMsgCode($code = 0, $break = "\n")
    {

        $str = $this->error->getAllMsgCode($code, $break);

        return $str;
    }


    /**
     * Adiciona classes filhas a super classe rh que podem ser extendidas ou não
     * 
     * @access public
     * @method addClassChildren
     * @param
     * 
     * @return $this
     */
    private function addClassChildren($class = null)
    {

        try {

            global $_fw;

            $new_class = $this->getClassName($class) . "Ex";

            $file['key'] = sha1(session_id() . $class . $this->git->getCommitOrigHead());
            $file['tmp'] = self::DIR_TMP  . $file['key'];
            $file['no_cache'] = ROOT_DIR . 'no_cache';

            // Criar um hash dos arquivo extendidos do model (class) e quando for instânciar a classe dinâmica verificar se o hash modou.
            // caso tenha mudado então é porque ouve atualização no modelo de négocio e é necessário fazer atualização do arquivo
            // hash_file('sha384', 'composer-setup.php') === '795f976fe0ebd8b75f26a6dd68f78fd3453ce79f32ecb33e7fd087d39bfeb978342fb73ac986cd4f54edd0dc902601dc'            

            if (!file_exists($file['tmp']) || file_exists($file['no_cache']) || $this->config->title('framework')->key('no_cache')->val()) {


                $code['din'] = $this->construct->setIdMaster($this->getIdMaster())->setDbName($this->config->id($this->getNameMaster())->title('conn')->key('db_name')->val())->setTable($this->config->id(\goFast\Lib\Config::ID_FW)->title('table_alias')->key($class)->val())->setAlias($class)->chkAndSetTable()->get();


                if (empty($code['din'])) {
                    $this->error->set(_("# Nenhum resultado retornado para a construção da classe {$class}"), E_FRAMEWORK_LOG);
                    $code['din'] = $this->load(array('id' => 'lastSucessDinClass-' . $class, 'state' => 0, 'session' => DOMAIN));
                    if (is_null($code['din'])) $this->error->set(_("# Não foi possível recuperar a última classe {$class} dinâmica carregada com sucesso"), E_FRAMEWORK_ERROR);
                    $this->save(array('id' => 'lastSucessDinClass-' . $class, 'session' => DOMAIN, 'data' => $code['din']));
                }


                $file['class_ext'] = ROOT_CLASS . $this->getFileName($class);

                /**
                 * Caso exista arquivo de classe extendida então a classe finâmica possui extensão de regra de negócio.
                 */
                if (is_readable($file['class_ext'])) {


                    $code['ext'] = file($file['class_ext']);
                    $total_linhas = count($code['ext']);
                    $linha_offset = array_find("{", $code['ext']);

                    /** 
                     * Exclui as linhas que precedem a declaração da classe e a própria declaração
                     */
                    for ($i = 0; $i <= $linha_offset; $i++) {
                        unset($code['ext'][$i]);
                    }

                    /**
                     * Prepara o arquivo para extender a classe
                     */
                    $code['ext'] = array_values($code['ext']);
                    $code['ext'] = array_reverse($code['ext']);
                    $linha_offset = array_find('}', $code['ext']);


                    /**
                     * Exclui as linhas que suscedem a chave de fechamento e as linhas seguintes da classe
                     */
                    for ($i = 0; $i <= $linha_offset; $i++) {
                        unset($code['ext'][$i]);
                    }

                    $code['ext'] = array_reverse($code['ext']);

                    /**
                     * Adiciona conteúdo do arquivo para criação da classe
                     */
                    $code['ext'] = implode("", $code['ext']);
                } else {

                    if (file_exists($file['class_ext'])) {
                        $code['ext'] = $this->load(array('id' => 'lastSucessDinClass-' . $class, 'state' => 0, 'session' => DOMAIN));
                        if (is_null($code['ext'])) $this->error->set(_("# Não foi possível recuperar a última classe extendida {$class} carregada com sucesso"), E_FRAMEWORK_ERROR);
                    }
                }

                $namespace = "<?php\n\nnamespace GoFast\Hub;\n\nuse \GoFast\Kernel\Core;\n\nclass";
                $arg = "['root_dir' => '" . addslashes(ROOT_DIR) . "', 'class' => '" . self::class . "', 'domain' => '" . $_SERVER['HTTP_HOST'] . "', 'fw.ini' => '" . addslashes(ROOT_DIR) . "etc" . DS . "fw.ini" . "', 'charset' => '" . $this->getCharset() . "' ]";

                if (empty($code['din']) && empty($code['ext'])) $this->error->set(_("# Não foi possível obter uma declaração de código para a classe {$class}"), E_FRAMEWORK_ERROR);

                // if(!empty($code['din']) && empty($code['ext'])) $code['merge']  = "<?php\n\nnamespace hub;\n\nclass {$this->getClassName($class)} { {$code['din']} }\n\nclass {$new_class} extends {$this->getClassName($class)} {\n\n  }";
                // if(empty($code['din']) && !empty($code['ext'])) $code['merge']  = "<?php\n\nnamespace hub;\n\nclass {$this->getClassName($class)} { {$code['ext']} }\n\nclass {$new_class} extends {$this->getClassName($class)} {\n\n  }";
                // if(!empty($code['din']) && !empty($code['ext'])) $code['merge'] = "<?php\n\nnamespace hub;\n\nclass {$this->getClassName($class)} { {$code['din']} }\n\nclass {$new_class} extends {$this->getClassName($class)} {\n\n {$code['ext']} }";

                if (!empty($code['din']) && empty($code['ext'])) $code['merge']  = "{$namespace} {$this->getClassName($class)} extends Core { {$code['din']} } \n\n class {$new_class} extends {$this->getClassName($class)} {  } \n\n return (new \\GoFast\\Hub\\{$new_class}({$arg}))->getInstance();";
                if (empty($code['din']) && !empty($code['ext'])) $code['merge']  = "{$namespace} {$this->getClassName($class)} extends Core { {$code['ext']} } \n\n class {$new_class} extends {$this->getClassName($class)} {  } \n\n return (new \\GoFast\\Hub\\{$new_class}({$arg}))->getInstance();";
                if (!empty($code['din']) && !empty($code['ext'])) $code['merge'] = "{$namespace} {$this->getClassName($class)} extends Core { {$code['din']} } \n\n class {$new_class} extends {$this->getClassName($class)} { {$code['ext']} } \n\n  return (new \\GoFast\\Hub\\{$new_class}({$arg}))->getInstance();";

                if (is_object($this->memcache))
                    $this->memcache->set($file['key'], $code['merge'], 0, $this->config->title('framework')->key('query_expiration_times')->val() ? $this->config->title('framework')->key('query_expiration_times')->val() : 0);
            }

            $Obj = $this->includeClass(['no_cache_file' => $file['no_cache'], 'file' => $file['tmp'], 'code' => $code['merge']]);


            //$Obj = new $new_class(array('charset' => $this->getCharset()));

            if (!is_object($Obj)) {

                $this->error->set(_("# Não foi possível construir a classe {$class}, será utilizada a última executada com éxito"), E_FRAMEWORK_WARNING);

                $code['merge'] = $this->load(array('id' => 'lastSucess-' . $class, 'state' => 0, 'session' => DOMAIN));

                $Obj = $this->includeClass(['no_cache_file' => $file['no_cache'], 'file' => $file['tmp'], 'code' => $code['merge']]);
            }


            if (!is_object($Obj)) $this->error->set(_("# Não foi possível recuperar a última classe {$class} executada com sucesso"), E_FRAMEWORK_ERROR);

            $this->save(array('id' => 'lastSucess-' . $class, 'session' => DOMAIN, 'data' => $code['merge']));

            if (!method_exists($Obj, 'setSuperClass')) $this->error->set(_("# Método {$class}->setSuperclass não definido para a classe"), E_FRAMEWORK_ERROR);

            $Obj->setSuperClass($this->getInstance())->setCharset($this->getCharset())->setModelColumns();

            if (!is_object($Obj)) $this->error->set(_("# Não foi possível agregar a classe {$class} ao concentrador do framework"), E_FRAMEWORK_ERROR);

            $Obj->setDefault();

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->error->setValue(0)->set(array(1, __METHOD__), E_FRAMEWORK_ERROR)->dump($code['cache']);
        }

        return $Obj;
    }

    /**
     * Método que printa na tela do usuário informações de debug conforme parâmetros
     * 
     * @access public
     * @method debug
     * @param
     * 
     * @return $this
     */
    public function debug($value = null)
    {

        $where = 1;

        foreach ($value as $k => $v) {

            switch ($k) {
                case 'method':
                    $method = explode(',', $v);
                    break;
                case 'where':
                    $where = $v;
                    break;
                case 'title':
                    $title = $v;
                    break;
                case 'echo':
                    $echo = $v;
                    break;
                default:
                    break;
            }
        }

        $cookie = '$val=' . (!empty($_COOKIE["debug_where"]) ? $_COOKIE["debug_where"] : '0') . ';';

        eval($cookie);

        if (in_array($_COOKIE['debug_method'], $method) || $where == $val) {

            echo isset($title) ? "<h6>{$title}</h6>" : "<h6>{$_COOKIE['debug_method']}{$_COOKIE['debug_where']}</h6>";

            echo "<textarea class='debug'>";

            if (is_array($echo)) {

                print_r($echo);
            } else {

                echo $echo;
            }

            echo '</textarea>';
        }

        return $this;
    }

    /**
     * 
     * @name close
     * 
     * @internal - Seta valores default em todos os objetos inst�nciados no framework
     */
    function close()
    {

        try {

            foreach ($this as $key => $value) {

                if (is_object($value->db)) {

                    if (method_exists($value->db, 'close')) {

                        $this->$key->db->close();
                    } else {

                        $this->error->set(_("Método setDefault não declarado na classe {$key}"), E_FRAMEWORK_ERROR);
                    }
                }
            }
        } catch (\Exception $ex) {

            $this->error->set(_("Uma excessão na MACRO setDefault do framework impediu a execução do método"), E_FRAMEWORK_ERROR, $ex);
        }

        return $this;
    }



    /**
     * Adiciona classes filhas a super classe rh que podem ser extendidas ou não
     * 
     * @access public
     * @method addClassChildren
     * @param
     * 
     * @return $this
     */
    protected function getFileName($value = null)
    {

        return str_replace('<table_alias>', $value, $this->config->id(\GoFast\Lib\Config::ID_FW)->title('mask')->key('file_name')->val());
    }

    protected function getClassName($value = null)
    {

        return str_replace('<table_alias>', $value, $this->config->id(\GoFast\Lib\Config::ID_FW)->title('mask')->key('class_name')->val());
    }

    /**
     * Método para criar a classe via include
     * @param
     * 
     * @return boolean
     */
    protected function includeClass($value)
    {

        foreach ($value as $k => $v) {

            switch ($k) {
                case 'no_cache_file':
                    $no_cache_file = $v;
                    break;
                case 'file':
                    $file = $v;
                    break;
                case 'code':
                    $code = $v;
                    break;
                default:
                    break;
            }
        }

        if (file_exists($no_cache_file) || !file_exists($file))
            if (!file_put_contents($file, $code)) $this->error->set('Nâo foi possível salvar o arquivo temporário da classe em ' . $file, E_FRAMEWORK_ERROR);


        return include($file);
    }


    /**
     * Executa um pharse no código
     * 
     * @access public
     * @method chkPhpSyntaxError
     * @param  string
     * 
     * @return string
     */
    protected function chkPhpSyntaxError($code = null)
    {

        if (!defined("CR"))
            define("CR", "\r");
        if (!defined("LF"))
            define("LF", "\n");
        if (!defined("CRLF"))
            define("CRLF", "\r\n");
        $braces = 0;
        $inString = 0;
        foreach (token_get_all('<?php ' . $code) as $token) {
            if (is_array($token)) {
                switch ($token[0]) {
                    case T_CURLY_OPEN:
                    case T_DOLLAR_OPEN_CURLY_BRACES:
                    case T_START_HEREDOD:
                        ++$inString;
                        break;
                    case T_END_HEREDOD:
                        --$inString;
                        break;
                }
            } else if ($inString & 1) {
                switch ($token) {
                    case '`':
                    case '\'':
                    case '"':
                        --$inString;
                        break;
                }
            } else {
                switch ($token) {
                    case '`':
                    case '\'':
                    case '"':
                        ++$inString;
                        break;
                    case '{':
                        ++$braces;
                        break;
                    case '}':
                        if ($inString) {
                            --$inString;
                        } else {
                            --$braces;
                            if ($braces < 0) break 2;
                        }
                        break;
                }
            }
        }
        $inString = @ini_set('log_errors', false);
        $token = @ini_set('display_errors', true);
        ob_start();
        $code = substr($code, strlen('<?php '));
        $braces || $code = "if(0){{$code}\n}";
        if (eval($code) === false) {
            if ($braces) {
                $braces = PHP_INT_MAX;
            } else {
                false !== strpos($code, CR) && $code = strtr(str_replace(CRLF, LF, $code), CR, LF);
                $braces = substr_count($code, LF);
            }
            $code = ob_get_clean();
            $code = strip_tags($code);
            if (preg_match("'syntax error, (.+) in .+ on line (\d+)$'s", $code, $code)) {
                $code[2] = (int) $code[2];
                $code = $code[2] <= $braces
                    ? array($code[1], $code[2])
                    : array('unexpected $end' . substr($code[1], 14), $braces);
            } else $code = array('syntax error', 0);
        } else {
            ob_end_clean();
            $code = false;
        }
        @ini_set('display_errors', $token);
        @ini_set('log_errors', $inString);

        return $code;
    }

    /**
     * Obtem o charset utilizado pela classe
     * 
     * @access public
     * @method getCharset
     * @param  
     * 
     * @return string
     */
    public function goFast($value = null)
    {

        try {

            global $class, $method;

            $value['id_master'] = $this->getIdMaster();
            $value['name_master'] = $this->getNameMaster();

            $this->environment = $value;

            $this->user->config($value);

            $class = str_replace('/', DIRECTORY_SEPARATOR, $class);
            $split = explode(DIRECTORY_SEPARATOR, $class)[0];
            $math = ['login', 'api'];

            if (!in_array($split, $math) && !$this->user->checkSession()->isOk()) {
                $this->redirect(['url' => $value['redirect']['login']]);
            } else {
                if (!empty($class) && file_exists($file = ROOT_APP_CONTROLLER . $class . '.class.php')) {
                    if (!$this->controller = include($file)) $this->error->set(sprintf(_("# Não foi possível incluir a classe %s"), $file), E_FRAMEWORK_ERROR);
                }
            }
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }
}
