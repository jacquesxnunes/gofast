<?php

/**
 * Classe para controle e interface web
 *
 * @file                webClass.php
 * @license		F71
 * @link
 * @copyright           2016 F71
 * @author		Jacques <jacques@f71.com.br>
 * @package             webClass
 * @access              public
 *
 * @version: 3.0.0000 - 16/10/2015 - Jacques - Vers�o Inicial
 * @version: 3.0.4506 - 24/11/2015 - Jacques - Adicionado ao CSS default os valores de html, body, nav e footer. Tamb�m alterada a refer�ncia indireta de links para direta a partir da raiz
 * @version: 3.0.5047 - 24/11/2015 - Jacques - Adicionado a tag tag_rev na inclus�o de arquivo javascript e css para obrigar a atualiza��o do arquivo pelo cache
 * @version: 3.0.5166 - 24/11/2015 - Jacques - Adicionado set e get da propriedade user para a classe interna da webClass
 * @version: 3.0.5297 - 04/01/2016 - Jacques - Adicionado método get para obter o dom�nio corrente do site.
 * @version: 3.0.5508 - 12/01/2016 - Jacques - Alterado no método getAlertHtml um if que verificava por empty para strlen por motivo de falso verdadeiro
 * @version: 3.0.7364 - 04/03/2016 - Jacques - Adiciona a substitui��o de string \n por <br> no método getAlertHtml
 * @version: 3.0.8131 - 04/03/2016 - Jacques - Adiciona método para obter o nome do dom�nio corrente
 * @version: 3.0.8506 - 04/03/2016 - Jacques - Adicionado a verifica��o de par�metro do logado al�m do cookie para submiss�es curl
 * @version: 3.0.8710 - 29/03/2016 - Jacques - Adicionado o método createCoreClass com as respectivas propriedades de classes
 * @version: 3.0.8710 - 20/04/2016 - Jacques - Adicionado controle de erro na classe
 * @version: 3.0.9307 - 04/05/2016 - Jacques - Adi��o de templates e constantes globais
 * @version: 3.0.9307 - 04/05/2016 - Jacques - Adi��o da chamada do action internamente
 * @version: 3.0.0000 - 11/12/2016 - Jacques - Extract transforma o vetor de requisi��o em um conjunto de vari�veis carregadas
 * @version: 3.0.0000 - 11/12/2016 - Jacques - Adicionado o método setMeta e opção de exeução de setMetaExt para inclusão de informações extendidas
 * @version: 3.0.0000 - 16/01/2017 - Jacques - Adicionado a variável cmbBox para uso pela classe
 *
 * @todo
 *
 * Funcionalidade
 *
 * 1. A classe webClass funciona basicamente com uma chamada a a��o como ex: $webFerias->action(); na classe web[NomeDaTela]Class extendida
 * 2. Dever� tamb�m ser setado para a classe extendida dentro do método action métodos de funcionalidade ex:
 *
 *      $this->setMethod('showPage');
 *
 *      onde $this->setMethod('NomeDoMetodo') ? o método que deseja executar, e exeMethod('telaForm') ? a chamada para a��o do método com par�metros ou n�o
 *
 * 3. Poder� tamb�m ser adicionado métodos extendidos pr�-definido para adi��o de funcionalidade e defini��o de propriedades em:
 *
 *      setTitle()
 *      setPageTitle()
 *      setCssExt()
 *      setJavaScriptExt()
 *      setBreadCrumb()
 *
 *      métodos extendidos sem pr�-definidos para serem usados em setMethod e exeMethod ex:
 *
 *      telaForm()
 *      telaModalCalculaFerias()
 *      chkFaltasAbonoPecuniario()
 *
 * 4. Os métodos podem ser desde exibi��o de conte?do para p�ginas como execu��o de chamada ajax.
 *
 */
namespace GoFast\Controller;

use GoFast\Kernel\Core;

/**
 * Classe controladora de core
 */
class Web extends Core {
    
    use \GoFast\Lib\Bridge;

    public static $instance;       

    private $user;
    private $build = 'tag_ver';
    private $method = '';
    private $page_title = '';
    private $is_updating = 0;
    private $method_default = 'index';
    private $method_in_frame = 0;
    private $css_header;
    private $js_footer;
    private $inner_body_html;
    private $template;                                                          // Template deverá ser uma array com o nome das tags de estrutura e duas subchaves, sendo um para caminho e outra para dados
    private $benchmarking = 0;
    private $time_start;
    private $time_stop;
    private $buffer;
    
    protected $html;

    public  $version;
    public  $error;
    public  $date;
    public  $db;
    public  $file;
    public  $cmbBox;
    public  $log;
    public  $lib;
    public  $config;
    public  $autoload;
    public  $curl;    
    public  $language;
    public  $accessControl; 
    
    /**
     * Método executado na construção da classe
     *
     * @access public
     * @method __construct
     * @param
     *
     * @return
     */
    public function __construct($value = null) {

        try {


            if(is_array($value)) {

                foreach ($value as $k => $v) {

                    switch ($k) {
                        case 'disabled':
                            $disabled = $v;
                            break;
                        case 'method':
                            $method = $v;
                            break;
                        case 'method_in_frame':
                            $method_in_frame = $v;
                            break;
                        case 'template':
                            $this->template = $v;
                            break;                        
                        default:
                            global $$k;
                            $$k = $v;
                            break;
                    }

                }

            }


            $value['class'] = self::class;

            $this->checkVersion();

            if(isset($value['container']['instance'])){
                $this->fw = $value['container']['instance'];
            }
            else {
                $this->fw = (new \GoFast\Hub\Fw())->getInstance();
            }

            parent::__construct($value);  
            
            $this->createCoreClass($value);       

            if(isset($method_in_frame)) $this->setMethodInFrame($method_in_frame);

            if(isset($method)) $this->setMethod($method);

            $this->action($disabled);

        }
        catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);

        }


        return $this;

    }

    /**
     * Método executado quando o objeto e referênciado diretamente para retornar uma string
     *
     * @access public
     * @method __toString
     * @param
     *
     * @return string
     */
    public function __toString() {

        return (string)$this->buffer;

    }

    /**
     * Mètodo disparado ao invocar métodos inacessíveis em um contexto de objeto.
     *
     * @access public
     * @method __call
     * @param ($name,$arguments)
     *
     * @return $this
     */
    public function __call($name, $arguments){

        try {

            $class = __CLASS__;

            if(!method_exists($this,$name)) $this->error->set(array(10,"{$class}->{$name}"),E_FRAMEWORK_ERROR);

            $this->setValue(1);

        }
        catch (\Exception $ex) {

            $this->setValue(0);

            $this->error->set(array(1,$class),E_FRAMEWORK_WARNING,$ex);

        }

        return $this;

    }

    /**
     * Método disparado quando invocando métodos inacessíveis em um contexto estático.
     *
     * @access public
     * @method __callStatic
     * @param ($name,$arguments)
     *
     * @return $this
     */
    public static function __callStatic($name, $arguments){

        try {

            $class = __CLASS__;

            if(!method_exists($this,$name)) $this->error->set(array(10,"{$class}->{$name}"),E_FRAMEWORK_ERROR);

            $this->setValue(1);

        }
        catch (\Exception $ex) {

            $this->setValue(0);

            $this->error->set(array(1,array(1,$class)),E_FRAMEWORK_WARNING,$ex);


        }

        return $this;

    }

    /**
     * Método executado ao escrever dados em propriedades inacessíveis.
     *
     * @access public
     * @method __set
     * @param ($name,$value)
     *
     * @return $this
     */
    public function __set($name, $value) {

        try {

            $class = __CLASS__;

            if(!isset($this->table[$name])) $this->error->set(array(11,"{$class}->{$name}"),E_FRAMEWORK_ERROR);

            $this->$name = $value;

            $this->setValue(1);

        }
        catch (\Exception $ex) {

            $this->setValue(0);

            $this->error->set(array(1,array(1,$name)),E_FRAMEWORK_WARNING,$ex);


        }

        return $this;

    }

    /**
     * Método utilizado para ler dados de propriedades inacessíveis.
     *
     * @access public
     * @method __get
     * @param ($name)
     *
     * @return $this
     */
    public function __get($name)
    {

        try {

            $class = __CLASS__;

            if(!isset($this->$name)) $this->error->set(array(10,"{$class}->{$name}"),E_FRAMEWORK_ERROR);

            $this->setValue(1);

        }
        catch (\Exception $ex) {

            $this->setValue(0);

            $this->error->set(array(1,__CLASS__),E_FRAMEWORK_WARNING,$ex);


        }


//        $trace = debug_backtrace();
//
//        trigger_error(
//            'Undefined property via __get(): ' . $name .
//            ' in ' . $trace[0]['file'] .
//            ' on line ' . $trace[0]['line'],
//            E_USER_NOTICE);

        return $name;

    }

     /**
     * Verifica versão do Kernel crítica que obriga a atualização da aplicação
     *
     * @access public
     * @method isOk
     * @param
     *
     * @return int
     */
    public function checkVersion() {

        if(GOFAST_VERSION!==GOFAST_KERNEL_VERSION) {
            $charset = $this->config->title('framework')->key('charset')->val();
            header("Content-type: text/html; charset={$charset}");  
            echo _('Versão do GoFast desatualizada! Atualize via composer update para poder continuar');
            exit();
        } 

    }   
    

    /**
     * Método que define uma página default a ser exibida na classe
     *
     * @access public
     * @method setDefaultPage
     * @param
     *
     * @return $this
     */
    public function setDefaultPage($value){

        $this->setMethodDefault($value);

        return $this;

    }

    /**
     * Método que define o nome de complemento de exibição de página
     *
     * @access public
     * @method setShowPage
     * @param
     *
     * @return $this
     */
    public function setShowPage($value){

        $this->setMethodDefault($value);

        return $this;

    }

    /**
     * Método que define o nome de complemento de exibição de página
     *
     * @access public
     * @method setMethodDefault
     * @param
     *
     * @return $this
     */
    public function setMethodDefault($value){

        $this->method_default = $value;

        return $this;

    }
    
    /**
     * Método para exibir um array chave x valor
     *
     * @access private
     * @method showArray
     * @param  array
     *
     * @return $this
     */
    protected function showArray($value){
        
        foreach ($value as $k => $v) {
            
            echo "{$k} = {$v}<br>";
            
        }
        
        return $this;

    }
    
    /**
     * Método que define o procedimento a ser executado pela classe
     *
     * @access protected
     * @method setMethod
     * @param
     *
     * @return $this
     */
    protected function setMethod($value){

        $this->method = $value;

        return $this;

    }

    /**
     * Método que define o procedimento a ser executado pela classe
     *
     * @access protected
     * @method setMethodInFrame
     * @param  boolean
     *
     * @return $this
     */
    protected function setMethodInFrame($value){

        $this->method_in_frame = $value;

        return $this;

    }


    /**
     * Método que define o procedimento a ser executado pela classe
     *
     * @access protected
     * @method setMethodExt
     * @param
     *
     * @return $this
     */
    protected function setMethodExt($value){

        $this->setMethod($value);

        return $this;

    }

    /**
     * Método que define a revisão da classe
     *
     * @access protected
     * @method setBuild
     * @param
     *
     * @return $this
     */
    protected function setBuild($value){

        $this->build = $value;

        return $this;

    }

    /**
     * Método que a conta de usuário ativa
     *
     * @access protected
     * @method setUser
     * @param
     *
     * @return $this
     */
    protected function setUser($value) {

        $this->user = $value;

        return $this;

    }

    /**
     * Método que define o título do arquivo HTML
     *
     * @access protected
     * @method setTitle
     * @param
     *
     * @return $this
     */
    protected function setTitle($value){

        $this->html['page']['header']['title'] = $value;

        return $this;

    }

    /**
     * Método que define o título da barra de navegação
     *
     * @access protected
     * @method setPageTitle
     * @param
     *
     * @return $this
     */
    protected function setPageTitle($value){

        $this->page_title = $value;

        return $this;

    }

    /**
     * Carrega o Css responsável pelo layout da página padrão e extendida
     *
     * @access private
     * @method setHeaderCss
     * @param
     *
     * @return $this
     */
    private function setHeaderCss($value){
        
       if(isset($value)) 
            $this->html['page']['header']['css'] = $this->view($value);

        return $this;


    }

    /**
     * Carrega os JavaScripts padrões e extendidos na parte superior do arquivo
     *
     * @access private
     * @method setJavaScriptHeader
     * @param
     *
     * @return $this
     */
    private function setHeaderJavaScript($value){
        
       if(isset($value)) 
            $this->html['page']['header']['javascript'] = $this->view($this->template['page']['header']['javascript']);
       
 
        return $this;


    }


    /**
     * Método para definir as informações de meta tag da página
     *
     * @access private
     * @method setHeader
     * @param
     *
     * @return $this
     */
    private function setHeaderMeta($value) {
        
       if(isset($value)) 
            $this->html['page']['header']['meta'] = $this->view($value);

        return $this;

    }

    
    /**
     * Método para definir as tags de template do cabeçário da página
     *
     * @access private
     * @method setHeader
     * @param
     *
     * @return $this
     */
    private function setHeader($value) {
        
       if(isset($value)) 
            $this->html['page']['header'] = $this->view($value);

        return $this;

    }
    
    /**
     * Método para definir o corpo do documento
     *
     * @access private
     * @method setBody
     * @param
     *
     * @return $this
     */
    private function setBody($value) {

       if(isset($value)) 
            $this->html['page']['body'] = $this->view($value);

        return $this;

    }

    /**
     * Método para definir o miolo do documento
     *
     * @access private
     * @method setInnerBody
     * @param
     *
     * @return $this
     */
    private function setBodyContent($value) {

       if(isset($value)) 
            $this->html['page']['body']['content'] = $this->view($value);

        return $this;

    }
    

    /**
     * Carrega os JavaScripts padrões e extendidos na parte inferior do arquivo
     *
     * @access private
     * @method setJavaScriptFooter
     * @param
     *
     * @return $this
     */
    private function setBodyJavaScript($value){
        
       if(isset($value))       
            $this->html['page']['body']['javascript'] = $this->view($value);

        return $this;


    }    

    /**
     * Método para defini o rodapé do documento
     *
     * @access private
     * @method setBodyFooter
     * @param
     *
     * @return $this
     */
    private function setBodyFooter($value){
        
       if(isset($value)) 
            $this->html['page']['body']['footer'] = $this->view($value);        

        return $this;


    }

    /**
     * Método utilizado para retornar o domínio
     *
     * @access protected
     * @method getdomain
     * @param
     *
     * @return $this
     */
    protected function getdomain(){

        return $_SERVER['HTTP_HOST'];

    }

    public function getShowPage(){

        return $this->method_default;

    }

    protected function getMethod(){

        return $this->method;

    }

    protected function getMethodExt(){

        return $this->getMethod();

    }

    /**
     * Método que obtem o nome do método padrão a ser executado
     *
     * @access public
     * @method getMethodDefault
     * @param
     *
     * @return $this
     */
    public function getMethodDefault(){

        return $this->method_default;

    }

    /**
     * Método que obtem a flag de condição de execução do método
     *
     * @access protected
     * @method getMethodInFrame
     * @param  boolean
     *
     * @return $this
     */
    protected function getMethodInFrame(){

        return $this->method_in_frame;

    }

    protected function getBuild(){

        return $this->build;

    }

    protected function getUser($value){

        return isset($value) ? $this->user[$value] : $this->user;

    }

    public function getTitle(){

        return $this->html['header']['title'];

    }

    protected function getPageTitle(){

        return $this->page_title;

    }

    private function getHeader() {

        return $this->html['page']['header']['tag'];

    }

    private function getHeaderMeta() {

        return $this->html['page']['header']['meta'];

    }

    private function getHeaderCss() {

        return $this->html['page']['header']['css'];

    }

    private function getHeaderJavaScript() {

        return $this->html['page']['header']['javascript'];

    }

    private function getBodyJavaScript() {

        return $this->html['page']['body']['javascript'];

    }

    private function getBody() {

        return $this->html['body']['value'];

    }

    private function getBodyContent() {

        return $this->html['page']['body']['content'];

    }

    private function getBodyFooter(){

        return $this->html['page']['body']['footer'];

    }

    protected function getDominio(){

        return $_SERVER['HTTP_HOST'];

    }
    
    /**
     * Obtem a url de origem 
     *
     * @access protected
     * @method getOrigin
     * @param
     *
     * @return string
     */    
    protected function getOrigin(){

        if (array_key_exists('HTTP_ORIGIN', $_SERVER)) {
            $origin = $_SERVER['HTTP_ORIGIN'];
        }
        else if (array_key_exists('HTTP_REFERER', $_SERVER)) {
            $origin = $_SERVER['HTTP_REFERER'];
        } else {
            $origin = $_SERVER['REMOTE_ADDR'];
        }
        
        return $origin;

    }    

    /**
     * Executa a inclusão específica de urls js externas
     *
     * @access protected
     * @method includeLibJSFooter
     * @param
     *
     * @return $this
     */
    protected function includeLibJSFooter($value){

        $this->js_footer .= "<script type=\"text/javascript\" src=\"{$value}?tag_rev\"></script>\n";

        return $this;

    }

    /**
     * Executa a inclusão específica de css
     *
     * @access protected
     * @method includeLibCssHeader
     * @param  string
     *
     * @return $this
     */
    protected function includeLibCssHeader($value){

        $this->css_header .= "<link href=\"{$value}?tag_rev\" rel=\"stylesheet\" type=\"text/css\">\n";

        return $this;

    }

    /**
     * Verifica se o sistema está em atualização e aguarda a liberação atravéz de redirecionamento com mensagem e armazenamento de todos parâmetros passados para processeguir ao liberar
     *
     * @access protected
     * @method isUpdate
     * @param
     *
     * @return
     */
    protected function isUpdate(){


    }

    protected function exeMethod(){

        try {


            /**
             * Inicia o buffer de saída
             */
            ob_start();

            $this->isUpdate();

            $dominio = $_SERVER['HTTP_HOST'];

            $value = $this->getMethod();

            $value_default = $this->getMethodDefault();

            if(!empty($value) && !method_exists($this,$value)) $this->error->set(array(10,__CLASS__."->{$value}"),E_FRAMEWORK_ERROR);
         

            if($this->getMethodInFrame() || empty($this->getMethod())){
                                           
                echo $this->showPage(empty($value) ? $value_default :  $value);

            }
            else {

                if(empty($value)) $value = $value_default;

                $this->$value();

            }

        }
        catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);

        }
    

        $buffer = ob_get_contents();

        ob_end_clean();

        $id_master = $this->config->id(\GoFast\Lib\Config::ID_FW)->title('master_domain')->key(DOMAIN)->val();
        $name_master = $this->config->id(\GoFast\Lib\Config::ID_FW)->title('master_name')->key($id_master)->val();        
        $charset = $this->config->id($name_master)->title('framework')->key('charset')->val();

        header("Content-type: text/html; charset={$charset}");  
        header("Server-Timing: {$value};desc=GoFast - Time Run Method {$this->getMethod()};dur={$this->Benchmarking()}");
        echo $buffer;

        return $this;

    }
    
    /**
     * Verifica se o sistema está em atualização e aguarda a liberação atravéz de redirecionamento com mensagem e armazenamento de todos parâmetros passados para processeguir ao liberar
     *
     * @access protected
     * @method isUpdate
     * @param
     *
     * @return
     */
    protected function Benchmarking($value = null){
        
        global $benchmarking;
        
        if(($this->benchmarking['enabled'] || $benchmarking) && !isset($this->time_start)){
            $this->time_start = (float)microtime(true); 
        }    
        elseif(($this->benchmarking['enabled'] || $benchmarking) && isset($this->time_start)) {
            $this->time_stop = (float)microtime(true);
            $time_elapsed = $this->time_stop - $this->time_start;

            $this->error->set(sprintf(_("# Benchmarking: Método=%s, Elapsed=%s"),$value,$time_elapsed),E_FRAMEWORK_DEBUG);

        }    
   
    }    

    protected function showPage($value = null){

        if(!empty($value)) $this->setMethod($value);
 
        
        $this->setHeaderMeta($this->template['page']['header']['meta']);         
        $this->setHeaderCss($this->template['page']['header']['css']);
        $this->setHeaderJavaScript($this->template['page']['header']['javascript']);
        $this->setBodyContent($this->template['page']['body']['content']);
        $this->setBodyFooter($this->template['page']['body']['footer']);        
        $this->setBodyJavaScript($this->template['page']['body']['javascript']);

        $this->setHeader(array_merge($this->template['page']['header'],$this->html['page']['header']));
        $this->setBody(array_merge($this->template['page']['body'],$this->html['page']['body']));       
        
        $this->html['page']['title'] = $this->getTitle();        
        $this->html['page']['lang'] = $this->config->title('framework')->key('lang')->val();
        $this->html['page']['charset'] = $this->config->title('framework')->key('charset')->val();

        return $this->view(array_merge($this->template['page'], $this->html['page']));
        
    }


    /**
     * Executa a ação assim que a classe é construída
     *
     * @access protected
     * @method action
     * @param
     *
     * @return
     */
    protected function action($disabled = 0){

        try {
            
            $cookie = $_COOKIE['echo_request'];

            //$this->setBuild($this->version->get());

            $this->sendRequestGlobalVariables();

            global $method;

            $this->setMethod($method);
            
            if($echo_request || $cookie) $this->showArray($_REQUEST);            
            
            if(method_exists($this,'actionExt')) $this->actionExt();
                    
            if(!$disabled) $this->exeMethod();
                      
            $this->setValue(1);

        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);

        }

        return $this;

    }

    /*
     * PHP-DOC - Verifica se existe mensagem passada por par?metro e caso exista retorna o mesmo formatado com mensagem do tipo pre-definido
     */
    protected function getAlertHtml($msg,$type='danger',$classExt=''){

        if(strlen($msg)){

            $msg = str_replace("\n", "<br>", $msg);

            return  "
                    <div class='alert alert-{$type} {$classExt}' role='alert'>
                      $msg
                    </div>
                    ";

        }
        else {

            return '';

        }

    }

    /**
     * Método estruturado para exibição de views
     *
     * @access protected
     * @method action
     * @param
     *
     * @return
     */
    public function view($value){

        
        ob_start();

        if(is_file($value['template'])) include $value['template'];
        
        /**
         * Caso o parâmetro do método seja getMethod, então carrega o método do método
         */
        if($value['method']=='getMethod'){
            $method = $this->getMethod();
            $this->$method($value);
        }
        else {
            if(method_exists($this,$value['method'])) {
                $method = $value['method'];
                $this->$method($value);         
            }
        }


        $this->buffer = ob_get_contents();

        ob_end_clean();   
        
        return $this->buffer;
        
    }    

} // Final da Class web
