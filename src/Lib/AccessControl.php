<?php 

namespace GoFast\Lib;

use GoFast\Kernel\Core;

class AccessControl extends Core
{

    public static $instance;          
    
    protected $id_user;
    
    public function __construct() {
        
        $this->setIdUser($_COOKIE['logado']);
        
    }
    
    public function setDefault() {
        return $this;
    }
    
    public function setIdUser($id_user) {
        $this->id_user = $id_user;
        
        return $this;
    }
    
    public function getIdUser() {
        return $this->id_user;
    }
    
    /**
     * Gera um Token no padrão Ymd+id_user que tem tempo de duração de um dia
     *
     * @access protected
     * @method setPrivateToken
     * @param  string
     *
     * @return $this
     */
    protected function setPrivateToken($value = null) {

        
        return $this;
        
    } 
    
    /**
     * Gera um Token no padrão Ymd+id_user que tem tempo de duração de um dia
     *
     * @access protected
     * @method setToken
     * @param  string
     *
     * @return $this
     */
    protected function setPublicToken($value = null) {

        if($value) setcookie("token", $token, (time()+3600)*24);
        
        return $this;
        
    }     
    
    /**
     * Gera um Token no padrão Ymd+id_user que tem tempo de duração de um dia
     *
     * @access protected
     * @method getToken
     * @param
     *
     * @return
     */
    protected function getToken() {

        return sha1($this->date->get('now')->val('Ymd').session_id());
        
    }    
    
    /**
     * Verifica por token e o valida
     *
     * @access protected
     * @method chkToken
     * @param
     *
     * @return $this
     */
    protected function chkToken() {

        try {
            
            global $token;
            
            if($token) $this->setToken($token);
                
            if(!$token && !$_COOKIE['token']) $this->error->set("Não foi passado nenhum token para esse acesso", E_FRAMEWORK_ERROR);
            
            if($_COOKIE['token']!==$this->getToken()) $this->error->set("Token inválido", E_FRAMEWORK_ERROR);
            
            $this->setValue(1);
            
        } catch (Exception $ex) {
            
            $this->setValue(0);
            
            $this->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);

        }
        
        return $this;
        
    }     
    
}


