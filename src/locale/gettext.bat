@echo off
echo Gerando lista de arquivos..
rem dir C:\projetos\f71framework\class\*.php /L /B /S >> %TEMP%\i18n.txt
rem dir C:\projetos\f71framework\app\*.php /L /B /S >> %TEMP%\i18n.txt
dir C:\projetos\f71framework\app\controller\ola-mundo\*.* /L /B /S > %TEMP%\i18n.txt
dir C:\projetos\f71framework\app\controller\rh\rescisao\clt\*.* /L /B /S >> %TEMP%\i18n.txt
dir C:\projetos\f71framework\app\template\rh\rescisao\clt\*.inc /L /B /S >> %TEMP%\i18n.txt
dir C:\projetos\f71framework\class\*.php /L /B /S >> %TEMP%\i18n.txt
dir C:\projetos\f71framework\template\web_class\*.php /L /B /S >> %TEMP%\i18n.txt

echo Criando arquivo .POT...
xgettext -k_e -k__ --from-code utf-8  -o C:\projetos\f71framework\locale\es_ES\LC_MESSAGES\messages.pot -L PHP --no-wrap -D \projetos\f71framework -f %TEMP%\i18n.txt
echo Done.
del %TEMP%\i18n.txt