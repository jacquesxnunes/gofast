<?php

/**
 * Classe para manipulação do arquivo de log
 * 
 * @file                LogClass.php
 * @license		F71
 * @link		
 * @copyright           2016 F71
 * @author		Jacques <jacques@f71.com.br>
 * @package             LogClass
 * @access              public  
 * 
 * @version: 3.0.0000 - 06/01/2017 - Jacques - Versão Inicial 
 * 
 */

namespace GoFast\Lib;

use GoFast\Kernel\Core;

class Log extends Core
{

    public static $instance;  
    
    private $_log;
    private $_file = 'error.log';
    private $line;
    private $lines;
    private $session;

    use \GoFast\Lib\Bridge;    
    
    /**
     * Método executado na construção da classe
     * 
     * @access public
     * @method __construct
     * @param
     * 
     * @return 
     */     
    public function __construct($value = null) {
        
        try {
        
            ob_start();

            parent::__construct($value);

            $this->createCoreClass($value);
        
            $log_file = ROOT_DIR.$this->_file;

            /**
             * verifica se existe o arquivo e caso não exista cria
             */            
            if(!$this->createFile($log_file)->isOk()) die(_("Não foi possível criar o arquivo de log em {$value}"));

            $file['error'] = ROOT_LIB.'error.class.php';            

            $value['login'] = sprintf("%-15s",substr(isset($_SESSION['login'])?$_SESSION['login']:'',0,15)) ;
            $value['logado'] = sprintf("%04d",isset($_COOKIE['logado'])?$_COOKIE['logado']:'0000');
            $value['session_id'] = sprintf("%032s", session_id());
            $value['server_name'] = sprintf("%-20s", substr(isset($_SERVER['SERVER_NAME'])?$_SERVER['SERVER_NAME']:'',0,20));

            $log_channel = "{$value['server_name']} {$value['session_id']} {$value['logado']} {$value['login']}";

            /**
             * O formato da data padrão é "Y-m-d H:i:s"
             */
            $dateFormat = "d-m-Y H:i:s";

            /**
             * O formato de saída padrão é "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n"
             */
            $output = "%datetime% %channel% %message%\n";

            /**
             * Crio um linha de formatação do log
             */
            $formatter = new \Monolog\Formatter\LineFormatter($output, $dateFormat);        

            /**
             * Crio alguns Handles
             */
            $stream    = new \Monolog\Handler\StreamHandler($log_file, \Monolog\Logger::DEBUG);
            $firephp   = new \Monolog\Handler\FirePHPHandler();        

            $stream->setFormatter($formatter);

            /**
             * Crio o log principal do aplicativo
             */
            $this->_log['main'] = new \Monolog\Logger($log_channel);

            $this->_log['main']->pushHandler($stream);
            $this->_log['main']->pushHandler($firephp);

            /**
             * Crio o log principal do aplicativo
             */
            $this->_log['security'] = new \Monolog\Logger($log_channel);

            $this->_log['security']->pushHandler($stream);
            $this->_log['security']->pushHandler($firephp);
        
            $this->setValue(1);

            ob_end_clean();
                    
        } catch (\Exception $ex) {
             
            $this->setValue(0);
            
            die(_("Error na classe de log"));

        }   
        
        
    }    
    
    /**
     * Método executado quando o objeto e referênciado diretamente para retornar uma string
     * 
     * @access public
     * @method __toString
     * @param
     * 
     * @return string
     */     
    public function __toString() {
        
        return (string)$this->value;

    }    
    
    /**
     * Método para criar o arquivo de log
     * 
     * @access public
     * @method createFile
     * @param
     * 
     * @return 
     */     
    public function createFile($value = null) {    
        
        try {
             
            $this->setValue(0);
            
            if(!file_exists($value)) {

                if($handle = fopen($value,'wb')) {
                    
                    chmod($handle, 0777);
                    
                    fclose($handle);
                    
                }
                else {
                    
                    if(!is_object($this->error)) die (_("Não foi detectado a class this->error, possivelmente problemas com o CURL ou HOST (log.class.php)"));
                    
                    die(_("Não foi possível criar o arquivo de log em {$value}"));

                }
                

            }        

            $this->setValue(1);
                    
        } catch (\Exception $ex) {
             
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);

        }   
        
        return $this;
        
    }
    
    /**
     * Método para definir os valores padrões da classe
     * 
     * @access public
     * @method setDefault
     * @param
     * 
     * @return 
     */     
    public function setDefault() {
        
        
    } 
    
    /**
     * Método utilizado para verificar o estado apôs execução de cada método
     * 
     * @access public
     * @method isOk
     * @param 
     * 
     * @return int
     */       
    public function isOk() {

        return (int)$this->value;

    }

    /**
     * Define valor para retorno no uso de métodos encadeados
     * 
     * @access public
     * @method setValue
     * @param $value
     * 
     * @return $this
     */       
    public function setValue($value = null){

        $this->value = $value;

        return $this;

    }
    
    /**
     * Registra um log de info
     * 
     * @access public
     * @method error
     * @param
     * 
     * @return 
     */     
    public function info($value = null) {

        $this->_log['main']->info($value);
        
    }    
    
    
    /**
     * Registra um log de warning
     * 
     * @access public
     * @method error
     * @param
     * 
     * @return 
     */     
    public function warning($value = null) {
        
        $this->_log['main']->warning($value);
        
    }    
    
    /**
     * Registra um log de error
     * 
     * @access public
     * @method error
     * @param
     * 
     * @return 
     */     
    public function error($value = null) {
        
        $this->_log['main']->error($value);
        
    }    
    
    
    
    /**
     * Método que lê o arquivo de log
     * 
     * @access public
     * @method read
     * @param  
     * 
     * @return 
     */     
     public function read(){
         
        try {
            

            
        } catch (Exception $ex) {

        }
         
        
        return $this;
        
     }    
     
    /**
     * Método que lê o arquivo de log
     * 
     * @access public
     * @method read
     * @param  
     * 
     * @return 
     */     
     public function show(){
         
         try {
             
         } catch (\Exception $ex) {

         }
        
        return $this;
        
     }    
     
    /**
     * Método que lê o arquivo de log e retorna as últimas 100 entradas
     * 
     * @access public
     * @method showLast
     * @param  
     * 
     * @return 
     */      
     public function showLastOld($value = null){
         
         try {
            
            if (is_array($value)) {

                foreach ($value as $k => $v) {
                    
                    switch ($k) {
                        case 'lines':
                            $qtd_rows = $v;
                            break;
                        case 'session':
                            $this->session = $v;
                            break;
                        case 'highlight':
                            $this->highlight = $v; 
                            break;
                        default:
                            break;
                    }

                }

            } 
            else {
                
                $qtd_rows = empty($value) ? 100 : $value;
                
            }             
             
            if($qtd_rows > 1000) $qtd_rows = 1000;
            
            $file = ROOT_DIR.$this->_file;
            
            $length = 1024;

            $offset_factor = 1;
            
            $bytes=filesize($file);

            $fp = fopen($file, "r") or die(_("Não foi possível abrir o arquivo de log"));

            $complete=false;
            
            $i = $qtd_rows;
            
            while (!$complete)
            {
                
                $offset = $qtd_rows * $length * $offset_factor;
                
                fseek($fp, -$offset, SEEK_END);

                if ($offset < $bytes) fgets($fp);
                
                while(!feof($fp))
                {
                    
                    $this->line = fgets($fp);
                    
                    if(!empty($this->line) && count($this->lines) <= $qtd_rows && $this->addFilterSessionLine()->isOk()) {
                     
                        $this->addSequential()->addLink()->addColorEventLine();
                                
                        $this->lines[$i--] = $this->line;
                        
                    }    

                    $complete = count($this->lines) > ($qtd_rows);
                    
                }
                
                if ($offset >= $bytes){
                    
                    $complete = true;
                    
                }
                else{
                    
                    $offset_factor *= 2;
                    
                }

            }
            
            fclose($fp);
            
//            exec("tail -n $qtd_rows $file",$lines);
            
            echo "<pre>";
            
            foreach ($this->lines as $key => $value) {
                
                echo $value;
                
            }
            
            echo "</pre>";
            
             
         } catch (\Exception $ex) {
             
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
            echo $this->error->getAllMsgCodeJson(0,'iso-8859-1');

         }
        
        return $this;
        
     }  
     
    public function showLast($value = null) {
        
        if (is_array($value)) {

             foreach ($value as $k => $v) {

                 switch ($k) {
                     case 'lines':
                         $qtd_rows = $v;
                         break;
                     case 'session':
                         $this->session = $v;
                         break;
                     case 'highlight':
                         $this->highlight = $v; 
                         break;
                     default:
                         break;
                 }

             }

         } 
         else {

             $qtd_rows = empty($value) ? 100 : $value;

         }         

        $file = ROOT_DIR.$this->_file;
        $handle = fopen($file, "r");
        $linecounter = $qtd_rows;
        $pos = -2;
        $beginning = false;
        $this->lines = array();
        
        while ($linecounter > 0) {
            $t = " ";
            while ($t != "\n") {
              if(fseek($handle, $pos, SEEK_END) == -1) {
                   $beginning = true; break; 
              }
              $t = fgetc($handle);
              $pos --;
            }

            if($beginning) rewind($handle);
            
            $this->line = fgets($handle);
            
            if(!empty($this->line) && count($this->lines) <= $qtd_rows && $this->addFilterSessionLine()->isOk()) {
                $this->addSequential($linecounter--)->addLink()->addColorEventLine();
                $this->lines[$lines-$linecounter-1] = $this->line;
            }
            
            if($beginning) break;
        }
        fclose ($handle);
        
        $this->showLogInHtmlTable();


    }
     
    /**
     * Método para adicionar a tag a href nas linhas de erro que contem urls
     * 
     * @access public
     * @method addLink
     * @param  
     * 
     * @return 
     */      
     public function addLink(){
         
         try {
             
            $this->setValue(0);
            
            // Definição do filtro da expressão regular com opçao de porta de conexão
            //$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}([:\/])(\S*\/)(\S*)?/";
            // Definição do filtro da expressão regular sem opção de identificar os links com porta            
            //$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
            // Definição do filtro da expressão regular completo           
            //$reg_exUrl = "/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|]/i";
            
            $reg_exUrl = "/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|]/i";
            
            // Verifica se o texto possui url
            if(preg_match($reg_exUrl, $this->line, $url)) $this->line = urldecode(preg_replace($reg_exUrl, "<a href=\"{$url[0]}\" target=\"_blank\">{$url[0]}</a> ", $this->line));
             
            $this->setValue(1);
                    
         } catch (\Exception $ex) {
             
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);

         }
        
        return $this;
        
     }    
     
     /**
     * Método para adicionar adicionar cor de fundo a linha de eventos
     * 
     * @access public
     * @method addColorEventLine
     * @param  
     * 
     * @return 
     */      
     public function addColorEventLine(){
         
         try {
             
            $this->setValue(0);
            
            if(in_array('session', explode(",",$this->highlight)) && strpos($this->line, 'SESSION')!==false)  $style .= ";background-color:#87ef87 ";
            if(in_array('request', explode(",",$this->highlight)) && strpos($this->line, 'REQUEST')!==false)  $style .= ";background-color:#87ef87 ";
            if(in_array('info', explode(",",$this->highlight)) && strpos($this->line, 'INFO')!==false)  $style .= ";background-color:#4db6ac ";
            if(in_array('notice', explode(",",$this->highlight)) && strpos($this->line, 'NOTICE')!==false)  $style .= ";background-color:#81d4fa ";
            if(in_array('warning', explode(",",$this->highlight)) && strpos($this->line, 'WARNING')!==false)  $style .= ";background-color:#f3f36f ";
            if(in_array('error', explode(",",$this->highlight)) && strpos($this->line, 'ERROR')!==false)  $style .= ";background-color:#f75555 ";
            $this->line = "<tr style=\"padding: 0{$style}\"><td style=\"padding: 0\"><pre>{$this->line}</pre></td></tr>";
            
            $this->setValue(1);
                    
         } catch (\Exception $ex) {
             
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);

         }
        
        return $this;
        
     }    
     
    /**
     * Método para adicionar filtrar as linhas de log por sessão de usuário
     * 
     * @access public
     * @method addFilterSessionLine
     * @param  
     * 
     * @return boolean
     */      
     public function addFilterSessionLine(){
         
        $this->setValue((strpos($this->line, $this->session)!==false || empty($this->session)));
        
        return $this;
        
     }  
     
    /**
     * Método para exibir o log em formato de tabela html
     * 
     * @access public
     * @method showLogInHtmlTable
     * @param  
     * 
     * @return 
     */      
     public function showLogInHtmlTable(){
         
         try {
        
            echo '<pre>';
            echo '<table>';
            foreach($this->lines as $key => $value) {
                echo $value;
            };
            echo '</table>';
            echo '</pre>';
             
            $this->setValue(1);
                    
         } catch (\Exception $ex) {
             
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);

         }
        
        return $this;
        
    }      
     
    /**
     * Método para adicionar sequencial
     * 
     * @access public
     * @method addSequential
     * @param  
     * 
     * @return boolean
     */      
     public function addSequential($value = null){
         
        $this->line = str_pad($value,4, "0", STR_PAD_LEFT).' - '.$this->line;
        
        return $this;
        
     }     
    
    /**
     * Método que lê o arquivo de log e retorna as últimas 100 entradas
     * 
     * @access public
     * @method showLast
     * @param  
     * 
     * @return 
     */      
     public function chkOS(){
         
         try {
             
            $this->setValue(0);
             
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') die(_('Essa chamada só pode ser executada em um servidor linux'));
            
            $this->setValue(1);
                    
         } catch (\Exception $ex) {
             
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);

         }
        
        return $this;
        
     }      
    
}
