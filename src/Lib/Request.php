<?php 

/**
 * Classe para manipulação de requisição ao servidor
 * 
 * @Jacques
 */
namespace GoFast\Lib;

class Request {

    public const METHOD_HEAD = 'HEAD';
    public const METHOD_GET = 'GET';
    public const METHOD_POST = 'POST';
    public const METHOD_PUT = 'PUT';
    public const METHOD_PATCH = 'PATCH';
    public const METHOD_DELETE = 'DELETE';
    public const METHOD_PURGE = 'PURGE';
    public const METHOD_OPTIONS = 'OPTIONS';
    public const METHOD_TRACE = 'TRACE';
    public const METHOD_CONNECT = 'CONNECT';   
    
    private $value_return;
    private $value;
    
    public function __construct($value = null) {
        
        $data = json_decode(file_get_contents("php://input"), true);
        
    }

    /**
     * Retorna a requisição como uma string
     *
     * @return string The request
     */
    public function __toString() {

        $content = $this->getContent();

        $cookieHeader = '';
        $cookies = [];

        foreach ($this->cookies as $k => $v) {
            $cookies[] = $k.'='.$v;
        }

        if (!empty($cookies)) {
            $cookieHeader = 'Cookie: '.implode('; ', $cookies)."\r\n";
        }

        return
            sprintf('%s %s %s', $this->getMethod(), $this->getRequestUri(), $this->server->get('SERVER_PROTOCOL'))."\r\n".
            $this->headers.
            $cookieHeader."\r\n".
            $content;
    }

    /**
     * Returns the JSON encoded POST data, if any, as an object.
     * 
     * @return Object|null
     */
    private function retrieveJsonPostData()
    {
        // get the raw POST data
        $rawData = file_get_contents("php://input");

        // this returns null if not valid json
        return json_decode($rawData);
    }    

    /**
     * Returns the request body content.
     *
     * @param bool $asResource If true, a resource will be returned
     *
     * @return string|resource The request body content or a resource to read the body stream
     */
    public function getContent($asResource = false)
    {
        $currentContentIsResource = \is_resource($this->content);

        if (true === $asResource) {
            if ($currentContentIsResource) {
                rewind($this->content);

                return $this->content;
            }

            // Content passed in parameter (test)
            if (\is_string($this->content)) {
                $resource = fopen('php://temp', 'r+');
                fwrite($resource, $this->content);
                rewind($resource);

                return $resource;
            }

            $this->content = false;

            return fopen('php://input', 'rb');
        }

        if ($currentContentIsResource) {
            rewind($this->content);

            return stream_get_contents($this->content);
        }

        if (null === $this->content || false === $this->content) {
            $this->content = file_get_contents('php://input');
        }

        return $this->content;
    }

    
  
    /**
     * Captura a requisição realizada para o servidor
     *
     * @return static
     */
    public static function capture()  {

        $request = self::create(['GET' => $_GET,'POST' => $_POST, 'COOKIE' => $_COOKIE, 'FILES' => $_FILES, 'SERVER' => $_SERVER]);
   
        return new static($request);
    }
    
    /**
     * Creates a new request with values from PHP's super globals.
     *
     * @return static
     */
    public static function create($value = null) {
        
        parse_url($url);

        var_dump(parse_url($url));
        var_dump(parse_url($url, PHP_URL_SCHEME));
        var_dump(parse_url($url, PHP_URL_USER));
        var_dump(parse_url($url, PHP_URL_PASS));
        var_dump(parse_url($url, PHP_URL_HOST));
        var_dump(parse_url($url, PHP_URL_PORT));
        var_dump(parse_url($url, PHP_URL_PATH));
        var_dump(parse_url($url, PHP_URL_QUERY));
        var_dump(parse_url($url, PHP_URL_FRAGMENT));

        return $request;
    }   
    

}
