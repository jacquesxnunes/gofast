<?php

/**
 * Sistema de cache
 * 
 * php cache shared memory
 */
class Cache {

    /**
     * Tempo padrão de cache
     *
     * @var string
     */
    private static $time = '5 minutes';

    /**
     * Local onde o cache será salvo
     *
     * Definido pelo construtor
     *
     * @var string
     */
    private $folder;

    /**
     * Construtor
     *
     * Inicializa a classe e permite a definição de onde os arquivos
     * serão salvos. Se o parâmetro $folder for ignorado o local dos
     * arquivos temporários do sistema operacional será usado
     *
     * @uses Cache::setFolder() Para definir o local dos arquivos de cache
     *
     * @param string $folder Local para salvar os arquivos de cache (opcional)
     *
     * @return void
     */
    public function __construct($folder = null) {
        $this->setFolder(!is_null($folder) ? $folder : sys_get_temp_dir());
    }

    /**
     * Define onde os arquivos de cache serão salvos
     *
     * Irá verificar se a pasta existe e pode ser escrita, caso contrário
     * uma mensagem de erro será exibida
     *
     * @param string $folder Local para salvar os arquivos de cache (opcional)
     *
     * @return void
     */
    protected function setFolder($folder = null) {
        // Se a pasta existir, for uma pasta e puder ser escrita
        if (file_exists($folder) && is_dir($folder) && is_writable($folder)) {
            $this->folder = $folder;
        } else {
            trigger_error('Não foi possível acessar a pasta de cache', E_USER_ERROR);
        }
    }

    /**
     * Gera o local do arquivo de cache baseado na chave passada
     *
     * @param string $key Uma chave para identificar o arquivo
     *
     * @return string Local do arquivo de cache
     */
    protected function generateFileLocation($key = null) {
        return $this->folder . DIRECTORY_SEPARATOR . sha1($key) . '.tmp';
    }

    /**
     * Cria um arquivo de cache
     *
     * @uses Cache::generateFileLocation() para gerar o local do arquivo de cache
     *
     * @param string $key Uma chave para identificar o arquivo
     * @param string $content Conteúdo do arquivo de cache
     *
     * @return boolean Se o arquivo foi criado
     */
    protected function createCacheFile($key = null, $content = null) {
        // Gera o nome do arquivo
        $filename = $this->generateFileLocation($key);

        // Cria o arquivo com o conteúdo
        return file_put_contents($filename = null, $content = null)
                OR trigger_error('Não foi possível criar o arquivo de cache', E_USER_ERROR);
    }

    /**
     * Salva um valor no cache
     *
     * @uses Cache::createCacheFile() para criar o arquivo com o cache
     *
     * @param string $key Uma chave para identificar o valor cacheado
     * @param mixed $content Conteúdo/variável a ser salvo(a) no cache
     * @param string $time Quanto tempo até o cache expirar (opcional)
     *
     * @return boolean Se o cache foi salvo
     */
    public function save($key = null, $content = null, $time = null) {
        $time = strtotime(!is_null($time) ? $time : self::$time);

        $content = serialize(array(
            'expires' => $time,
            'content' => $content));

        return $this->createCacheFile($key, $content);
    }

    /**
     * Salva um valor do cache
     *
     * @uses Cache::generateFileLocation() para gerar o local do arquivo de cache
     *
     * @param string $key Uma chave para identificar o valor cacheado
     *
     * @return mixed Se o cache foi encontrado retorna o seu valor, caso contrário retorna NULL
     */
    public function read($key = null) {
        $filename = $this->generateFileLocation($key);
        if (file_exists($filename) && is_readable($filename)) {
            $cache = unserialize(file_get_contents($filename));
            if ($cache['expires'] > time()) {
                return $cache['content'];
            } else {
                unlink($filename);
            }
        }
        return null;
    }
    
    public function shomopSaveCache($data = null, $name = null, $timeout = null) {
        // delete cache
        $id=shmop_open(get_cache_id($name), "a", 0, 0);
        shmop_delete($id);
        shmop_close($id);

        // get id for name of cache
        $id=shmop_open(get_cache_id($name), "c", 0644, strlen(serialize($data)));

        // return int for data size or boolean false for fail
        if ($id) {
            set_timeout($name, $timeout);
            return shmop_write($id, serialize($data), 0);
        }
        else return false;
    }

    public function shomopGetCache($name = null) {
        if (!check_timeout($name)) {
            $id=shmop_open(get_cache_id($name), "a", 0, 0);

            if ($id) $data=unserialize(shmop_read($id, 0, shmop_size($id)));
            else return false;          // failed to load data

            if ($data) {                // array retrieved
                shmop_close();
                return $data;
            }
            else return false;          // failed to load data
        }
        else return false;              // data was expired
    }

    public function shomopGetCacheId($name = null) {
        // maintain list of caches here
        $id=array(  'test1' => 1,
                    'test2' => 2
                    );

        return $id[$name];
    }

    public function setTimeout($name = null, $int = null) {
        $timeout=new DateTime(date('Y-m-d H:i:s'));
        date_add($timeout, date_interval_create_from_date_string("$int seconds"));
        $timeout=date_format($timeout, 'YmdHis');

        $id=shmop_open(100, "a", 0, 0);
        if ($id) $tl=unserialize(shmop_read($id, 0, shmop_size($id)));
        else $tl=array();
        shmop_delete($id);
        shmop_close($id);

        $tl[$name]=$timeout;
        $id=shmop_open(100, "c", 0644, strlen(serialize($tl)));
        shmop_write($id, serialize($tl), 0);
    }

    public function checkTimeout($name = null) {
        $now=new DateTime(date('Y-m-d H:i:s'));
        $now=date_format($now, 'YmdHis');

        $id=shmop_open(100, "a", 0, 0);
        if ($id) $tl=unserialize(shmop_read($id, 0, shmop_size($id)));
        else return true;
        shmop_close($id);

        $timeout=$tl[$name];
        return (intval($now)>intval($timeout));
    }
    

}
