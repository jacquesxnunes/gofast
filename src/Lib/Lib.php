<?php

/*
 * PHP-DOC - Classe de bibliotecas
 *  
 * 29/02/2016
 * 
 * Classe de com biblioteca de automatiza��o do framework
 * 
 * Vers�o: 3.00.7788 - 29/02/2016 - Jacques - Vers�o Inicial
 * 
 * @Jacques
 */

namespace GoFast\Lib;

use GoFast\Kernel\Core;

class Lib extends Core
{
    const TOKEN_TIME = 60;

    public static $instance;
    public $config;

    use \GoFast\Lib\Bridge;

    /*
     * PHP-DOC 
     * 
     * @name setDefault
     * 
     * @internal - Define um valor padr�o de in�cio de valor para as propriedades do objeto
     */

    public function setDefault()
    {

        $this->value = '';

        return $this;
    }

    /**
     * Método gerador de token
     * 
     * @param  string key
     * 
     * @return string hash
     */
    public function getToken($key = null)
    {

        $hash = null;

        $token_time = $this->config->title('framework')->key('token_time')->val();
        $token_time = ($token_time ? $token_time : self::TOKEN_TIME);

        $secret_key = bin2hex(empty($key) ? "12345678901234567890" : $key);

        $time_window = $token_time;
        $exact_time = microtime(true);
        $rounded_time = round($exact_time / $time_window);


        $hash = sha1($secret_key . $rounded_time);


        return $hash;
    }

    /*
     * PHP-DOC 
     * 
     * @name - getMakeHtmlOption
     * 
     * @internal - Retorna a montagem de um option range de acordo com os par�metros que pode ser um vetor 
     *             com segundo par�metro de string definindo o �ndice dos vetores para atribui��o de valor,
     *             ou o intervalo de dois inteiros entre o valor um e dois
     */

    public function getMakeHtmlOption($value1 = null, $value2 = null, $value3 = '<option value="&value1">&value1 - &value2</option>')
    {

        try {

            if (is_array($value1) && is_string($value2)) {

                $field = explode(',', $value2);

                foreach ($value1 as $key => $value) {

                    $this->value .= str_replace('&value2', $value[$field[0]], str_replace('&value1', $value[$field[1]], $value3));
                }
            } elseif (is_int($value1) && is_int($value2)) {

                for ($i = $ini; $i <= $end; $i++) {

                    $this->value .= str_replace('&value2', $i, str_replace('&value1', $i, $value3));
                }
            } else {

                $this->error->set(array(3, __METHOD__), E_FRAMEWORK_ERROR);
            }
        } catch (\Exception $ex) {

            $this->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING);
        }

        exit($this->value);


        return $this;
    }

    function array_find($string = null, $array = null)
    {

        foreach ($array as $key => $value) {

            if (strpos($value, $string) !== FALSE) {

                return $key;

                break;
            }
        }
    }

    /**
     * Método para verificar todas as classes carregadas e que são fundamentais para execução do código do 
     */
    function check_class()
    {

        $array = get_declared_classes();
    }

    /**
     * Formata um determinado conjunto de plano de contas em formato hierárquico (árvore)
     * @param array $array1 Array com os planos de conta a ser formatado (Desde que tenha o campo classificador e nível)
     * @param int $nivel Parâmetro usado apenas na chamada recursiva, dizendo o nível de conta a ser formatado
     * @return array Array formatado hierárquicamente.
     */
    //    public function formatPlanoContas(array $array1, $nivel = 1) {
    //        $array2 = [];
    //        foreach ($array1 as $key => $value) {
    //            if ($value['nivel'] == $nivel) {
    //                $array2[$value['classificador']] = $value;
    //                unset($array1[$key]);
    //                $item = $this->formatPlanoContas($array1, $nivel + 1);
    //                if (is_array($item) && count($item) > 0) {
    //                    $array2[$value['classificador']]['node'] = $item;
    //                }
    //            }
    //        }
    //        return $array2;
    //    }


    public function formatPlanoContas(array $array1 = null, $nivel = 1, $cta_superior = 0, $return_array = FALSE)
    {
        $array2 = [];

        $valor1 = 0;

        $valor2 = 0;

        foreach ($array1 as $key => $value) {
            if ($value['cta_superior'] == $cta_superior && $value['nivel'] == $nivel) {
                $array2[$value['classificador']] = $value;
                unset($array1[$key]);
                $temp = $this->formatPlanoContas($array1, $nivel + 1, $value['id_balanco'], TRUE);
                $item = $temp['item'];
                if (is_array($item) && count($item) > 0) {
                    $array2[$value['classificador']]['node'] = $item;
                    $array2[$value['classificador']]['valor1'] += $temp['valor1'];
                    $array2[$value['classificador']]['valor2'] += $temp['valor2'];
                }
                $valor1 += ($value['receita_despesa'] == '1') ? $value['valor1'] : -$value['valor1'];
                $valor2 += ($value['receita_despesa'] == '1') ? $value['valor2'] : -$value['valor2'];
            }
        }

        ksort($array2);
        return $return_array ? ['item' => $array2, 'valor1' => $valor1, 'valor2' => $valor2] : $array2;
    }

    /**
     *
     * @param array $array1
     */
    public function montaHtmlSubNiveis(array $array1 = null, $print_mode = FALSE)
    {
        $html = '';
        //        $total_ano = 0;
        //        $total_ano_anterior = 0;
        foreach ($array1 as $key => $value) {
            $endentacao = '';
            for ($i = 0; $i < $value['nivel']; $i++) {
                $endentacao .= '&emsp;';
            }
            if (is_array($value['node']) && !empty($value['node'])) {
                $html .= '<tr class="valign-middle">';
                $html .= '<td colspan="5" class="text-bold">' . $endentacao . $value['descricao'] . '</td>';
                $html .= '</tr>';
                $html .= $this->montaHtmlSubNiveis($value['node'], $print_mode);
                $html .= '<tr>';
                $html .= '<td colspan="2" class="text-bold">' . $endentacao . 'TOTAL ' . $value['descricao'] . '</td>';
                $parenteses = ($value['valor1'] >= 0) ? ['', ''] : ['(', ')'];
                $html .= '<td style="border-top:1px solid #000;" class="text-right text-bold">R$ <input type="text" name="valor1[]" size="10" class=" from-sm valor1 money replaceable" value="' . $parenteses[0] . number_format(abs($value['valor1']), 2, ',', '.') . $parenteses[1] . '"></td>';
                $html .= '<td>&emsp;</td>';
                $parenteses = ($value['valor2'] >= 0) ? ['', ''] : ['(', ')'];
                $html .= '<td style="border-top:1px solid #000;" class="text-right text-bold">R$ <input type="text" name="valor2[]" size="10" class=" from-sm valor2 money replaceable" value="' . $parenteses[0] . number_format(abs($value['valor2']), 2, ',', '.') . $parenteses[1] . '"></td>';
                $html .= '</tr>';
            } else {
                $html .= '<tr>';
                $html .= '<td>' . $endentacao . $value['descricao'] . '</td>';
                $html .= '<td class="text-center">';
                if (!$print_mode) {
                    $html .= '<span class="txt_nota">'
                        . '<span class="num_nota" style="display:none"></span>'
                        . "<input type=\"text\" class=\"input_nota replaceable\" size=\"5\" value=\"{$value['txt_nota']}\" data-id=\"{$value['id_balanco']}\">"
                        . '</span>';
                } else {
                    $html .= $value['txt_nota'] != '' ? '(' . $value['txt_nota'] . ')' : '';
                }
                $html .= '</td>';
                $parenteses = ($value['valor1'] >= 0) ? ['', ''] : ['(', ')'];
                $html .= '<td class="text-right">R$ <input type="text" name="valor1[]" size="10" class=" from-sm valor1 money replaceable" value="' . $parenteses[0] . number_format((abs($value['valor1'])), 2, ',', '.') . $parenteses[1] . '"></td>';
                $html .= '<td>&emsp;</td>';
                $parenteses = ($value['valor2'] >= 0) ? ['', ''] : ['(', ')'];
                $html .= '<td class="text-right">R$ <input type="text" name="valor2[]" size="10" class=" from-sm valor2 money replaceable" value="' . $parenteses[0] . number_format((abs($value['valor2'])), 2, ',', '.') . $parenteses[1] . '"></td>';
                $html .= '</tr>';
                //                $total_ano += $value["valor1"];
                //                $total_ano_anterior += $value["valor2"];
            }
        }
        return $html;
    }

    public function totalizaPlanoContas($array = null)
    {
        $total1 = 0;
        $total2 = 0;
        foreach ($array as $key => $value) {
            if (is_array($value['node']) && !empty($value['node'])) {
                $temp = $this->totalizaPlanoContas($value['node']);
                $array[$key]['valor1'] = $temp['total1'];
                $array[$key]['valor2'] = $temp['total2'];
                $array[$key]['node'] = $temp['array'];
            }
            $total1 += ($value['receita_despesa']) ? $value['valor1'] : -$value['valor1'];
            $total2 += ($value['receita_despesa']) ? $value['valor2'] : -$value['valor2'];
        }
        return ['array' => $array, 'total1' => $total1, 'total2' => $total2];
    }
}
