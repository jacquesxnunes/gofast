<?php

namespace GoFast\Auth;

use GoFast\Kernel\Core;

class User extends Core
{

    const HASH_LENGTH = 40;
    const TOKEN_LENGTH = 20;
    const PASSWORD_MIN = 6;
    const SESSION_EXPIRE = 3600;
    const SITE_KEY = 'voSjOOcifMwoLEqS85Ho1pV9N5GN0Qpy8PQwKmn3zuI';
    const PASSWORD_DEFAULT = '2Y';
    const BCRYPT_COST =  1;
    const VERIFY_EMAIL_MIN_LENGTH = 3;   // Limite mínimo de tamanho de um email
    const VERIFY_EMAIL_MAX_LENGTH = 254; // Limite máximo de tamanho de um email
    const COOKIE_NAME = 'goFastID';
    const REDIRECT_TO_LOGIN = 'login';
    const REDIRECT_TO_REGISTER = 'register';
    const REDIRECT_TO_RESET = 'reset';
    const REDIRECT_TO_INDEX = 'index';

    public static $instance;

    public $error;
    public $config;
    public $users;
    public $alias;
    public $router;
    public $id = 0;
    public $email = '';
    public $password = '';
    public $name = '';

    protected $isAuthenticated = 0;
    protected $currentuser = null;
    protected $messages_dictionary = [];
    protected $recaptcha_config = [];



    private $hash = ['key' => ['public' => '', 'private' => '']];
    private $agent = '';
    private $cookie = [
        'name' => '',
        'value' => '',
        'expire' => 0,
        'path' => '/',
        'domain' => '',
        'secure' => 0,
        'httponly' => 0
    ];

    use \GoFast\Lib\Bridge;


    /**
     * Initiates database connection
     *
     * @param array
     */
    public function __construct($value = null)
    {

        try {

            if (is_array($value)) {

                foreach ($value as $k => $v) {

                    switch ($k) {
                        case 'nao_definido':
                            $nao_definido = $v;
                            break;
                        default:
                            break;
                    }
                }
            }

            parent::__construct($value);

            $this->createCoreClass($value);  

            $session['config']['expire'] = $this->config->id(\GoFast\Lib\Config::ID_FW)->setFile(\GoFast\Lib\Config::PATH_FW)->title('auth')->key('session_expire')->val();
            $session['expire'] = $session['config']['expire'] ? $session['config']['expire'] : self::SESSION_EXPIRE;

            $this->cookie['domain'] = DOMAIN;
            $this->cookie['expire'] = time() + $session['expire'];
            $this->cookie['name'] = self::COOKIE_NAME;

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }

    /**
     * Método que define Define valores default da classe
     * 
     * @access public
     * @method setDefault
     * @param  
     * 
     * @return $this
     */
    public function setDefault()
    {

        return $this;
    }

    /**
     * Configuração da classe de autenticação
     * 
     * @param  array
     *
     * @return $this
     */
    public function config($value = null)
    {

        try {


            $return = [];

            if (is_array($value)) {

                foreach ($value as $k => $v) {

                    switch ($k) {
                        case 'alias':
                            $this->alias = $v;
                            break;
                        case 'redirect':
                            $this->router['redirect'] = $v;
                            break;
                        default:
                            break;
                    }
                }
            }


            if (is_object($value['container']['instance']))
                $this->fw = $value['container']['instance'];


            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }


    /**
     * Carrega para classe de user os dados primários de outra classe de cadastro
     * 
     * @return $this
     */
    public function loadUser()
    {


        try {

            $object_alias = ucfirst($this->alias['object']);
            $setEmail = 'set' . ucfirst($this->alias['email']);

            $this->fw->users = $this->fw->$object_alias->getInstance();

            if (!is_object($this->fw->users)) $this->error->set(_("Objeto de usuário não definido"), E_FRAMEWORK_ERROR);
            if (!$this->fw->users->setDefault()->$setEmail($this->email)->select()->getRow()->isOk()) $this->error->set(_("Usuário não cadastrado"), E_FRAMEWORK_ERROR);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }

    /**
     * Carrega o ID do usuário relativo ao objetivo mapeado
     * 
     * @return $this
     */
    public function getId()
    {

        $getId = 'get' . ucfirst($this->alias['id']);

        if (!is_object($this->fw->users)) $this->error->set(_("Objeto de usuário não definido para recuperação do ID"), E_FRAMEWORK_ERROR);

        return $this->fw->users->$getId();
    }

    /**
     * Carrega o email do usuário relativo ao objetivo mapeado
     * 
     * @return $this
     */
    public function getEmail()
    {

        $getEmail = 'get' . ucfirst($this->alias['email']);

        if (!is_object($this->fw->users)) $this->error->set(_("Objeto de usuário não definido para recuperação do email"), E_FRAMEWORK_ERROR);

        return $this->fw->users->$getEmail();
    }

    /**
     * Obtem um hash da senha
     * 
     * @return string
     */
    public function getPasswordHash($value)
    {

        return password_hash($value, PASSWORD_DEFAULT);
    }

    /**
     * Obtem um hash da senha
     * 
     * @return string
     */
    public function getPassword()
    {


        $getPassword = 'get' . ucfirst($this->alias['password']);

        return $this->fw->users->$getPassword();
    }

    /**
     * Carrega o email do usuário relativo ao objetivo mapeado
     * 
     * @return $this
     */
    public function getName()
    {

        $getName = 'get' . ucfirst($this->alias['name']);

        if (!is_object($this->fw->users)) $this->error->set(_("Objeto de usuário não definido para recuperação do nome de usuário"), E_FRAMEWORK_ERROR);

        return $this->fw->users->$getName();
    }

    /**
     * Método para comparação de senha
     *
     * @return $this
     */
    public function comparePasswords()
    {

        try {

            if (!password_verify($this->password, $this->getPassword()))
                $this->error->set(_("Verificação de senha não obteve sucesso"), E_FRAMEWORK_ERROR);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }


    /**
     * Verifica a qualidade da senha
     *
     * @param string $password
     *
     * @return object $this
     */
    public function validatePassword()
    {

        try {


            if (strlen($this->password) < self::PASSWORD_MIN)
                $this->error->set(_("Tamanho da senha menor que a permitida"), E_FRAMEWORK_ERROR);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }

    /**
     * Verifica se é um email válido
     * 
     * @param string $email
     *
     * @return object $this
     */
    public function validateEmail($email = "")
    {

        try {

            if (strlen($this->email) < (int)self::VERIFY_EMAIL_MIN_LENGTH) $this->error->set(_("Email muito curto"), E_FRAMEWORK_ERROR);

            if (strlen($this->email) > (int)self::VERIFY_EMAIL_MAX_LENGTH) $this->error->set(_("Email muito longo"), E_FRAMEWORK_ERROR);

            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) $this->error->set(_("Não é um email válido"), E_FRAMEWORK_ERROR);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }



    /**
     * Returns IP address
     * @return string $ip
     */
    protected function getIp()
    {
        if (getenv('HTTP_CLIENT_IP')) {
            $ipAddress = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipAddress = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            $ipAddress = getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            $ipAddress = getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            $ipAddress = getenv('HTTP_FORWARDED');
        } elseif (getenv('REMOTE_ADDR')) {
            $ipAddress = getenv('REMOTE_ADDR');
        } else {
            $ipAddress = '127.0.0.1';
        }

        return $ipAddress;
    }

    /**
     * Retorna o hash armazenado no cookie
     *
     * @return string
     */
    private function getPublicKeyCookie()
    {
        return isset($_COOKIE[$this->cookie['name']]) ? $_COOKIE[$this->cookie['name']] : "";
    }



    /**
     * Retorna o hash armazenado no cookie
     *
     * @return string
     */
    private function getPublicKey()
    {


        $getId = 'get' . ucfirst($this->alias['id']);
        $getEmail = 'get' . ucfirst($this->alias['email']);

        return sha1(self::SITE_KEY . session_id() . $this->getIp() . microtime());
    }

    /**
     * Retorna o hash armazenado no cookie
     *
     * @return string
     */
    private function getPrivateKey()
    {


        $getId = 'get' . ucfirst($this->alias['id']);
        $getEmail = 'get' . ucfirst($this->alias['email']);

        return  sha1(session_id() . $this->getIp() . self::SITE_KEY);
    }


    /**
     * Retorna o hash armazenado no cookie
     *
     * @return string
     */
    private function getCertificateKey($value = null)
    {

        $private_key = $this->getPrivateKey();
        $public_key  = $this->getPublicKey();

        if (is_array($value)) {

            foreach ($value as $k => $v) {

                switch ($k) {
                    case 'private_key':
                        $private_key = $v;
                        break;
                    case 'public_key':
                        $public_key = $v;
                        break;
                    default:
                        break;
                }
            }
        }

        return  sha1($public_key . $private_key);
    }

    /**
     * Método para comparação de senha
     *
     * @return $this
     */
    public function setMakeHashs()
    {

        try {

            $getId = 'get' . ucfirst($this->alias['id']);
            $getEmail = 'get' . ucfirst($this->alias['email']);

            $this->hash['key']['public'] = $this->getPublicKey();
            $this->hash['key']['private'] =  $this->getPrivateKey();
            $this->hash['key']['certificate'] = $this->getCertificateKey(['public_key' => $this->hash['key']['public']]);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }



    /**
     * Cria uma sessão para um usuário 
     * 
     * @return object $this
     */
    private function addSession()
    {

        try {


            $this->setMakeHashs();
            $this->save(['id' => $this->hash['key']['public'], 'data' => $this->hash['key']['private']]);
            $this->save(['id' => session_id(), 'data' => $this->getEmail()]);

            setcookie($this->cookie['name'], $this->hash['key']['public'], $this->cookie['expire'],  $this->cookie['path'], $this->cookie['domain'], $this->config->cookie_secure, $this->config->cookie_http);

            $_COOKIE[$this->cookie['name']] = $this->hash['key']['public'];

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }
    /**
     * Função para verificar se é uma sessão de usuário válida
     * 
     *
     * @return $this
     */
    public function checkSession()
    {

        try {

            $this->email = $this->load(['id' => session_id(), 'state' => 0]);
            if (!$this->loadUser()->isOk())  $this->error->set(_("Não foi possível carregar a conta de usuário"), E_FRAMEWORK_ERROR);

            $cert1 = $this->getCertificateKey(['public_key' => $this->getPublicKeyCookie()]);
            $cert2 = $this->getCertificateKey(['public_key' => $this->getPublicKeyCookie(), 'private_key' => $this->load(['id' => $this->getPublicKeyCookie(), 'state' => 0])]);

            $this->isAuthenticated = $this->config->id(\GoFast\Lib\Config::ID_FW)->setFile(\GoFast\Lib\Config::PATH_FW)->title('auth')->key('do_not_applay_session_authentication_strategy')->val() ? 1 : ($cert1 == $cert2);

            if (!$this->isAuthenticated) $this->error->set(_("Sessão inválida"), E_FRAMEWORK_ERROR);

            $this->save(['id' => $this->getPublicKeyCookie(), 'data' => '']); // Apaga sessão anterior
            $this->addSession();
            $this->save(['id' => session_id(), 'data' => $this->getEmail()]);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }

    /**
     * Função para redirecionar o acesso caso array de parâmetro esteja definido
     * 
     * @param  string
     * 
     * @return $this
     */
    public function redirectTo($value = null)
    {

        try {

            if (isset($this->router['redirect'][$value]))
                $this->redirect($this->router['redirect'][$value]);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        return $this;
    }

    /**
     * Autenticação do usuário
     * 
     * @param  array
     *
     * @return array $return
     */
    public function login($value = null)
    {

        try {

            $not_redirect = 0;

            if (is_array($value)) {

                $this->config($value);

                foreach ($value as $k => $v) {

                    switch ($k) {
                        case 'auth':
                            foreach ($v as $k_1 => $v_1) {
                                switch ($k_1) {
                                    case 'email':
                                        $this->email = $v_1;
                                        break;
                                    case 'password':
                                        $this->password = $v_1;
                                        break;
                                }
                            }
                            break;
                        case 'not_redirect':
                            $not_redirect = $v;
                            break;
                        default:
                            break;
                    }
                }
            }

            if (!$this->loadUser()->isOk())  $this->error->set(_("Não foi possível carregar a conta de usuário"), E_FRAMEWORK_ERROR);
            if (!$this->comparePasswords()->isOk()) $this->error->set(_("Senha incorreta"), E_FRAMEWORK_ERROR);
            if (!$this->addSession()->isOk()) $this->error->set(_("Não foi possível criar a sessão do usuário"), E_FRAMEWORK_ERROR);
            if (!$this->checkSession()->isOk()) $this->error->set(_("Não foi possível criar a sessão do usuário"), E_FRAMEWORK_ERROR);

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }


        if (!$not_redirect)
            $this->redirectTo($this->isOk() ? \GoFast\Auth\User::REDIRECT_TO_INDEX : \GoFast\Auth\User::REDIRECT_TO_LOGIN);

        return $this;
    }

    /**
     * Finaliza uma sessão de usuário
     * 
     * @param  array
     *
     * @return array $return
     */
    public function logout($value = null)
    {

        try {

            $not_redirect = 0;

            if (is_array($value)) {

                $this->config($value);

                foreach ($value as $k => $v) {

                    switch ($k) {
                        case 'not_redirect':
                            $not_redirect = $v;
                            break;
                        default:
                            break;
                    }
                }
            }

            $this->save(['id' => session_id(), 'data' => 'logout']);
            session_destroy();

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }

        if (!$not_redirect)
            $this->redirectTo(\GoFast\Auth\User::REDIRECT_TO_LOGIN);

        return $this;
    }

    /**
     * Verifica se o usuário está autenticado e possui uma chave válida
     * 
     * @return boolean
     */
    public function isLogged()
    {

        try {

            $this->isAuthenticated = $this->isAuthenticated ? $this->isAuthenticated : $this->checkSession()->isOk();

            $this->setValue(1);
        } catch (\Exception $ex) {

            $this->setValue(0)->error->set(array(1, __METHOD__), E_FRAMEWORK_WARNING, $ex);
        }


        if (!$this->isAuthenticated)
            $this->redirectTo(\GoFast\Auth\User::REDIRECT_TO_LOGIN);

        return $this->isAuthenticated;
    }
}
