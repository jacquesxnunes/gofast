# Changelog

All notable changes to `spatie/string` will be documented in this file

## 0.8.0 - 2020-09-22

### Added
- Making the repository available
