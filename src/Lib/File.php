<?php 
/*
 * PHO-DOC - FileClass.php
 * 
 * Classe de defini��o de tipos de documentos para upload
 * 
 * 26-02-2016
 *
 * @name FileClass 
 * @package FileClass
 * @access public 
 *  
 * @version 
 *
 * Vers�o: 3.0.5055 - 26/02/2016 - Jacques - Vers�o Inicial
 * 
 * @author jacques@f71.com.br
 * 
 * @copyright www.f71.com.br 
 *  
 */ 
namespace GoFast\Lib;

use GoFast\Kernel\Core;

class File extends Core{
    
    
    private $_file = array(
                         'path' => '',
                         'public_path' => '',
                         'name' => '',
                         'ext' => '',
                         'size' => 0,
                         'create' => '',
                         'update' => ''
                         );
    
    public function setDefault(){
        
        
    }
    
    public function setPath($value = null) {
        
        $this->_file['path'] = $value;
        
        return $this;
        
    }
    
    public function getPath() {
        
        return $this->_file['path'];
        
    }
    
    public function setPublicPath($value = null) {
        
        $this->_file['public_path'] = $value;
        
        return $this;
        
    }
    
    public function getPublicPath() {
        
        return $this->_file['public_path'];
        
    }
    
    public function setName($value = null) {
        
        $this->_file['name'] = $value;
        
        return $this;
        
    }
    
    public function getName() {
        
        return $this->_file['name'];
        
    }
    
    public function setExt($value = null) {
        
        $this->_file['ext'] = $value;
        
        return $this;
        
    }
    
    public function getExt() {
        
        return $this->_file['ext'];
        
    }
    
    public function setSize($value = null) {
        
        $this->_file['size'] = $value;
        
        return $this;
        
    }
    
    public function getSize() {
        
        return $this->_file['size'];
        
    }
    
    public function getPublicLocation(){
        
        return $this->getPublicPath().$this->getName().$this->getExt();
        
    }
    
    public function exists(){
        
        return file_exists($this->getPath());
        
    }
    
    public function unlink(){
        
        return unlink($this->getPath());
        
    }
    
    public function fileperms() {
        
        $perms = fileperms('/etc/passwd');

        if (($perms & 0xC000) == 0xC000) {
            // Socket
            $info = 's';
        } elseif (($perms & 0xA000) == 0xA000) {
            // Link simbólico
            $info = 'l';
        } elseif (($perms & 0x8000) == 0x8000) {
            // Regular
            $info = '-';
        } elseif (($perms & 0x6000) == 0x6000) {
            // Bloco especial
            $info = 'b';
        } elseif (($perms & 0x4000) == 0x4000) {
            // Diretório
            $info = 'd';
        } elseif (($perms & 0x2000) == 0x2000) {
            // Caractere especial
            $info = 'c';
        } elseif (($perms & 0x1000) == 0x1000) {
            // FIFO pipe
            $info = 'p';
        } else {
            // Desconhecido
            $info = 'u';
        }

        // Proprietário
        $info .= (($perms & 0x0100) ? 'r' : '-');
        $info .= (($perms & 0x0080) ? 'w' : '-');
        $info .= (($perms & 0x0040) ?
                    (($perms & 0x0800) ? 's' : 'x' ) :
                    (($perms & 0x0800) ? 'S' : '-'));

        // Grupo
        $info .= (($perms & 0x0020) ? 'r' : '-');
        $info .= (($perms & 0x0010) ? 'w' : '-');
        $info .= (($perms & 0x0008) ?
                    (($perms & 0x0400) ? 's' : 'x' ) :
                    (($perms & 0x0400) ? 'S' : '-'));

        // Outros
        $info .= (($perms & 0x0004) ? 'r' : '-');
        $info .= (($perms & 0x0002) ? 'w' : '-');
        $info .= (($perms & 0x0001) ?
                    (($perms & 0x0200) ? 't' : 'x' ) :
                    (($perms & 0x0200) ? 'T' : '-'));

        echo $info;            
        
        
    }
            
    

}
