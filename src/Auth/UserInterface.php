<?php
namespace GoFast\Auth;

interface InterfaceUser
{
    /**
     * Obtem o número de identificação única de usuário
     *
     * @param  mixed  $identifier
     * @return int|null
     */
    public function Id($value);
    
    /**
     * Obtem o número de identificação única de usuário para qualquer sistema 
     *
     * @return string|null
     */
    public function hash($value);    
    
    /**
     * Define ou obtem o nome de usuário
     *
     * @return string|null
     */
    public function name($value);    
    
    /**
     * Define ou obtem a senha se usuário
     * 
     * @param  string
     * @return string|null
     */
    public function password($value);    

    /**
     * Define ou obtem o token de usuário
     *
     * @param  string  $token
     * @return string|null
     */
    public function token($value);
    
    /**
     * Gera um Token no padrão Ymd+id_user que tem tempo de duração de um dia
     *
     * @param  string
     * @return string
     */
    public function privateToken($value);
    
    /**
     * Gera um Token no padrão Ymd+id_user que tem tempo de duração de um dia
     *
     * @param  string
     * @return string
     */    
    public function publicToken($value); 


    /**
     * Gera um Token no padrão Ymd+id_user que tem tempo de duração de um dia
     *
     * @param  string
     * @return $this
     */
    public function credentials($value);

    /**
     * Verifica se o usuário está logado
     *
     * @param  
     * @return boolean
     */
    public function isLogged();    


}
