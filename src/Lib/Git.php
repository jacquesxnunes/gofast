<?php
/**
 * Classe para manipulação do git
 * 
 * @file                git.class.php
 * @license		F71
 * @link		
 * @copyright           2016 F71
 * @author		Jacques <jacques@f71.com.br>
 * @package             Version
 * @access              public  
 * 
 * @version: 3.0.0000 - 27/12/2018 - Jacques - Versão Inicial 
 * 
 * @todo 
 * 
 */
namespace GoFast\Lib;

use GoFast\Kernel\Core;

class Git extends Core{

    public static $instance;  
    
    public $error;  
    
    private $result;

    private $path = array(
                            'branch' => '',
                            'git' => array(
                                            'default' => '',
                                            'win' => 'C:\Arquivos de Programas\Git\bin',
                                            'lin' => '/usr/bin/git'
                                            )
                            );

    use \GoFast\Lib\Bridge;                        

    /**
     * Método construtor da classe
     * 
     * @access public
     * @method __construct
     * @param string $release
     * @param string $path
     * 
     * @return 
     */     
    public function __construct($value = null) {
        
        try {
                    
            parent::__construct($value);   
         
            foreach ($value as $k => $v) {

                switch ($k) {
                    case 'root_dir':
                    case 'path':
                        $path = $v;
                        break;
                    default:
                        break;
                }

            }        

            $this->createCoreClass($value);
            
            $this->setDefault();

            $this->path['branch'] = $path.'.git'.DS;

            if (!is_dir($this->path['branch'])) $this->error->set("# Diretório do git {$this->path['branch']} inexistente",E_FRAMEWORK_WARNING);
            
            $this->setValue(1);

        } catch (\Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }
        
    }
     
    
    /**
     * Método que inicializa a classe com valores default
     * 
     * @access public
     * @method setDefault
     * 
     * @return 
     */     
    public function setDefault() {
        
        $this->commit    = '';
        
        $this->path['git']['default'] = $this->path['git'][strpos('linux', strtolower(php_uname()))!==false ? 'lin' : 'win'];
        
    }    

    /**
     * Método que consulta o commit do origem head
     * 
     * @access public
     * @method getCommitOrigHead
     * 
     * @return bool|string
     */     
    public function getCommitOrigHead() {
        
        return $this->commit = file($this->path['branch'] . 'ORIG_HEAD',FILE_IGNORE_NEW_LINES)[0];

    }
    
    /**
     * Método que consulta o commit do origem head
     * 
     * @access public
     * @method exe
     * 
     * @return bool|string
     */     
    public function exe($value = null) {
        
        try {
            
            foreach ($value as $k => $v) {

                switch ($k) {
                    case 'path':
                        $path = $v;
                        break;
                    case 'command':
                        $command = $v;
                        break;
                    default:
                        break;
                }

            }            

            $process = proc_open(
                "git {$command}",
                array(
                    0 => array("pipe", "r"),  // stdin
                    1 => array("pipe", "w"),  // stdout -> a saída usada é essa
                    2 => array("pipe", "w")   // stderr 
                ),
                $pipes,
                $path
            );

            if (!is_resource($process)) $this->error->set("# O pipe não é um recurso disponível",E_FRAMEWORK_ERROR);

            $this->result = explode(';',trim(stream_get_contents($pipes[1])));
            
            fclose($pipes[0]);        
            fclose($pipes[1]);
            fclose($pipes[2]);

            $this->setValue(1);

            
        } catch (\Exception $ex) {
            
            $this->setValue(0)->error->set(array(1,__METHOD__),E_FRAMEWORK_WARNING,$ex);
            
        }
        
        return $this;
        

    }    
    
    /**
     * Método para obter o resultado de execução de um comando do git
     * 
     * @access public
     * @method get
     * 
     * @return string
     */     
    public function get() {
        
        return $this->result;

    }    
    
}
